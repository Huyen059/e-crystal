call settings.bat common

rem Account: Config
call import.bat utils ParamTargetValue net.democritus.sys Client
call import.bat account Profile net.democritus.usr Client
call import.bat account DataAccess net.democritus.acl Client

rem Workflow: Config
call import.bat workflow Workflow net.democritus.workflow Client
call import.bat workflow EngineService net.democritus.wfe Client
call import.bat workflow StateTask net.democritus.workflow Client

rem Enterprise: Registry
call import.bat enterprise Country net.palver.registry Client
call import.bat enterprise Sector net.palver.registry Client
call import.bat enterprise WorkingYear net.palver.registry Client
call import.bat enterprise WorkingMonth net.palver.registry Client

rem Enterprise: Resources
call import.bat enterprise SourcerType net.palver.resource Client
call import.bat enterprise EmployeeType net.palver.resource Client

rem Timesheet: Provisioning
call import.bat erpTimesheets TimesheetProvisioning org.normalizedsystems.nsx.erp Client2



