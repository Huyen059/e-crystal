call settings.bat %1

rem Account: Users
call import.bat account User net.democritus.usr Client

rem Assets: Assets
call import.bat assets Asset net.democritus.assets Client2

rem Enterprise: Registry
call import.bat enterprise Contact net.palver.registry Client
call import.bat enterprise Personalia net.palver.registry Client
call import.bat enterprise Person net.palver.registry Client2
call import.bat enterprise Corporation net.palver.registry Client

rem Enterprise: Resources
call import.bat enterprise Sourcer net.palver.resource Client
call import.bat enterprise SourcerPersonalContact net.palver.resource Client
call import.bat enterprise SourcerCorporateContact net.palver.resource Client
call import.bat enterprise SourcerContract net.palver.resource Client
call import.bat enterprise Employee net.palver.resource Client
call import.bat enterprise EmployeeHourlyRate net.palver.resource Client
call import.bat enterprise EmployeeSalaryRate net.palver.resource Client
call import.bat enterprise EmployeeHoliday net.palver.resource Client
call import.bat enterprise EmployeeAbsence net.palver.resource Client
call import.bat enterprise EmploymentContract net.palver.resource Client

rem Enterprise: Products
call import.bat enterprise ProductLine net.palver.product Client
call import.bat enterprise SoftwareLicenseType net.palver.product Client
call import.bat enterprise ApplicationServiceType net.palver.product Client
call import.bat enterprise ProjectResourceType net.palver.product Client

rem Enterprise: Customers
call import.bat enterprise Client net.palver.customer Client
call import.bat enterprise ClientPersonalContact net.palver.customer Client
call import.bat enterprise ClientCorporateContact net.palver.customer Client
call import.bat enterprise ClientContract net.palver.customer Client
call import.bat enterprise SalesOrder net.palver.customer Client
call import.bat enterprise SoftwareLicense net.palver.customer Client
call import.bat enterprise ApplicationService net.palver.customer Client





