import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TimesheetListComponent } from './components/erpTimesheets/components/timesheet-list/timesheet-list.component';
import { NavBarComponent } from './nav/nav-bar/nav-bar.component';
import {HttpClientModule} from "@angular/common/http";
import { TimesheetDetailComponent } from './components/erpTimesheets/components/timesheet-detail/timesheet-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    TimesheetListComponent,
    NavBarComponent,
    TimesheetDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
