import {Country} from "./country";

export class Contact {
  address: string;
  city: string;
  country: Country;
  email: string;
  externalId: string;
  name: string;
  phone: string;
}
