import {Country} from "./country";
import {Contact} from "./contact";

export class Corporation {
  bankAccount: string;
  country: Country;
  externalId: string;
  legalForm: string;
  mainContact: Contact;
  name: string;
  vatNumber: string;
}
