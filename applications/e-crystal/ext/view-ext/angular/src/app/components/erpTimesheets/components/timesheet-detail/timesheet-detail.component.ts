import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {TimesheetService} from "../../services/timesheet.service";
import {Timesheet} from "../../classes/timesheet";

@Component({
  selector: 'app-timesheet-detail',
  templateUrl: './timesheet-detail.component.html',
  styleUrls: ['./timesheet-detail.component.css']
})
export class TimesheetDetailComponent implements OnInit {
  timesheet = new Timesheet();

  constructor(
    private timesheetService: TimesheetService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.timesheet.externalId = params.get('id');
    });

    this.timesheetService.getTimesheetByExternalId(this.timesheet.externalId).subscribe(data => {
      let items: any[] = data.list;
      items.forEach(item => {
        let timesheet = this.timesheetService.convertDataToTimesheet(item);
        this.timesheet = timesheet;
      })
    });
  }

}
