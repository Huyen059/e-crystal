import {Personalia} from "./personalia";
import {Contact} from "./contact";

export class Person {
  dateOfBirth: Date;
  externalId: string;
  firstName: string;
  lastName: string;
  name: string;
  personalia: Personalia;
  privateContact: Contact;
}
