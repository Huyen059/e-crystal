export class Personalia {
  bankAccount: string;
  birthPlace: string;
  education: string;
  externalId: string;
  gender: string;
  idNumber: string;
  maritalStatus: string;
}
