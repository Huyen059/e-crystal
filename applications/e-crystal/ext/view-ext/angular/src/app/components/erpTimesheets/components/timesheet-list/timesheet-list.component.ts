import { Component, OnInit } from '@angular/core';
import {Timesheet} from "../../classes/timesheet";
import {TimesheetService} from "../../services/timesheet.service";
import {Observable} from "rxjs";
import {Employee} from "../../../enterprise/classes/employee";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-timesheet-list',
  templateUrl: './timesheet-list.component.html',
  styleUrls: ['./timesheet-list.component.css']
})
export class TimesheetListComponent implements OnInit {
  timesheets: Timesheet[] = [];
  private isFormShow = false;

  constructor(
    private timesheetService: TimesheetService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.timesheetService.getTimesheets().subscribe(
      data => {
        let items : any[] = data.list;
        items.forEach(item => {
          let timesheet = this.timesheetService.convertDataToTimesheet(item);
          this.timesheets.push(timesheet);
        })

      }
    );
  }
  
  onSelect(externalId: string) {
    this.router.navigate([externalId], {relativeTo: this.route});
  }

  showForm() {
    this.isFormShow = true;
  }
}
