import {Corporation} from "./corporation";
import {SourcerType} from "./sourcer-type";

export class Sourcer {
  corporation: Corporation;
  externalId: string;
  name: string;
  sourcerType: SourcerType;
}
