import {Employee} from "../../enterprise/classes/employee";

export class Timesheet {
  externalId: string;
  month: number;
  status: string;
  totalHours: number;
  totalManDays: number;
  year: number;
  employee: Employee;
  user: string;
}
