export enum TimesheetSearchMethod {
  FIND_BY_NAME_EQ = 'findByNameEq',
  FIND_ALL_TIMESHEETS = 'findAllTimesheets',
  FIND_BY_USER_EQ = 'findByUserEq',
  FIND_BY_MONTH_EQ = 'findByMonthEq_YearEq',
  FIND_BY_USER_EQ_YEAR_EQ_MONTH_EQ = 'findByUserEq_YearEq_MonthEq',
  FIND_BY_STATUS_EQ = 'findByStatusEq',
  FIND_BY_EMPLOYEE_EQ = 'findByEmployeeEq',
  FIND_BY_EXTERNALID_EQ = 'findByExternalIdEq'
}
