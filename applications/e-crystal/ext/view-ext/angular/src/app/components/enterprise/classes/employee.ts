import {EmployeeType} from "./employee-type";
import {Person} from "./person";
import {Sourcer} from "./sourcer";

export class Employee {
  active: string;
  contractType: string;
  email: string;
  employeeType: EmployeeType;
  externalId: string;
  mobile: string;
  name: string;
  person: Person;
  sourcer: Sourcer;
}
