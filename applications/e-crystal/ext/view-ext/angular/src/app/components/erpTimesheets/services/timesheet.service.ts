import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {TimesheetSearchMethod} from "../classes/timesheet-search-method.enum";
import {Observable, of} from "rxjs";
import {UrlGeneratorService} from "../../../url-generator.service";
import {MessageService} from "../../../message.service";
import {catchError, tap} from "rxjs/operators";
import {authorizationData} from "../../../../../../../../credential";
import {Timesheet} from "../classes/timesheet";
import {Employee} from "../../enterprise/classes/employee";

@Injectable({
  providedIn: 'root'
})
export class TimesheetService {
  private componentName = 'erpTimesheets';
  private elementApi = 'timesheet-search-json';
  private page = 0;
  private rowsPerPage = 7;
  private projection = 'info';
  private searchMethod = TimesheetSearchMethod.FIND_ALL_TIMESHEETS;

  constructor(
    private http: HttpClient,
    private urlGeneratorService: UrlGeneratorService,
    private messageService: MessageService
  ) {
  }

  private authorizationData: string;

  getTimesheets(): Observable<any> {
    const url = this.generateUrl();
    return this.http.get<any>(
      url,
      {headers: new HttpHeaders(`Authorization: Basic ${authorizationData}`)})
      .pipe(
        tap(_ => this.log('fetched timesheets')),
        catchError(this.handleError('Get timesheets', []))
      );
  }

  getTimesheetByExternalId(externalId: string) {
    this.searchMethod = TimesheetSearchMethod.FIND_BY_EXTERNALID_EQ;
    const url = this.generateUrl() + '&details.externalId=' + externalId;
    return this.http.get<any>(
      url,
      {headers: new HttpHeaders(`Authorization: Basic ${authorizationData}`)})
      .pipe(
        tap(_ => this.log('fetched timesheet with externalId: ' + externalId)),
        catchError(this.handleError('Get timesheet by externalId', []))
      );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  convertDataToTimesheet(item) {
    let timesheet = new Timesheet();
    if (item.employee.id != 0) {
      let employee = new Employee();
      employee.name = item.employee.name;
    }

    timesheet.externalId = item.externalId;
    timesheet.month = item.month;
    timesheet.status = item.status;
    timesheet.totalHours = item.totalHours;
    timesheet.totalManDays = item.totalManDays;
    timesheet.year = item.year;
    if (item.user.id != 0) {
      timesheet.user = item.user.name;
    }
    return timesheet;
  }

  log(message: string): void {
    this.messageService.add(`TimesheetService: ${message}`);
  }

  private generateUrl() {
    return this.urlGeneratorService.generateUrl(this.componentName, this.elementApi, this.searchMethod, this.page, this.rowsPerPage, this.projection);
  }
}
