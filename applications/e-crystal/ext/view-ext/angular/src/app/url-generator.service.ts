import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UrlGeneratorService {
  private _baseUrl = 'http://localhost:9300';
  private _applicationName = 'e-crystal';
  private componentName: string;
  private elementApi: string;
  private page: number;
  private rowsPerPage: number;
  private projection: string;
  private searchMethod: string;

  constructor() {
  }

  generateUrl(componentName: string, elementApi: string, searchMethod: string, page: number, rowsPerPage: number, projection: string): string {
    return this.baseUrl + '/'
      + this.applicationName + '/'
      + componentName + '/'
      + elementApi + '?'
      + 'page=' + page + '&'
      + 'rowsPerPage=' + rowsPerPage + '&'
      + 'projection=' + projection + '&'
      + 'searchMethod=' + searchMethod;
  }


  get baseUrl(): string {
    return this._baseUrl;
  }

  get applicationName(): string {
    return this._applicationName;
  }
}
