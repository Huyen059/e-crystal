import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TimesheetListComponent} from "./components/erpTimesheets/components/timesheet-list/timesheet-list.component";
import {TimesheetDetailComponent} from "./components/erpTimesheets/components/timesheet-detail/timesheet-detail.component";

const routes: Routes = [
  {
    path: 'timesheets',
    component: TimesheetListComponent
  },
  {
    path: 'timesheets/:id',
    component: TimesheetDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
