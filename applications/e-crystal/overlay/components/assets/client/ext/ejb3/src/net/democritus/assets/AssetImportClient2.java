package net.democritus.assets;

// expanded with version 3.1.7

import java.util.Vector;
import java.util.Collection;
import java.util.StringTokenizer;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.FileReader;
import java.io.BufferedReader;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.democritus.sys.DataRef;
import net.democritus.sys.CrudsResult;
import net.democritus.sys.UserContext;
import net.democritus.sys.ParameterContext;

import net.democritus.jndi.ComponentJNDI;


// anchor:imports:start
// anchor:imports:end

/**
 * Remote import client to test and perform CRUD with respect,
 * to the business logic of the Asset in the EJB tier
 */

public class AssetImportClient2 {

  private static AssetRemote asset = null;
  private static AssetAgent assetAgent = null;


  public static void main(String args[]) {

    Context ctx = null;
    String paramVal = "";
    Integer paramInt = null;
    Long paramLong = null;
    Double paramDouble = null;
    Date paramDate = null;
    Boolean paramBoolean = null;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
    DataRef dataRef = null;
    Vector<DataRef> dataRefVec = null;
    StringTokenizer paramValStrTok = null;

    // Check if there is one argument
    if (args.length != 1) {
      System.err.println("Syntax is : java -cp <classpath> AssetImportClient file:/'absolutePath'/Asset.csv");
      System.err.println("  using separator token ';' between fields, and '|' inside field enumerations");
      System.err.println("  fields are [name;type;fileAsset;internalAsset;downloadLink;contentType;byteSize;fileSize;fileId;complete;externalAsset]");
      System.err.println("  and SimpleDateFormat 'dd-MM-yy HH:mm:ss'");
      System.exit(2);
    }

    try {
      ctx = new InitialContext();
    }
    catch (Exception e) {
      System.err.println("Failed to create initial context: ");
      e.printStackTrace();
    }

    try {
      //ComponentJNDI componentJNDI = ComponentJNDI.getComponentJNDI("assets");
      //String assetName = componentJNDI.getDataRemoteName("Asset");
      //asset = (AssetRemote) componentJNDI.lookupRemote(assetName);
      assetAgent = AssetAgent.getAssetAgent();
    }
    catch (Exception e) {
      System.err.println("Failed to narrow AssetRemote: ");
      e.printStackTrace();
    }

    int nrFields = 0;
    // anchor:number-of-fields:start
    nrFields = nrFields + 4;
    // anchor:number-of-fields:end

    try {
      int lineNr = 0;

      // open File with URI args[0]
      File aFile = new File(URI.create(args[0]));
      FileReader aFr = new FileReader(aFile);
      BufferedReader aBr = new BufferedReader(aFr);
      String aLine = "";
      String [] lineParts = null;
      while (aLine != null) {
        aLine = aBr.readLine();
        if (aLine != null) {
          lineParts = aLine.split(";", -1);
          if (lineParts.length == nrFields) {
            System.out.println("import CSV row with index " + lineNr);
            AssetDetails assetDetails = new AssetDetails();
            // anchor:detail-assignments:start
            paramVal = lineParts[0];
            assetDetails.setName(paramVal);
            paramVal = lineParts[1];
            assetDetails.setType(paramVal);
            paramVal = lineParts[2];
            assetDetails.setContentType(paramVal);
            paramVal = lineParts[3];
            // anchor:detail-assignments:end
            String name = assetDetails.getName();
            Long id = null;
            try {
              File uploadFile = new File(paramVal);
              InputStream uploadStream = new FileInputStream(uploadFile);
              assetDetails.setByteSize(new Long(uploadFile.length()));
              AssetStream assetStream = new AssetStream();
              assetStream.setInputStream(uploadStream);
              assetStream.setAssetDetails(assetDetails);
              CrudsResult<DataRef> result = assetAgent.uploadAsset(assetStream);
/*
              CrudsResult<DataRef> result = asset.getId(createParameter(name));
              if (result.isSuccess()) {
                id = result.getValue().getId();
                assetDetails.setId(id);
                modifyDetails(assetDetails);
              } else {
                createDetails(assetDetails);
              }
*/
            }
            catch (Exception e) {
              createDetails(assetDetails);
            }
          }
          else {
            System.err.println("incorrect number of arguments at line with index = " + lineNr);
          }
          lineNr++;
        }
      }
    }
    catch (Exception e) {
      System.err.println("Failed to create Asset entry: ");
      e.printStackTrace();
    }
  }

  private static void createDetails(AssetDetails details) {
     ParameterContext<AssetDetails> detailsParameter = new ParameterContext<AssetDetails>(getUserContext(), details);
     asset.create(detailsParameter);
  }

  private static void modifyDetails(AssetDetails details) {
     ParameterContext<AssetDetails> detailsParameter = new ParameterContext<AssetDetails>(getUserContext(), details);
     asset.modify(detailsParameter);
  }

  private static UserContext getUserContext() {
    return null;
  }

  private static <T> ParameterContext<T> createParameter(T value) {
    return new ParameterContext(getUserContext(), value);
  }

}
