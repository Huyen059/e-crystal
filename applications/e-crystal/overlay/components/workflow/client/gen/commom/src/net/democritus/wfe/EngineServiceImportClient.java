package net.democritus.wfe;

// expanded with version 3.2.1.4

import java.util.Vector;
import java.util.Collection;
import java.util.StringTokenizer;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.democritus.sys.DataRef;
import net.democritus.sys.CrudsResult;
import net.democritus.sys.UserContext;
import net.democritus.sys.ParameterContext;

import net.democritus.jndi.ComponentJNDI;


// anchor:imports:start
import net.democritus.valuetype.basic.DateTime;
import net.democritus.valuetype.basic.DateTimeConverter;
// anchor:imports:end

/**
 * Remote import client to test and perform CRUD with respect,
 * to the business logic of the EngineService in the EJB tier
 */

public class EngineServiceImportClient {

  private static EngineServiceRemote engineService = null;


  public static void main(String args[]) {

    Context ctx = null;
    String paramVal = "";
    Integer paramInt = null;
    Long paramLong = null;
    Double paramDouble = null;
    Date paramDate = null;
    Boolean paramBoolean = null;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
    DataRef dataRef = null;
    Vector<DataRef> dataRefVec = null;
    StringTokenizer paramValStrTok = null;

    // Check if there is one argument
    if (args.length != 1) {
      System.err.println("Syntax is : java -cp <classpath> EngineServiceImportClient file:/'absolutePath'/EngineService.csv");
      System.err.println("  using separator token ';' between fields, and '|' inside field enumerations");
      System.err.println("  fields are [name;status;changed;busy;waitTime;collector;workflow;timeWindowGroup;lastRunAt;batchSize;maximumNumberOfNodes;engineNodeServices]");
      System.err.println("  and SimpleDateFormat 'dd-MM-yy HH:mm:ss'");
      System.exit(2);
    }

    try {
      ctx = new InitialContext();
    }
    catch (Exception e) {
      System.err.println("Failed to create initial context: ");
      e.printStackTrace();
    }

    try {
      ComponentJNDI componentJNDI = ComponentJNDI.getComponentJNDI("workflow");
      String engineServiceName = componentJNDI.getDataRemoteName("EngineService");
      engineService = (EngineServiceRemote) componentJNDI.lookupRemote(engineServiceName);
    }
    catch (Exception e) {
      System.err.println("Failed to narrow EngineServiceRemote: ");
      e.printStackTrace();
    }

    int nrFields = 0;
    // anchor:number-of-fields:start
    nrFields = nrFields + 12;
    // anchor:number-of-fields:end

    try {
      int lineNr = 0;

      // open File with URI args[0]
      File aFile = new File(URI.create(args[0]));
      FileReader aFr = new FileReader(aFile);
      BufferedReader aBr = new BufferedReader(aFr);
      String aLine = "";
      String [] lineParts = null;
      while (aLine != null) {
        aLine = aBr.readLine();
        if (aLine != null) {
          lineParts = aLine.split(";", -1);
          if (lineParts.length == nrFields) {
            System.out.println("import CSV row with index " + lineNr);
            EngineServiceDetails engineServiceDetails = new EngineServiceDetails();
            // anchor:detail-assignments:start
            paramVal = lineParts[0];
            engineServiceDetails.setName(paramVal);
            paramVal = lineParts[1];
            engineServiceDetails.setStatus(paramVal);
            paramVal = lineParts[2];
            engineServiceDetails.setChanged(paramVal);
            paramVal = lineParts[3];
            engineServiceDetails.setBusy(paramVal);
            paramVal = lineParts[4];
            try {
              paramInt = new Integer(paramVal);
              engineServiceDetails.setWaitTime(paramInt);
            } catch (Exception e) {
              System.err.println("error assigning Integer: " + e);
              e.printStackTrace();
            }
            paramVal = lineParts[5];
            try {
              paramLong = new Long(paramVal);
              engineServiceDetails.setCollector(paramLong);
            } catch (Exception e) {
              System.err.println("error assigning Long: " + e);
              e.printStackTrace();
            }
            paramVal = lineParts[6];
            dataRef = DataRef.withName(paramVal);
            engineServiceDetails.setWorkflow(dataRef);
            paramVal = lineParts[7];
            dataRef = DataRef.withName(paramVal);
            engineServiceDetails.setTimeWindowGroup(dataRef);
            paramVal = lineParts[8];
            engineServiceDetails.setLastRunAt(new DateTimeConverter().fromString(paramVal).getValue());
            paramVal = lineParts[9];
            try {
              paramInt = new Integer(paramVal);
              engineServiceDetails.setBatchSize(paramInt);
            } catch (Exception e) {
              System.err.println("error assigning Integer: " + e);
              e.printStackTrace();
            }
            paramVal = lineParts[10];
            try {
              paramInt = new Integer(paramVal);
              engineServiceDetails.setMaximumNumberOfNodes(paramInt);
            } catch (Exception e) {
              System.err.println("error assigning Integer: " + e);
              e.printStackTrace();
            }
            paramVal = lineParts[11];
            dataRefVec = new Vector();
            if (lineParts[11].length() > 1) {
              paramValStrTok = new StringTokenizer(lineParts[11], "|");
              while (paramValStrTok.hasMoreTokens()) {
                paramVal = paramValStrTok.nextToken();
                System.out.println("EngineNodeServices paramVal = " + paramVal);
                dataRef = DataRef.withName(paramVal);
                dataRefVec.add(dataRef);
              }
            }
            engineServiceDetails.setEngineNodeServices(dataRefVec);
            // anchor:detail-assignments:end
            String name = engineServiceDetails.getName();
            Long id = null;
            try {
              CrudsResult<DataRef> result = engineService.getId(createParameter(name));
              if (result.isSuccess()) {
                id = result.getValue().getId();
                engineServiceDetails.setId(id);
                modifyDetails(engineServiceDetails);
              } else {
                createDetails(engineServiceDetails);
              }
            }
            catch (Exception e) {
              createDetails(engineServiceDetails);
            }
          }
          else {
            System.err.println("incorrect number of arguments at line with index = " + lineNr);
          }
          lineNr++;
        }
      }
    }
    catch (Exception e) {
      System.err.println("Failed to create EngineService entry: ");
      e.printStackTrace();
    }
  }

  private static void createDetails(EngineServiceDetails details) {
     ParameterContext<EngineServiceDetails> detailsParameter = new ParameterContext<EngineServiceDetails>(getUserContext(), details);
     engineService.create(detailsParameter);
  }

  private static void modifyDetails(EngineServiceDetails details) {
     ParameterContext<EngineServiceDetails> detailsParameter = new ParameterContext<EngineServiceDetails>(getUserContext(), details);
     engineService.modify(detailsParameter);
  }

  private static UserContext getUserContext() {
    return null;
  }

  private static <T> ParameterContext<T> createParameter(T value) {
    return new ParameterContext(getUserContext(), value);
  }

}

