package net.democritus.workflow;

// expanded with version 3.2.1.4

import java.util.Vector;
import java.util.Collection;
import java.util.StringTokenizer;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.democritus.sys.DataRef;
import net.democritus.sys.CrudsResult;
import net.democritus.sys.UserContext;
import net.democritus.sys.ParameterContext;

import net.democritus.jndi.ComponentJNDI;


// anchor:imports:start
// anchor:imports:end

/**
 * Remote import client to test and perform CRUD with respect,
 * to the business logic of the StateTask in the EJB tier
 */

public class StateTaskImportClient {

  private static StateTaskRemote stateTask = null;


  public static void main(String args[]) {

    Context ctx = null;
    String paramVal = "";
    Integer paramInt = null;
    Long paramLong = null;
    Double paramDouble = null;
    Date paramDate = null;
    Boolean paramBoolean = null;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
    DataRef dataRef = null;
    Vector<DataRef> dataRefVec = null;
    StringTokenizer paramValStrTok = null;

    // Check if there is one argument
    if (args.length != 1) {
      System.err.println("Syntax is : java -cp <classpath> StateTaskImportClient file:/'absolutePath'/StateTask.csv");
      System.err.println("  using separator token ';' between fields, and '|' inside field enumerations");
      System.err.println("  fields are [name;processor;implementation;params;beginState;interimState;failedState;endState;workflow;maxConcurrentTasks;timeout]");
      System.err.println("  and SimpleDateFormat 'dd-MM-yy HH:mm:ss'");
      System.exit(2);
    }

    try {
      ctx = new InitialContext();
    }
    catch (Exception e) {
      System.err.println("Failed to create initial context: ");
      e.printStackTrace();
    }

    try {
      ComponentJNDI componentJNDI = ComponentJNDI.getComponentJNDI("workflow");
      String stateTaskName = componentJNDI.getDataRemoteName("StateTask");
      stateTask = (StateTaskRemote) componentJNDI.lookupRemote(stateTaskName);
    }
    catch (Exception e) {
      System.err.println("Failed to narrow StateTaskRemote: ");
      e.printStackTrace();
    }

    int nrFields = 0;
    // anchor:number-of-fields:start
    nrFields = nrFields + 11;
    // anchor:number-of-fields:end

    try {
      int lineNr = 0;

      // open File with URI args[0]
      File aFile = new File(URI.create(args[0]));
      FileReader aFr = new FileReader(aFile);
      BufferedReader aBr = new BufferedReader(aFr);
      String aLine = "";
      String [] lineParts = null;
      while (aLine != null) {
        aLine = aBr.readLine();
        if (aLine != null) {
          lineParts = aLine.split(";", -1);
          if (lineParts.length == nrFields) {
            System.out.println("import CSV row with index " + lineNr);
            StateTaskDetails stateTaskDetails = new StateTaskDetails();
            // anchor:detail-assignments:start
            paramVal = lineParts[0];
            stateTaskDetails.setName(paramVal);
            paramVal = lineParts[1];
            stateTaskDetails.setProcessor(paramVal);
            paramVal = lineParts[2];
            stateTaskDetails.setImplementation(paramVal);
            paramVal = lineParts[3];
            stateTaskDetails.setParams(paramVal);
            paramVal = lineParts[4];
            stateTaskDetails.setBeginState(paramVal);
            paramVal = lineParts[5];
            stateTaskDetails.setInterimState(paramVal);
            paramVal = lineParts[6];
            stateTaskDetails.setFailedState(paramVal);
            paramVal = lineParts[7];
            stateTaskDetails.setEndState(paramVal);
            paramVal = lineParts[8];
            dataRef = DataRef.withName(paramVal);
            stateTaskDetails.setWorkflow(dataRef);
            paramVal = lineParts[9];
            try {
              paramInt = new Integer(paramVal);
              stateTaskDetails.setMaxConcurrentTasks(paramInt);
            } catch (Exception e) {
              System.err.println("error assigning Integer: " + e);
              e.printStackTrace();
            }
            paramVal = lineParts[10];
            try {
              paramLong = new Long(paramVal);
              stateTaskDetails.setTimeout(paramLong);
            } catch (Exception e) {
              System.err.println("error assigning Long: " + e);
              e.printStackTrace();
            }
            // anchor:detail-assignments:end
            String name = stateTaskDetails.getName();
            Long id = null;
            try {
              CrudsResult<DataRef> result = stateTask.getId(createParameter(name));
              if (result.isSuccess()) {
                id = result.getValue().getId();
                stateTaskDetails.setId(id);
                modifyDetails(stateTaskDetails);
              } else {
                createDetails(stateTaskDetails);
              }
            }
            catch (Exception e) {
              createDetails(stateTaskDetails);
            }
          }
          else {
            System.err.println("incorrect number of arguments at line with index = " + lineNr);
          }
          lineNr++;
        }
      }
    }
    catch (Exception e) {
      System.err.println("Failed to create StateTask entry: ");
      e.printStackTrace();
    }
  }

  private static void createDetails(StateTaskDetails details) {
     ParameterContext<StateTaskDetails> detailsParameter = new ParameterContext<StateTaskDetails>(getUserContext(), details);
     stateTask.create(detailsParameter);
  }

  private static void modifyDetails(StateTaskDetails details) {
     ParameterContext<StateTaskDetails> detailsParameter = new ParameterContext<StateTaskDetails>(getUserContext(), details);
     stateTask.modify(detailsParameter);
  }

  private static UserContext getUserContext() {
    return null;
  }

  private static <T> ParameterContext<T> createParameter(T value) {
    return new ParameterContext(getUserContext(), value);
  }

}

