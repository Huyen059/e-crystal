package net.democritus.usr;

// expanded with version 3.2.1.4

import java.util.Vector;
import java.util.Collection;
import java.util.StringTokenizer;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.democritus.sys.DataRef;
import net.democritus.sys.CrudsResult;
import net.democritus.sys.UserContext;
import net.democritus.sys.ParameterContext;

import net.democritus.jndi.ComponentJNDI;


// anchor:imports:start
import java.util.Date;
// anchor:imports:end

/**
 * Remote import client to test and perform CRUD with respect,
 * to the business logic of the User in the EJB tier
 */

public class UserImportClient {

  private static UserRemote user = null;


  public static void main(String args[]) {

    Context ctx = null;
    String paramVal = "";
    Integer paramInt = null;
    Long paramLong = null;
    Double paramDouble = null;
    Date paramDate = null;
    Boolean paramBoolean = null;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
    DataRef dataRef = null;
    Vector<DataRef> dataRefVec = null;
    StringTokenizer paramValStrTok = null;

    // Check if there is one argument
    if (args.length != 1) {
      System.err.println("Syntax is : java -cp <classpath> UserImportClient file:/'absolutePath'/User.csv");
      System.err.println("  using separator token ';' between fields, and '|' inside field enumerations");
      System.err.println("  fields are [name;password;fullName;email;mobile;language;firstName;lastName;persNr;lastModifiedAt;enteredAt;disabled;timeout;account;profile;userGroups;encryptedPassword]");
      System.err.println("  and SimpleDateFormat 'dd-MM-yy HH:mm:ss'");
      System.exit(2);
    }

    try {
      ctx = new InitialContext();
    }
    catch (Exception e) {
      System.err.println("Failed to create initial context: ");
      e.printStackTrace();
    }

    try {
      ComponentJNDI componentJNDI = ComponentJNDI.getComponentJNDI("account");
      String userName = componentJNDI.getDataRemoteName("User");
      user = (UserRemote) componentJNDI.lookupRemote(userName);
    }
    catch (Exception e) {
      System.err.println("Failed to narrow UserRemote: ");
      e.printStackTrace();
    }

    int nrFields = 0;
    // anchor:number-of-fields:start
    nrFields = nrFields + 17;
    // anchor:number-of-fields:end

    try {
      int lineNr = 0;

      // open File with URI args[0]
      File aFile = new File(URI.create(args[0]));
      FileReader aFr = new FileReader(aFile);
      BufferedReader aBr = new BufferedReader(aFr);
      String aLine = "";
      String [] lineParts = null;
      while (aLine != null) {
        aLine = aBr.readLine();
        if (aLine != null) {
          lineParts = aLine.split(";", -1);
          if (lineParts.length == nrFields) {
            System.out.println("import CSV row with index " + lineNr);
            UserDetails userDetails = new UserDetails();
            // anchor:detail-assignments:start
            paramVal = lineParts[0];
            userDetails.setName(paramVal);
            paramVal = lineParts[1];
            userDetails.setPassword(paramVal);
            paramVal = lineParts[2];
            userDetails.setFullName(paramVal);
            paramVal = lineParts[3];
            userDetails.setEmail(paramVal);
            paramVal = lineParts[4];
            userDetails.setMobile(paramVal);
            paramVal = lineParts[5];
            userDetails.setLanguage(paramVal);
            paramVal = lineParts[6];
            userDetails.setFirstName(paramVal);
            paramVal = lineParts[7];
            userDetails.setLastName(paramVal);
            paramVal = lineParts[8];
            userDetails.setPersNr(paramVal);
            paramVal = lineParts[9];
            try {
              paramDate = sdf.parse(paramVal);
              userDetails.setLastModifiedAt(paramDate);
            } catch (Exception e) {
              System.err.println("error assigning Date: " + e + ", new Date() will be used");
              e.printStackTrace();
              paramDate = new Date();
            }
            paramVal = lineParts[10];
            try {
              paramDate = sdf.parse(paramVal);
              userDetails.setEnteredAt(paramDate);
            } catch (Exception e) {
              System.err.println("error assigning Date: " + e + ", new Date() will be used");
              e.printStackTrace();
              paramDate = new Date();
            }
            paramVal = lineParts[11];
            userDetails.setDisabled(paramVal);
            paramVal = lineParts[12];
            try {
              paramInt = new Integer(paramVal);
              userDetails.setTimeout(paramInt);
            } catch (Exception e) {
              System.err.println("error assigning Integer: " + e);
              e.printStackTrace();
            }
            paramVal = lineParts[13];
            dataRef = DataRef.withName(paramVal);
            userDetails.setAccount(dataRef);
            paramVal = lineParts[14];
            dataRef = DataRef.withName(paramVal);
            userDetails.setProfile(dataRef);
            paramVal = lineParts[15];
            dataRefVec = new Vector();
            if (lineParts[15].length() > 1) {
              paramValStrTok = new StringTokenizer(lineParts[15], "|");
              while (paramValStrTok.hasMoreTokens()) {
                paramVal = paramValStrTok.nextToken();
                System.out.println("UserGroups paramVal = " + paramVal);
                dataRef = DataRef.withName(paramVal);
                dataRefVec.add(dataRef);
              }
            }
            userDetails.setUserGroups(dataRefVec);
            paramVal = lineParts[16];
            userDetails.setEncryptedPassword(paramVal);
            // anchor:detail-assignments:end
            String name = userDetails.getName();
            Long id = null;
            try {
              CrudsResult<DataRef> result = user.getId(createParameter(name));
              if (result.isSuccess()) {
                id = result.getValue().getId();
                userDetails.setId(id);
                modifyDetails(userDetails);
              } else {
                createDetails(userDetails);
              }
            }
            catch (Exception e) {
              createDetails(userDetails);
            }
          }
          else {
            System.err.println("incorrect number of arguments at line with index = " + lineNr);
          }
          lineNr++;
        }
      }
    }
    catch (Exception e) {
      System.err.println("Failed to create User entry: ");
      e.printStackTrace();
    }
  }

  private static void createDetails(UserDetails details) {
     ParameterContext<UserDetails> detailsParameter = new ParameterContext<UserDetails>(getUserContext(), details);
     user.create(detailsParameter);
  }

  private static void modifyDetails(UserDetails details) {
     ParameterContext<UserDetails> detailsParameter = new ParameterContext<UserDetails>(getUserContext(), details);
     user.modify(detailsParameter);
  }

  private static UserContext getUserContext() {
    return null;
  }

  private static <T> ParameterContext<T> createParameter(T value) {
    return new ParameterContext(getUserContext(), value);
  }

}

