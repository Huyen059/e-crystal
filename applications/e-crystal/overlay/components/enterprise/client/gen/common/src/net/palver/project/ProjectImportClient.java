package net.palver.project;

// expanded with version 3.2.1.4

import java.util.Vector;
import java.util.Collection;
import java.util.StringTokenizer;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.democritus.sys.DataRef;
import net.democritus.sys.CrudsResult;
import net.democritus.sys.UserContext;
import net.democritus.sys.ParameterContext;

import net.democritus.jndi.ComponentJNDI;


// anchor:imports:start
// anchor:imports:end

/**
 * Remote import client to test and perform CRUD with respect,
 * to the business logic of the Project in the EJB tier
 */

public class ProjectImportClient {

  private static ProjectRemote project = null;


  public static void main(String args[]) {

    Context ctx = null;
    String paramVal = "";
    Integer paramInt = null;
    Long paramLong = null;
    Double paramDouble = null;
    Date paramDate = null;
    Boolean paramBoolean = null;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
    DataRef dataRef = null;
    Vector<DataRef> dataRefVec = null;
    StringTokenizer paramValStrTok = null;

    // Check if there is one argument
    if (args.length != 1) {
      System.err.println("Syntax is : java -cp <classpath> ProjectImportClient file:/'absolutePath'/Project.csv");
      System.err.println("  using separator token ';' between fields, and '|' inside field enumerations");
      System.err.println("  fields are [name;client;clientReference;developmentBudget;maintenanceBudget;order;remainingBudget;invoicingMode;clientResponsible;projectManager;technicalLead;domainAnalyst;internalCode]");
      System.err.println("  and SimpleDateFormat 'dd-MM-yy HH:mm:ss'");
      System.exit(2);
    }

    try {
      ctx = new InitialContext();
    }
    catch (Exception e) {
      System.err.println("Failed to create initial context: ");
      e.printStackTrace();
    }

    try {
      ComponentJNDI componentJNDI = ComponentJNDI.getComponentJNDI("enterprise");
      String projectName = componentJNDI.getDataRemoteName("Project");
      project = (ProjectRemote) componentJNDI.lookupRemote(projectName);
    }
    catch (Exception e) {
      System.err.println("Failed to narrow ProjectRemote: ");
      e.printStackTrace();
    }

    int nrFields = 0;
    // anchor:number-of-fields:start
    nrFields = nrFields + 13;
    // anchor:number-of-fields:end

    try {
      int lineNr = 0;

      // open File with URI args[0]
      File aFile = new File(URI.create(args[0]));
      FileReader aFr = new FileReader(aFile);
      BufferedReader aBr = new BufferedReader(aFr);
      String aLine = "";
      String [] lineParts = null;
      while (aLine != null) {
        aLine = aBr.readLine();
        if (aLine != null) {
          lineParts = aLine.split(";", -1);
          if (lineParts.length == nrFields) {
            System.out.println("import CSV row with index " + lineNr);
            ProjectDetails projectDetails = new ProjectDetails();
            // anchor:detail-assignments:start
            paramVal = lineParts[0];
            projectDetails.setName(paramVal);
            paramVal = lineParts[1];
            dataRef = DataRef.withName(paramVal);
            projectDetails.setClient(dataRef);
            paramVal = lineParts[2];
            projectDetails.setClientReference(paramVal);
            paramVal = lineParts[3];
            try {
              paramDouble = new Double(paramVal);
              projectDetails.setDevelopmentBudget(paramDouble);
            } catch (Exception e) {
              System.err.println("error assigning Double: " + e);
              e.printStackTrace();
            }
            paramVal = lineParts[4];
            try {
              paramDouble = new Double(paramVal);
              projectDetails.setMaintenanceBudget(paramDouble);
            } catch (Exception e) {
              System.err.println("error assigning Double: " + e);
              e.printStackTrace();
            }
            paramVal = lineParts[5];
            dataRef = DataRef.withName(paramVal);
            projectDetails.setOrder(dataRef);
            paramVal = lineParts[6];
            projectDetails.setInvoicingMode(paramVal);
            paramVal = lineParts[7];
            dataRef = DataRef.withName(paramVal);
            projectDetails.setClientResponsible(dataRef);
            paramVal = lineParts[8];
            dataRef = DataRef.withName(paramVal);
            projectDetails.setProjectManager(dataRef);
            paramVal = lineParts[9];
            dataRef = DataRef.withName(paramVal);
            projectDetails.setTechnicalLead(dataRef);
            paramVal = lineParts[10];
            dataRef = DataRef.withName(paramVal);
            projectDetails.setDomainAnalyst(dataRef);
            paramVal = lineParts[11];
            projectDetails.setInternalCode(paramVal);
            // anchor:detail-assignments:end
            String name = projectDetails.getName();
            Long id = null;
            try {
              CrudsResult<DataRef> result = project.getId(createParameter(name));
              if (result.isSuccess()) {
                id = result.getValue().getId();
                projectDetails.setId(id);
                modifyDetails(projectDetails);
              } else {
                createDetails(projectDetails);
              }
            }
            catch (Exception e) {
              createDetails(projectDetails);
            }
          }
          else {
            System.err.println("incorrect number of arguments at line with index = " + lineNr);
          }
          lineNr++;
        }
      }
    }
    catch (Exception e) {
      System.err.println("Failed to create Project entry: ");
      e.printStackTrace();
    }
  }

  private static void createDetails(ProjectDetails details) {
     ParameterContext<ProjectDetails> detailsParameter = new ParameterContext<ProjectDetails>(getUserContext(), details);
     project.create(detailsParameter);
  }

  private static void modifyDetails(ProjectDetails details) {
     ParameterContext<ProjectDetails> detailsParameter = new ParameterContext<ProjectDetails>(getUserContext(), details);
     project.modify(detailsParameter);
  }

  private static UserContext getUserContext() {
    return null;
  }

  private static <T> ParameterContext<T> createParameter(T value) {
    return new ParameterContext(getUserContext(), value);
  }

}

