package net.palver.registry;

// expanded with version 3.2.1.4

import java.util.Vector;
import java.util.Collection;
import java.util.StringTokenizer;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.democritus.sys.DataRef;
import net.democritus.sys.CrudsResult;
import net.democritus.sys.UserContext;
import net.democritus.sys.ParameterContext;

import net.democritus.jndi.ComponentJNDI;


// anchor:imports:start
import net.democritus.valuetype.basic.Iban;
import net.democritus.valuetype.basic.IbanConverter;
// anchor:imports:end

/**
 * Remote import client to test and perform CRUD with respect,
 * to the business logic of the Personalia in the EJB tier
 */

public class PersonaliaImportClient {

  private static PersonaliaRemote personalia = null;


  public static void main(String args[]) {

    Context ctx = null;
    String paramVal = "";
    Integer paramInt = null;
    Long paramLong = null;
    Double paramDouble = null;
    Date paramDate = null;
    Boolean paramBoolean = null;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
    DataRef dataRef = null;
    Vector<DataRef> dataRefVec = null;
    StringTokenizer paramValStrTok = null;

    // Check if there is one argument
    if (args.length != 1) {
      System.err.println("Syntax is : java -cp <classpath> PersonaliaImportClient file:/'absolutePath'/Personalia.csv");
      System.err.println("  using separator token ';' between fields, and '|' inside field enumerations");
      System.err.println("  fields are [name;nationalRegister;idNumber;gender;nationality;birthPlace;education;maritalStatus;partner;bankAccount]");
      System.err.println("  and SimpleDateFormat 'dd-MM-yy HH:mm:ss'");
      System.exit(2);
    }

    try {
      ctx = new InitialContext();
    }
    catch (Exception e) {
      System.err.println("Failed to create initial context: ");
      e.printStackTrace();
    }

    try {
      ComponentJNDI componentJNDI = ComponentJNDI.getComponentJNDI("enterprise");
      String personaliaName = componentJNDI.getDataRemoteName("Personalia");
      personalia = (PersonaliaRemote) componentJNDI.lookupRemote(personaliaName);
    }
    catch (Exception e) {
      System.err.println("Failed to narrow PersonaliaRemote: ");
      e.printStackTrace();
    }

    int nrFields = 0;
    // anchor:number-of-fields:start
    nrFields = nrFields + 10;
    // anchor:number-of-fields:end

    try {
      int lineNr = 0;

      // open File with URI args[0]
      File aFile = new File(URI.create(args[0]));
      FileReader aFr = new FileReader(aFile);
      BufferedReader aBr = new BufferedReader(aFr);
      String aLine = "";
      String [] lineParts = null;
      while (aLine != null) {
        aLine = aBr.readLine();
        if (aLine != null) {
          lineParts = aLine.split(";", -1);
          if (lineParts.length == nrFields) {
            System.out.println("import CSV row with index " + lineNr);
            PersonaliaDetails personaliaDetails = new PersonaliaDetails();
            // anchor:detail-assignments:start
            paramVal = lineParts[0];
            personaliaDetails.setName(paramVal);
            paramVal = lineParts[1];
            personaliaDetails.setNationalRegister(paramVal);
            paramVal = lineParts[2];
            personaliaDetails.setIdNumber(paramVal);
            paramVal = lineParts[3];
            personaliaDetails.setGender(paramVal);
            paramVal = lineParts[4];
            personaliaDetails.setNationality(paramVal);
            paramVal = lineParts[5];
            personaliaDetails.setBirthPlace(paramVal);
            paramVal = lineParts[6];
            personaliaDetails.setEducation(paramVal);
            paramVal = lineParts[7];
            personaliaDetails.setMaritalStatus(paramVal);
            paramVal = lineParts[8];
            personaliaDetails.setPartner(paramVal);
            paramVal = lineParts[9];
            personaliaDetails.setBankAccount(new IbanConverter().fromString(paramVal).getValue());
            // anchor:detail-assignments:end
            String name = personaliaDetails.getName();
            Long id = null;
            try {
              CrudsResult<DataRef> result = personalia.getId(createParameter(name));
              if (result.isSuccess()) {
                id = result.getValue().getId();
                personaliaDetails.setId(id);
                modifyDetails(personaliaDetails);
              } else {
                createDetails(personaliaDetails);
              }
            }
            catch (Exception e) {
              createDetails(personaliaDetails);
            }
          }
          else {
            System.err.println("incorrect number of arguments at line with index = " + lineNr);
          }
          lineNr++;
        }
      }
    }
    catch (Exception e) {
      System.err.println("Failed to create Personalia entry: ");
      e.printStackTrace();
    }
  }

  private static void createDetails(PersonaliaDetails details) {
     ParameterContext<PersonaliaDetails> detailsParameter = new ParameterContext<PersonaliaDetails>(getUserContext(), details);
     personalia.create(detailsParameter);
  }

  private static void modifyDetails(PersonaliaDetails details) {
     ParameterContext<PersonaliaDetails> detailsParameter = new ParameterContext<PersonaliaDetails>(getUserContext(), details);
     personalia.modify(detailsParameter);
  }

  private static UserContext getUserContext() {
    return null;
  }

  private static <T> ParameterContext<T> createParameter(T value) {
    return new ParameterContext(getUserContext(), value);
  }

}

