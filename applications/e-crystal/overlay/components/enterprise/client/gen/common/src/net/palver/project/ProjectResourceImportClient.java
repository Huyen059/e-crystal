package net.palver.project;

// expanded with version 3.2.1.4

import java.util.Vector;
import java.util.Collection;
import java.util.StringTokenizer;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.democritus.sys.DataRef;
import net.democritus.sys.CrudsResult;
import net.democritus.sys.UserContext;
import net.democritus.sys.ParameterContext;

import net.democritus.jndi.ComponentJNDI;


// anchor:imports:start
// anchor:imports:end

/**
 * Remote import client to test and perform CRUD with respect,
 * to the business logic of the ProjectResource in the EJB tier
 */

public class ProjectResourceImportClient {

  private static ProjectResourceRemote projectResource = null;


  public static void main(String args[]) {

    Context ctx = null;
    String paramVal = "";
    Integer paramInt = null;
    Long paramLong = null;
    Double paramDouble = null;
    Date paramDate = null;
    Boolean paramBoolean = null;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
    DataRef dataRef = null;
    Vector<DataRef> dataRefVec = null;
    StringTokenizer paramValStrTok = null;

    // Check if there is one argument
    if (args.length != 1) {
      System.err.println("Syntax is : java -cp <classpath> ProjectResourceImportClient file:/'absolutePath'/ProjectResource.csv");
      System.err.println("  using separator token ';' between fields, and '|' inside field enumerations");
      System.err.println("  fields are [employee;project;projectResourceRole;name]");
      System.err.println("  and SimpleDateFormat 'dd-MM-yy HH:mm:ss'");
      System.exit(2);
    }

    try {
      ctx = new InitialContext();
    }
    catch (Exception e) {
      System.err.println("Failed to create initial context: ");
      e.printStackTrace();
    }

    try {
      ComponentJNDI componentJNDI = ComponentJNDI.getComponentJNDI("enterprise");
      String projectResourceName = componentJNDI.getDataRemoteName("ProjectResource");
      projectResource = (ProjectResourceRemote) componentJNDI.lookupRemote(projectResourceName);
    }
    catch (Exception e) {
      System.err.println("Failed to narrow ProjectResourceRemote: ");
      e.printStackTrace();
    }

    int nrFields = 0;
    // anchor:number-of-fields:start
    nrFields = nrFields + 4;
    // anchor:number-of-fields:end

    try {
      int lineNr = 0;

      // open File with URI args[0]
      File aFile = new File(URI.create(args[0]));
      FileReader aFr = new FileReader(aFile);
      BufferedReader aBr = new BufferedReader(aFr);
      String aLine = "";
      String [] lineParts = null;
      while (aLine != null) {
        aLine = aBr.readLine();
        if (aLine != null) {
          lineParts = aLine.split(";", -1);
          if (lineParts.length == nrFields) {
            System.out.println("import CSV row with index " + lineNr);
            ProjectResourceDetails projectResourceDetails = new ProjectResourceDetails();
            // anchor:detail-assignments:start
            paramVal = lineParts[0];
            dataRef = DataRef.withName(paramVal);
            projectResourceDetails.setEmployee(dataRef);
            paramVal = lineParts[1];
            dataRef = DataRef.withName(paramVal);
            projectResourceDetails.setProject(dataRef);
            paramVal = lineParts[2];
            dataRef = DataRef.withName(paramVal);
            projectResourceDetails.setProjectResourceRole(dataRef);
            paramVal = lineParts[3];
            projectResourceDetails.setName(paramVal);
            // anchor:detail-assignments:end
            String name = projectResourceDetails.getName();
            Long id = null;
            try {
              CrudsResult<DataRef> result = projectResource.getId(createParameter(name));
              if (result.isSuccess()) {
                id = result.getValue().getId();
                projectResourceDetails.setId(id);
                modifyDetails(projectResourceDetails);
              } else {
                createDetails(projectResourceDetails);
              }
            }
            catch (Exception e) {
              createDetails(projectResourceDetails);
            }
          }
          else {
            System.err.println("incorrect number of arguments at line with index = " + lineNr);
          }
          lineNr++;
        }
      }
    }
    catch (Exception e) {
      System.err.println("Failed to create ProjectResource entry: ");
      e.printStackTrace();
    }
  }

  private static void createDetails(ProjectResourceDetails details) {
     ParameterContext<ProjectResourceDetails> detailsParameter = new ParameterContext<ProjectResourceDetails>(getUserContext(), details);
     projectResource.create(detailsParameter);
  }

  private static void modifyDetails(ProjectResourceDetails details) {
     ParameterContext<ProjectResourceDetails> detailsParameter = new ParameterContext<ProjectResourceDetails>(getUserContext(), details);
     projectResource.modify(detailsParameter);
  }

  private static UserContext getUserContext() {
    return null;
  }

  private static <T> ParameterContext<T> createParameter(T value) {
    return new ParameterContext(getUserContext(), value);
  }

}

