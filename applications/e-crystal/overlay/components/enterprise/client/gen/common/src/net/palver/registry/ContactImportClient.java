package net.palver.registry;

// expanded with version 3.2.1.4

import java.util.Vector;
import java.util.Collection;
import java.util.StringTokenizer;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.democritus.sys.DataRef;
import net.democritus.sys.CrudsResult;
import net.democritus.sys.UserContext;
import net.democritus.sys.ParameterContext;

import net.democritus.jndi.ComponentJNDI;


// anchor:imports:start
import net.democritus.valuetype.basic.Email;
import net.democritus.valuetype.basic.EmailConverter;
// anchor:imports:end

/**
 * Remote import client to test and perform CRUD with respect,
 * to the business logic of the Contact in the EJB tier
 */

public class ContactImportClient {

  private static ContactRemote contact = null;


  public static void main(String args[]) {

    Context ctx = null;
    String paramVal = "";
    Integer paramInt = null;
    Long paramLong = null;
    Double paramDouble = null;
    Date paramDate = null;
    Boolean paramBoolean = null;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
    DataRef dataRef = null;
    Vector<DataRef> dataRefVec = null;
    StringTokenizer paramValStrTok = null;

    // Check if there is one argument
    if (args.length != 1) {
      System.err.println("Syntax is : java -cp <classpath> ContactImportClient file:/'absolutePath'/Contact.csv");
      System.err.println("  using separator token ';' between fields, and '|' inside field enumerations");
      System.err.println("  fields are [name;address;city;zipCode;country;email;phone]");
      System.err.println("  and SimpleDateFormat 'dd-MM-yy HH:mm:ss'");
      System.exit(2);
    }

    try {
      ctx = new InitialContext();
    }
    catch (Exception e) {
      System.err.println("Failed to create initial context: ");
      e.printStackTrace();
    }

    try {
      ComponentJNDI componentJNDI = ComponentJNDI.getComponentJNDI("enterprise");
      String contactName = componentJNDI.getDataRemoteName("Contact");
      contact = (ContactRemote) componentJNDI.lookupRemote(contactName);
    }
    catch (Exception e) {
      System.err.println("Failed to narrow ContactRemote: ");
      e.printStackTrace();
    }

    int nrFields = 0;
    // anchor:number-of-fields:start
    nrFields = nrFields + 7;
    // anchor:number-of-fields:end

    try {
      int lineNr = 0;

      // open File with URI args[0]
      File aFile = new File(URI.create(args[0]));
      FileReader aFr = new FileReader(aFile);
      BufferedReader aBr = new BufferedReader(aFr);
      String aLine = "";
      String [] lineParts = null;
      while (aLine != null) {
        aLine = aBr.readLine();
        if (aLine != null) {
          lineParts = aLine.split(";", -1);
          if (lineParts.length == nrFields) {
            System.out.println("import CSV row with index " + lineNr);
            ContactDetails contactDetails = new ContactDetails();
            // anchor:detail-assignments:start
            paramVal = lineParts[0];
            contactDetails.setName(paramVal);
            paramVal = lineParts[1];
            contactDetails.setAddress(paramVal);
            paramVal = lineParts[2];
            contactDetails.setCity(paramVal);
            paramVal = lineParts[3];
            contactDetails.setZipCode(paramVal);
            paramVal = lineParts[4];
            dataRef = DataRef.withName(paramVal);
            contactDetails.setCountry(dataRef);
            paramVal = lineParts[5];
            contactDetails.setEmail(new EmailConverter().fromString(paramVal).getValue());
            paramVal = lineParts[6];
            contactDetails.setPhone(paramVal);
            // anchor:detail-assignments:end
            String name = contactDetails.getName();
            Long id = null;
            try {
              CrudsResult<DataRef> result = contact.getId(createParameter(name));
              if (result.isSuccess()) {
                id = result.getValue().getId();
                contactDetails.setId(id);
                modifyDetails(contactDetails);
              } else {
                createDetails(contactDetails);
              }
            }
            catch (Exception e) {
              createDetails(contactDetails);
            }
          }
          else {
            System.err.println("incorrect number of arguments at line with index = " + lineNr);
          }
          lineNr++;
        }
      }
    }
    catch (Exception e) {
      System.err.println("Failed to create Contact entry: ");
      e.printStackTrace();
    }
  }

  private static void createDetails(ContactDetails details) {
     ParameterContext<ContactDetails> detailsParameter = new ParameterContext<ContactDetails>(getUserContext(), details);
     contact.create(detailsParameter);
  }

  private static void modifyDetails(ContactDetails details) {
     ParameterContext<ContactDetails> detailsParameter = new ParameterContext<ContactDetails>(getUserContext(), details);
     contact.modify(detailsParameter);
  }

  private static UserContext getUserContext() {
    return null;
  }

  private static <T> ParameterContext<T> createParameter(T value) {
    return new ParameterContext(getUserContext(), value);
  }

}

