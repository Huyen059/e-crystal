package net.palver.registry;

// expanded with version 3.2.1.4

import java.util.Vector;
import java.util.Collection;
import java.util.StringTokenizer;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.democritus.sys.DataRef;
import net.democritus.sys.CrudsResult;
import net.democritus.sys.UserContext;
import net.democritus.sys.ParameterContext;

import net.democritus.jndi.ComponentJNDI;


// anchor:imports:start
// anchor:imports:end

/**
 * Remote import client to test and perform CRUD with respect,
 * to the business logic of the WorkingMonth in the EJB tier
 */

public class WorkingMonthImportClient {

  private static WorkingMonthRemote workingMonth = null;


  public static void main(String args[]) {

    Context ctx = null;
    String paramVal = "";
    Integer paramInt = null;
    Long paramLong = null;
    Double paramDouble = null;
    Date paramDate = null;
    Boolean paramBoolean = null;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
    DataRef dataRef = null;
    Vector<DataRef> dataRefVec = null;
    StringTokenizer paramValStrTok = null;

    // Check if there is one argument
    if (args.length != 1) {
      System.err.println("Syntax is : java -cp <classpath> WorkingMonthImportClient file:/'absolutePath'/WorkingMonth.csv");
      System.err.println("  using separator token ';' between fields, and '|' inside field enumerations");
      System.err.println("  fields are [month;year;name]");
      System.err.println("  and SimpleDateFormat 'dd-MM-yy HH:mm:ss'");
      System.exit(2);
    }

    try {
      ctx = new InitialContext();
    }
    catch (Exception e) {
      System.err.println("Failed to create initial context: ");
      e.printStackTrace();
    }

    try {
      ComponentJNDI componentJNDI = ComponentJNDI.getComponentJNDI("enterprise");
      String workingMonthName = componentJNDI.getDataRemoteName("WorkingMonth");
      workingMonth = (WorkingMonthRemote) componentJNDI.lookupRemote(workingMonthName);
    }
    catch (Exception e) {
      System.err.println("Failed to narrow WorkingMonthRemote: ");
      e.printStackTrace();
    }

    int nrFields = 0;
    // anchor:number-of-fields:start
    nrFields = nrFields + 3;
    // anchor:number-of-fields:end

    try {
      int lineNr = 0;

      // open File with URI args[0]
      File aFile = new File(URI.create(args[0]));
      FileReader aFr = new FileReader(aFile);
      BufferedReader aBr = new BufferedReader(aFr);
      String aLine = "";
      String [] lineParts = null;
      while (aLine != null) {
        aLine = aBr.readLine();
        if (aLine != null) {
          lineParts = aLine.split(";", -1);
          if (lineParts.length == nrFields) {
            System.out.println("import CSV row with index " + lineNr);
            WorkingMonthDetails workingMonthDetails = new WorkingMonthDetails();
            // anchor:detail-assignments:start
            paramVal = lineParts[0];
            workingMonthDetails.setMonth(paramVal);
            paramVal = lineParts[1];
            dataRef = DataRef.withName(paramVal);
            workingMonthDetails.setYear(dataRef);
            paramVal = lineParts[2];
            workingMonthDetails.setName(paramVal);
            // anchor:detail-assignments:end
            String name = workingMonthDetails.getName();
            Long id = null;
            try {
              CrudsResult<DataRef> result = workingMonth.getId(createParameter(name));
              if (result.isSuccess()) {
                id = result.getValue().getId();
                workingMonthDetails.setId(id);
                modifyDetails(workingMonthDetails);
              } else {
                createDetails(workingMonthDetails);
              }
            }
            catch (Exception e) {
              createDetails(workingMonthDetails);
            }
          }
          else {
            System.err.println("incorrect number of arguments at line with index = " + lineNr);
          }
          lineNr++;
        }
      }
    }
    catch (Exception e) {
      System.err.println("Failed to create WorkingMonth entry: ");
      e.printStackTrace();
    }
  }

  private static void createDetails(WorkingMonthDetails details) {
     ParameterContext<WorkingMonthDetails> detailsParameter = new ParameterContext<WorkingMonthDetails>(getUserContext(), details);
     workingMonth.create(detailsParameter);
  }

  private static void modifyDetails(WorkingMonthDetails details) {
     ParameterContext<WorkingMonthDetails> detailsParameter = new ParameterContext<WorkingMonthDetails>(getUserContext(), details);
     workingMonth.modify(detailsParameter);
  }

  private static UserContext getUserContext() {
    return null;
  }

  private static <T> ParameterContext<T> createParameter(T value) {
    return new ParameterContext(getUserContext(), value);
  }

}

