package net.palver.registry;

// expanded with version 3.2.1.4

import java.util.Vector;
import java.util.Collection;
import java.util.StringTokenizer;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.democritus.sys.DataRef;
import net.democritus.sys.CrudsResult;
import net.democritus.sys.UserContext;
import net.democritus.sys.ParameterContext;

import net.democritus.jndi.ComponentJNDI;


// anchor:imports:start
import net.democritus.valuetype.basic.Iban;
import net.democritus.valuetype.basic.IbanConverter;
// anchor:imports:end

/**
 * Remote import client to test and perform CRUD with respect,
 * to the business logic of the Corporation in the EJB tier
 */

public class CorporationImportClient {

  private static CorporationRemote corporation = null;


  public static void main(String args[]) {

    Context ctx = null;
    String paramVal = "";
    Integer paramInt = null;
    Long paramLong = null;
    Double paramDouble = null;
    Date paramDate = null;
    Boolean paramBoolean = null;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
    DataRef dataRef = null;
    Vector<DataRef> dataRefVec = null;
    StringTokenizer paramValStrTok = null;

    // Check if there is one argument
    if (args.length != 1) {
      System.err.println("Syntax is : java -cp <classpath> CorporationImportClient file:/'absolutePath'/Corporation.csv");
      System.err.println("  using separator token ';' between fields, and '|' inside field enumerations");
      System.err.println("  fields are [name;legalForm;country;vatNumber;bankAccount;mainContact]");
      System.err.println("  and SimpleDateFormat 'dd-MM-yy HH:mm:ss'");
      System.exit(2);
    }

    try {
      ctx = new InitialContext();
    }
    catch (Exception e) {
      System.err.println("Failed to create initial context: ");
      e.printStackTrace();
    }

    try {
      ComponentJNDI componentJNDI = ComponentJNDI.getComponentJNDI("enterprise");
      String corporationName = componentJNDI.getDataRemoteName("Corporation");
      corporation = (CorporationRemote) componentJNDI.lookupRemote(corporationName);
    }
    catch (Exception e) {
      System.err.println("Failed to narrow CorporationRemote: ");
      e.printStackTrace();
    }

    int nrFields = 0;
    // anchor:number-of-fields:start
    nrFields = nrFields + 6;
    // anchor:number-of-fields:end

    try {
      int lineNr = 0;

      // open File with URI args[0]
      File aFile = new File(URI.create(args[0]));
      FileReader aFr = new FileReader(aFile);
      BufferedReader aBr = new BufferedReader(aFr);
      String aLine = "";
      String [] lineParts = null;
      while (aLine != null) {
        aLine = aBr.readLine();
        if (aLine != null) {
          lineParts = aLine.split(";", -1);
          if (lineParts.length == nrFields) {
            System.out.println("import CSV row with index " + lineNr);
            CorporationDetails corporationDetails = new CorporationDetails();
            // anchor:detail-assignments:start
            paramVal = lineParts[0];
            corporationDetails.setName(paramVal);
            paramVal = lineParts[1];
            corporationDetails.setLegalForm(paramVal);
            paramVal = lineParts[2];
            dataRef = DataRef.withName(paramVal);
            corporationDetails.setCountry(dataRef);
            paramVal = lineParts[3];
            corporationDetails.setVatNumber(paramVal);
            paramVal = lineParts[4];
            corporationDetails.setBankAccount(new IbanConverter().fromString(paramVal).getValue());
            paramVal = lineParts[5];
            dataRef = DataRef.withName(paramVal);
            corporationDetails.setMainContact(dataRef);
            // anchor:detail-assignments:end
            String name = corporationDetails.getName();
            Long id = null;
            try {
              CrudsResult<DataRef> result = corporation.getId(createParameter(name));
              if (result.isSuccess()) {
                id = result.getValue().getId();
                corporationDetails.setId(id);
                modifyDetails(corporationDetails);
              } else {
                createDetails(corporationDetails);
              }
            }
            catch (Exception e) {
              createDetails(corporationDetails);
            }
          }
          else {
            System.err.println("incorrect number of arguments at line with index = " + lineNr);
          }
          lineNr++;
        }
      }
    }
    catch (Exception e) {
      System.err.println("Failed to create Corporation entry: ");
      e.printStackTrace();
    }
  }

  private static void createDetails(CorporationDetails details) {
     ParameterContext<CorporationDetails> detailsParameter = new ParameterContext<CorporationDetails>(getUserContext(), details);
     corporation.create(detailsParameter);
  }

  private static void modifyDetails(CorporationDetails details) {
     ParameterContext<CorporationDetails> detailsParameter = new ParameterContext<CorporationDetails>(getUserContext(), details);
     corporation.modify(detailsParameter);
  }

  private static UserContext getUserContext() {
    return null;
  }

  private static <T> ParameterContext<T> createParameter(T value) {
    return new ParameterContext(getUserContext(), value);
  }

}

