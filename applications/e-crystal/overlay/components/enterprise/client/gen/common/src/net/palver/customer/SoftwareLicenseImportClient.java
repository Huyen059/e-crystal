package net.palver.customer;

// expanded with version 3.2.1.4

import java.util.Vector;
import java.util.Collection;
import java.util.StringTokenizer;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.democritus.sys.DataRef;
import net.democritus.sys.CrudsResult;
import net.democritus.sys.UserContext;
import net.democritus.sys.ParameterContext;

import net.democritus.jndi.ComponentJNDI;


// anchor:imports:start
// anchor:imports:end

/**
 * Remote import client to test and perform CRUD with respect,
 * to the business logic of the SoftwareLicense in the EJB tier
 */

public class SoftwareLicenseImportClient {

  private static SoftwareLicenseRemote softwareLicense = null;


  public static void main(String args[]) {

    Context ctx = null;
    String paramVal = "";
    Integer paramInt = null;
    Long paramLong = null;
    Double paramDouble = null;
    Date paramDate = null;
    Boolean paramBoolean = null;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
    DataRef dataRef = null;
    Vector<DataRef> dataRefVec = null;
    StringTokenizer paramValStrTok = null;

    // Check if there is one argument
    if (args.length != 1) {
      System.err.println("Syntax is : java -cp <classpath> SoftwareLicenseImportClient file:/'absolutePath'/SoftwareLicense.csv");
      System.err.println("  using separator token ';' between fields, and '|' inside field enumerations");
      System.err.println("  fields are [softwareLicenseType;client;workingYear;licensePrice;amount]");
      System.err.println("  and SimpleDateFormat 'dd-MM-yy HH:mm:ss'");
      System.exit(2);
    }

    try {
      ctx = new InitialContext();
    }
    catch (Exception e) {
      System.err.println("Failed to create initial context: ");
      e.printStackTrace();
    }

    try {
      ComponentJNDI componentJNDI = ComponentJNDI.getComponentJNDI("enterprise");
      String softwareLicenseName = componentJNDI.getDataRemoteName("SoftwareLicense");
      softwareLicense = (SoftwareLicenseRemote) componentJNDI.lookupRemote(softwareLicenseName);
    }
    catch (Exception e) {
      System.err.println("Failed to narrow SoftwareLicenseRemote: ");
      e.printStackTrace();
    }

    int nrFields = 0;
    // anchor:number-of-fields:start
    nrFields = nrFields + 5;
    // anchor:number-of-fields:end

    try {
      int lineNr = 0;

      // open File with URI args[0]
      File aFile = new File(URI.create(args[0]));
      FileReader aFr = new FileReader(aFile);
      BufferedReader aBr = new BufferedReader(aFr);
      String aLine = "";
      String [] lineParts = null;
      while (aLine != null) {
        aLine = aBr.readLine();
        if (aLine != null) {
          lineParts = aLine.split(";", -1);
          if (lineParts.length == nrFields) {
            System.out.println("import CSV row with index " + lineNr);
            SoftwareLicenseDetails softwareLicenseDetails = new SoftwareLicenseDetails();
            // anchor:detail-assignments:start
            paramVal = lineParts[0];
            dataRef = DataRef.withName(paramVal);
            softwareLicenseDetails.setSoftwareLicenseType(dataRef);
            paramVal = lineParts[1];
            dataRef = DataRef.withName(paramVal);
            softwareLicenseDetails.setClient(dataRef);
            paramVal = lineParts[2];
            dataRef = DataRef.withName(paramVal);
            softwareLicenseDetails.setWorkingYear(dataRef);
            paramVal = lineParts[3];
            try {
              paramInt = new Integer(paramVal);
              softwareLicenseDetails.setLicensePrice(paramInt);
            } catch (Exception e) {
              System.err.println("error assigning Integer: " + e);
              e.printStackTrace();
            }
            paramVal = lineParts[4];
            try {
              paramInt = new Integer(paramVal);
              softwareLicenseDetails.setAmount(paramInt);
            } catch (Exception e) {
              System.err.println("error assigning Integer: " + e);
              e.printStackTrace();
            }
            // anchor:detail-assignments:end
            createDetails(softwareLicenseDetails);
          }
          else {
            System.err.println("incorrect number of arguments at line with index = " + lineNr);
          }
          lineNr++;
        }
      }
    }
    catch (Exception e) {
      System.err.println("Failed to create SoftwareLicense entry: ");
      e.printStackTrace();
    }
  }

  private static void createDetails(SoftwareLicenseDetails details) {
     ParameterContext<SoftwareLicenseDetails> detailsParameter = new ParameterContext<SoftwareLicenseDetails>(getUserContext(), details);
     softwareLicense.create(detailsParameter);
  }

  private static void modifyDetails(SoftwareLicenseDetails details) {
     ParameterContext<SoftwareLicenseDetails> detailsParameter = new ParameterContext<SoftwareLicenseDetails>(getUserContext(), details);
     softwareLicense.modify(detailsParameter);
  }

  private static UserContext getUserContext() {
    return null;
  }

  private static <T> ParameterContext<T> createParameter(T value) {
    return new ParameterContext(getUserContext(), value);
  }

}

