import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

import { ActivityListComponent } from '../../components/activity-list/activity-list.component';

@Component({
  selector: 'app-calendar-page',
  templateUrl: './calendar.page.html',
  styleUrls: ['./calendar.page.scss'],
})
export class CalendarPage implements OnInit {

  @ViewChild(ActivityListComponent, { static: false }) activityList: ActivityListComponent
  activityListProperties: object;
  loading: boolean;
  userId: number;

  constructor(
    private storage: Storage,
  ) {}

  ngOnInit() {
    this.loading = true;
    if (this.userId === undefined) {
      this.storage.get('e-crystal_USER_INFO').then(response => {
        this.userId = response.user.id;
        this.loading = false;
        this.activityListProperties = {
          finderName: "findByControllerEq_DoneEq_timestampLt",
          finderKeyValues: {
            "details.controller.id": response.user.id,
            "details.done": false,
            "details.timestamp.value": new Date().toISOString(),
          },
          sortBy: "timestamp",
        }
      })
    }
  }

  ionViewWillEnter() {
    if (this.activityList !== undefined) {
      this.activityList.refreshActivitys();
    }
  }
}
