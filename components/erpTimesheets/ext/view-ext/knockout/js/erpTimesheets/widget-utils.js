define(function (require) {
    var ko = require('knockout');
    var utils = require('nsx/util/utils');

    function ifAll() {
        var args = arguments;
        return ko.pureComputed(function () {
            for (var i = 0; i < args.length; i++) {
                if (utils.isFalse(args[i])) {
                    return false;
                }
            }

            return true;
        });
    }

    function ifAny() {
        var args = arguments;
        return ko.pureComputed(function () {
            for (var i = 0; i < args.length; i++) {
                if (utils.isTrue(args[i])) {
                    return true;
                }
            }

            return false;
        });
    }

    function ifNone() {
        var args = arguments;
        return ko.pureComputed(function () {
            for (var i = 0; i < args.length; i++) {
                if (utils.isTrue(args[i])) {
                    return false;
                }
            }

            return true;
        });
    }

    return {
        ifAll: ifAll,
        ifAny: ifAny,
        ifNone: ifNone
    }
});