package org.normalizedsystems.nsx.erp;

import net.democritus.sys.Context;
import net.democritus.sys.CrudsResult;
import net.democritus.sys.DataRef;
import net.democritus.sys.command.CommandResult;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.chrono.ChronoZonedDateTime;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.Boolean.TRUE;

public class CopyEntryCommandExtension {

  private final Context context;

  public CopyEntryCommandExtension(Context context) {
    this.context = context;
  }

  public CommandResult copyEntry(TimesheetEntryDetails entry, TimesheetEntryCommand.CopyEntry command) {
    final TimesheetEntryLocalAgent agent = TimesheetEntryLocalAgent.getTimesheetEntryAgent(context);

    final List<Date> newDates = calculateValidDates(entry, command);
    for (Date newDate : newDates) {
      entry.setId(0L);
      entry.setName(null);
      entry.setDate(newDate);

      final CrudsResult<DataRef> createResult = agent.create(entry);
      if (createResult.isError()) {
        return CommandResult.error(command.getCommandId(), "Failed to create entry on date " + newDate);
      }
    }

    return CommandResult.success(command.getCommandId());
  }

  /**
   * Calculates a set of dates defined by the command parameters, which are within the same month of the timesheet
   * entry, as well as not equal to the date of that entry.
   *
   * @param entry
   * @param command
   * @return
   */
  private List<Date> calculateValidDates(TimesheetEntryDetails entry, TimesheetEntryCommand.CopyEntry command) {
    if (entry.getDate() == null || command.getStartDate() == null) {
      return Collections.emptyList();
    }
    final ZonedDateTime entryDate = entry.getDate().toInstant().atZone(ZoneId.systemDefault()).truncatedTo(ChronoUnit.DAYS);
    final ZonedDateTime startDate = command.getStartDate().toInstant().atZone(ZoneId.systemDefault()).truncatedTo(ChronoUnit.DAYS);
    final ZonedDateTime endDate = command.getEndDate() == null
        ? startDate
        : command.getEndDate().toInstant().atZone(ZoneId.systemDefault()).truncatedTo(ChronoUnit.DAYS);
    if (startDate.isAfter(endDate)) {
      // We expect the range of dates to run forward in time
      return Collections.emptyList();
    }

    return Stream.iterate(startDate, d -> d.plus(1, ChronoUnit.DAYS))
        .limit(ChronoUnit.DAYS.between(startDate, endDate) + 1)
        .filter(d -> !d.equals(entryDate))
        .filter(d -> isSameMonth(d, entryDate))
        .filter(d -> isDayAllowed(d, TRUE.equals(command.getIncludeWeekends())))
        .map(ChronoZonedDateTime::toInstant)
        .map(Date::from)
        .collect(Collectors.toList());
  }

  private boolean isSameMonth(ZonedDateTime date1, ZonedDateTime date2) {
    return YearMonth.from(date1).equals(YearMonth.from(date2));
  }

  private boolean isDayAllowed(ZonedDateTime date, boolean includeWeekend) {
    return includeWeekend || date.get(ChronoField.DAY_OF_WEEK) < DayOfWeek.SATURDAY.getValue();
  }

}
