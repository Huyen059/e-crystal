package org.normalizedsystems.nsx.erp;

import net.democritus.state.StateUpdate;
import net.democritus.sys.CrudsResult;
import net.democritus.sys.DataRef;
import net.democritus.sys.ParameterContext;

public class TimesheetStateUpdater {

  private TimesheetCrudsLocal timesheetCruds;
  private TimesheetEntryCrudsLocal timesheetEntryCruds;
  private ExpenseCrudsLocal expenseCruds;
  private ExpenseDocumentCrudsLocal expenseDocumentCruds;

  TimesheetStateUpdater(TimesheetCrudsLocal timesheetCruds, TimesheetEntryCrudsLocal timesheetEntryCruds,
                  ExpenseCrudsLocal expenseCruds, ExpenseDocumentCrudsLocal expenseDocumentCruds) {
    this.timesheetCruds = timesheetCruds;
    this.timesheetEntryCruds = timesheetEntryCruds;
    this.expenseCruds = expenseCruds;
    this.expenseDocumentCruds = expenseDocumentCruds;
  }

  public CrudsResult<Void> updateTimesheetState(ParameterContext<DataRef> refParameter,
                                                TimesheetState currentState,
                                                TimesheetState newState) {
    StateUpdate stateUpdate = new StateUpdate();
    stateUpdate.setExpectedStatus(currentState.getStatus());
    stateUpdate.setTargetStatus(newState.getStatus());
    stateUpdate.setTarget(refParameter.getValue());

    return timesheetCruds.compareAndSetStatus(refParameter.construct(stateUpdate));
  }

  public CrudsResult<Void> updateTimesheetDetailsState(ParameterContext<TimesheetDetails> detailsParameter,
                                                       TimesheetState currentState,
                                                       TimesheetState newState) {
    return updateTimesheetState(detailsParameter.construct(detailsParameter.getValue().getDataRef()),
        currentState, newState);
  }

  public CrudsResult<Void> updateTimesheetEntryState(ParameterContext<DataRef> refParameter,
                                                     TimesheetEntryState currentState,
                                                     TimesheetEntryState newState) {
    StateUpdate stateUpdate = new StateUpdate();
    stateUpdate.setExpectedStatus(currentState.getStatus());
    stateUpdate.setTargetStatus(newState.getStatus());
    stateUpdate.setTarget(refParameter.getValue());

    return timesheetEntryCruds.compareAndSetStatus(refParameter.construct(stateUpdate));
  }

  public CrudsResult<Void> updateExpenseState(ParameterContext<DataRef> refParameter,
                                              ExpenseState currentState,
                                              ExpenseState newState) {
    StateUpdate stateUpdate = new StateUpdate();
    stateUpdate.setExpectedStatus(currentState.getStatus());
    stateUpdate.setTargetStatus(newState.getStatus());
    stateUpdate.setTarget(refParameter.getValue());

    return expenseCruds.compareAndSetStatus(refParameter.construct(stateUpdate));
  }

  public CrudsResult<Void> updateExpenseDocumentState(ParameterContext<DataRef> refParameter,
                                                      ExpenseDocumentState currentState,
                                                      ExpenseDocumentState newState) {
    StateUpdate stateUpdate = new StateUpdate();
    stateUpdate.setExpectedStatus(currentState.getStatus());
    stateUpdate.setTargetStatus(newState.getStatus());
    stateUpdate.setTarget(refParameter.getValue());

    return expenseDocumentCruds.compareAndSetStatus(refParameter.construct(stateUpdate));
  }

}
