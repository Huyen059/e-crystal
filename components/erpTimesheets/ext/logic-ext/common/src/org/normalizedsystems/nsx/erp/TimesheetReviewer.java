package org.normalizedsystems.nsx.erp;

import net.democritus.sys.*;
import net.democritus.sys.search.SearchDetails;

/**
 * This class can be used to close an open timesheet.
 */
class TimesheetReviewer {

  private TimesheetCrudsLocal timesheetCruds;
  private TimesheetEntryCrudsLocal timesheetEntryCruds;
  private ExpenseCrudsLocal expenseCruds;
  private ExpenseDocumentCrudsLocal expenseDocumentCruds;

  private TimesheetStateUpdater stateUpdater;

  TimesheetReviewer(TimesheetCrudsLocal timesheetCruds, TimesheetEntryCrudsLocal timesheetEntryCruds,
                           ExpenseCrudsLocal expenseCruds, ExpenseDocumentCrudsLocal expenseDocumentCruds) {
    this.timesheetCruds = timesheetCruds;
    this.timesheetEntryCruds = timesheetEntryCruds;
    this.expenseCruds = expenseCruds;
    this.expenseDocumentCruds = expenseDocumentCruds;

    this.stateUpdater = new TimesheetStateUpdater(
        timesheetCruds, timesheetEntryCruds, expenseCruds, expenseDocumentCruds);
  }

  TaskResult<Void> reviewTimesheet(ParameterContext<TimesheetDetails> timesheetParameter) {
    TimesheetDetails timesheetDetails = timesheetParameter.getValue();

    if (!TimesheetState.REVIEWING.equals(timesheetDetails.getStatusAsEnum())) {
      return TaskResult.error(Diagnostic.error("erpTimesheets", "timesheet", "notReviewingException"));
    }

    // Update status of timesheet
    CrudsResult<Void> crudsResult = stateUpdater.updateTimesheetDetailsState(timesheetParameter,
        TimesheetState.REVIEWING, TimesheetState.REVIEWED);
    if (crudsResult.isError()) {
      return TaskResult.error(crudsResult.getDiagnostics());
    }

    // Update expenses
    TimesheetEntryFindByTimesheetEq_StatusEqDetails timesheetEntryFinder = new TimesheetEntryFindByTimesheetEq_StatusEqDetails();
    timesheetEntryFinder.setTimesheet(timesheetDetails.getDataRef());
    timesheetEntryFinder.setStatus(TimesheetEntryState.CLOSED.getStatus());

    SearchResult<DataRef> searchResult = timesheetEntryCruds.find(timesheetParameter.construct(
        SearchDetails.fetchAllDataRef(timesheetEntryFinder)));
    if (searchResult.isError()) {
      return TaskResult.error(searchResult.getDiagnostics());
    }

    // Close timesheet entries
    for (DataRef timesheetEntry : searchResult.getResults()) {
      TaskResult<Void> closeResult = reviewTimesheetEntry(timesheetParameter.construct(timesheetEntry));
      if (closeResult.isError()) {
        return TaskResult.error(closeResult.getDiagnostics());
      }
    }

    return TaskResult.success();
  }

  private TaskResult<Void> reviewTimesheetEntry(ParameterContext<DataRef> timesheetEntryParameter) {
    DataRef timesheetEntryRef = timesheetEntryParameter.getValue();

    // Update status of timesheet entry
    CrudsResult<Void> crudsResult = stateUpdater.updateTimesheetEntryState(timesheetEntryParameter,
        TimesheetEntryState.CLOSED, TimesheetEntryState.LOCKED);
    if (crudsResult.isError()) {
      return TaskResult.error(crudsResult.getDiagnostics());
    }

    // Update expenses
    ExpenseFindByTimesheetEntryEq_StatusEqDetails expenseFinder = new ExpenseFindByTimesheetEntryEq_StatusEqDetails();
    expenseFinder.setTimesheetEntry(timesheetEntryRef);
    expenseFinder.setStatus(ExpenseState.CLOSED.getStatus());

    SearchResult<DataRef> searchResult = expenseCruds.find(timesheetEntryParameter.construct(
        SearchDetails.fetchAllDataRef(expenseFinder)));
    if (searchResult.isError()) {
      return TaskResult.error(searchResult.getDiagnostics());
    }

    for (DataRef expenseRef : searchResult.getResults()) {
      TaskResult<Void> closeResult = reviewExpense(timesheetEntryParameter.construct(expenseRef));
      if (closeResult.isError()) {
        return TaskResult.error(closeResult.getDiagnostics());
      }
    }

    return TaskResult.success();
  }

  private TaskResult<Void> reviewExpense(ParameterContext<DataRef> expenseParameter) {
    DataRef expenseRef = expenseParameter.getValue();

    // Update status of expense
    CrudsResult<Void> crudsResult = stateUpdater.updateExpenseState(expenseParameter,
        ExpenseState.CLOSED, ExpenseState.LOCKED);
    if (crudsResult.isError()) {
      return TaskResult.error(crudsResult.getDiagnostics());
    }

    // Update expenses
    ExpenseDocumentFindByExpenseEq_StatusEqDetails expenseDocumentFinder = new ExpenseDocumentFindByExpenseEq_StatusEqDetails();
    expenseDocumentFinder.setExpense(expenseRef);
    expenseDocumentFinder.setStatus(ExpenseDocumentState.CLOSED.getStatus());

    SearchResult<DataRef> searchResult = expenseDocumentCruds.find(expenseParameter.construct(
        SearchDetails.fetchAllDataRef(expenseDocumentFinder)));
    if (searchResult.isError()) {
      return TaskResult.error(searchResult.getDiagnostics());
    }

    for (DataRef expenseDocumentRef : searchResult.getResults()) {
      TaskResult<Void> closeResult = reviewExpenseDocument(expenseParameter.construct(expenseDocumentRef));
      if (closeResult.isError()) {
        return TaskResult.error(closeResult.getDiagnostics());
      }
    }

    return TaskResult.success();
  }

  private TaskResult<Void> reviewExpenseDocument(ParameterContext<DataRef> expenseDocumentParameter) {
    // Update status of expense document
    CrudsResult<Void> crudsResult = stateUpdater.updateExpenseDocumentState(expenseDocumentParameter,
        ExpenseDocumentState.CLOSED, ExpenseDocumentState.LOCKED);
    if (crudsResult.isError()) {
      return TaskResult.error(crudsResult.getDiagnostics());
    }

    return TaskResult.success();
  }

}
