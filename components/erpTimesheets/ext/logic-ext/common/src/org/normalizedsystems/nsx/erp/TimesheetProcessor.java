package org.normalizedsystems.nsx.erp;

import net.democritus.sys.*;
import net.democritus.sys.search.SearchDetails;

/**
 * This class can be used to close an open timesheet.
 */
class TimesheetProcessor {

  private TimesheetCrudsLocal timesheetCruds;
  private TimesheetEntryCrudsLocal timesheetEntryCruds;
  private ExpenseCrudsLocal expenseCruds;
  private ExpenseDocumentCrudsLocal expenseDocumentCruds;

  private TimesheetStateUpdater stateUpdater;

  TimesheetProcessor(TimesheetCrudsLocal timesheetCruds, TimesheetEntryCrudsLocal timesheetEntryCruds,
                     ExpenseCrudsLocal expenseCruds, ExpenseDocumentCrudsLocal expenseDocumentCruds) {
    this.timesheetCruds = timesheetCruds;
    this.timesheetEntryCruds = timesheetEntryCruds;
    this.expenseCruds = expenseCruds;
    this.expenseDocumentCruds = expenseDocumentCruds;

    this.stateUpdater = new TimesheetStateUpdater(
        timesheetCruds, timesheetEntryCruds, expenseCruds, expenseDocumentCruds);
  }

  TaskResult<Void> processTimesheet(ParameterContext<TimesheetDetails> timesheetParameter) {
    TimesheetDetails timesheetDetails = timesheetParameter.getValue();

    if (!TimesheetState.REVIEWED.equals(timesheetDetails.getStatusAsEnum())) {
      return TaskResult.error(Diagnostic.error("erpTimesheets", "timesheet", "cantProcessException"));
    }

    // Update status of timesheet
    CrudsResult<Void> crudsResult = stateUpdater.updateTimesheetDetailsState(timesheetParameter,
        TimesheetState.REVIEWED, TimesheetState.PROCESSED);
    if (crudsResult.isError()) {
      return TaskResult.error(crudsResult.getDiagnostics());
    }

    return TaskResult.success();
  }

}
