package org.normalizedsystems.nsx.erp;

import net.democritus.state.StateUpdate;
import net.democritus.sys.*;
import net.democritus.sys.search.SearchDetails;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class can be used to close an open timesheet.
 */
class TimesheetReviewStarter {

  private TimesheetCrudsLocal timesheetCruds;
  private TimesheetEntryCrudsLocal timesheetEntryCruds;
  private ExpenseCrudsLocal expenseCruds;
  private ExpenseDocumentCrudsLocal expenseDocumentCruds;

  TimesheetReviewStarter(TimesheetCrudsLocal timesheetCruds, TimesheetEntryCrudsLocal timesheetEntryCruds,
                         ExpenseCrudsLocal expenseCruds, ExpenseDocumentCrudsLocal expenseDocumentCruds) {
    this.timesheetCruds = timesheetCruds;
    this.timesheetEntryCruds = timesheetEntryCruds;
    this.expenseCruds = expenseCruds;
    this.expenseDocumentCruds = expenseDocumentCruds;
  }

  TaskResult<Void> startReviewTimesheet(ParameterContext<TimesheetDetails> timesheetParameter) {
    TimesheetDetails timesheetDetails = timesheetParameter.getValue();

    if (!TimesheetState.CLOSED.equals(timesheetDetails.getStatusAsEnum())) {
      return TaskResult.error(Diagnostic.error("erpTimesheets", "timesheet", "cantStartReviewException"));
    }

    // Update expenses
    TimesheetEntryFindByTimesheetEq_StatusEqDetails timesheetEntryFinder = new TimesheetEntryFindByTimesheetEq_StatusEqDetails();
    timesheetEntryFinder.setTimesheet(timesheetDetails.getDataRef());
    timesheetEntryFinder.setStatus(TimesheetEntryState.CLOSED.getStatus());

    SearchResult<TimesheetEntryDetails> searchResult = timesheetEntryCruds.find(timesheetParameter.construct(
        SearchDetails.fetchAllDetails(timesheetEntryFinder)));
    if (searchResult.isError()) {
      return TaskResult.error(searchResult.getDiagnostics());
    }

    // Perform validation
    TaskResult<Void> validationResult = validateTimesheetEntries(timesheetParameter.construct(searchResult.getResults()));
    if (validationResult.isError()) {
      return TaskResult.error(validationResult.getDiagnostics());
    }

    // Update status of timesheet parameter
    StateUpdate stateUpdate = new StateUpdate();
    stateUpdate.setExpectedStatus(TimesheetState.CLOSED.getStatus());
    stateUpdate.setTargetStatus(TimesheetState.REVIEWING.getStatus());
    stateUpdate.setTarget(timesheetDetails.getDataRef());

    CrudsResult<Void> crudsResult = timesheetCruds.compareAndSetStatus(timesheetParameter.construct(stateUpdate));
    if (crudsResult.isError()) {
      return TaskResult.error(crudsResult.getDiagnostics());
    }

    return TaskResult.success();
  }

  private TaskResult<Void> validateTimesheetEntries(ParameterContext<List<TimesheetEntryDetails>> timesheetEntriesParameter) {
    List<TimesheetEntryDetails> entries = timesheetEntriesParameter.getValue();
    Map<Integer, Double> hoursPerDay = new HashMap<Integer, Double>();

    // Initialize map
    for (int day = 1; day <= 31; day++) {
      hoursPerDay.put(day, 0D);
    }

    Calendar calendar = Calendar.getInstance();
    for (TimesheetEntryDetails entry : entries) {
      calendar.setTime(entry.getDate());
      int day = calendar.get(Calendar.DAY_OF_MONTH);

      // Increment hours for the day specified by the entry
      hoursPerDay.put(day, hoursPerDay.get(day) + entry.getHours());
    }

    // Verify that for all days no more than 24 hours have been logged
    for (Map.Entry<Integer, Double> dayEntry : hoursPerDay.entrySet()) {
      if (dayEntry.getValue() > 24) {
        return TaskResult.error(Diagnostic.error("erpTimesheets", "timesheet", "invalidHoursInDayException"));
      }
    }

    return TaskResult.success();
  }

}
