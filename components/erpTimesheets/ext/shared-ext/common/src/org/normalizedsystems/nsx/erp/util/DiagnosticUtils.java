package org.normalizedsystems.nsx.erp.util;

import net.democritus.sys.Diagnostic;
import net.democritus.sys.command.CommandResult;
import net.democritus.sys.command.ICommand;

import java.util.Collection;

public class DiagnosticUtils {

  public static CommandResult createCommandError(ICommand command, Collection<Diagnostic> diagnostics,
                                                 String defaultMessage) {
    String message = defaultMessage;
    for (Diagnostic diagnostic : diagnostics) {
      if (!diagnostic.isSuccess()) {
        // If the diagnostic has a key, get the qualified name + the message key
        if (diagnostic.hasKey()) {
          message = diagnostic.getQualifiedName() + "." + diagnostic.getKey();
        }

        // We found something that isn't a success to return as an error
        break;
      }
    }

    return CommandResult.error(command, message);
  }

  public static CommandResult createCommandError(ICommand command, Collection<Diagnostic> diagnostics) {
    return createCommandError(command, diagnostics, "error");
  }

}
