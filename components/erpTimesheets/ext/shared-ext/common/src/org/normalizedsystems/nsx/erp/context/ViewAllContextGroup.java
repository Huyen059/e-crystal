package org.normalizedsystems.nsx.erp.context;

import net.democritus.sys.ContextGroup;

public class ViewAllContextGroup implements ContextGroup {

  private boolean viewAll = false;

  public ViewAllContextGroup(boolean viewAll) {
    this.viewAll = viewAll;
  }

  public boolean canViewAll() {
    return viewAll;
  }
}
