package org.normalizedsystems.nsx.erp.util;

public class TimesheetUtils {
  /**
   * Formats the display name of a timesheet instance.
   *
   * @param userName The username to add to the display name.
   * @param year The year of the timesheet.
   * @param month The month of the timesheet.
   * @return The display name of the timesheet.
   */
  public static String formatDisplayName(String userName, Integer year, Integer month) {
    return String.format("%04d%02d_%s", year, month, userName);
  }
}
