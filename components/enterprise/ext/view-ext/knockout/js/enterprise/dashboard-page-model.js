define(function (require) {
  var ko = require('knockout');
  var utils = require('nsx/util/utils');
  var translate = require('nsx/util/text-utils').translate;

  var Menu = require('nsx/menu');
  var Page = require('nsx/injectors/page-segment');
  var intentHandlerLoader = require('nsx/config/intent-handler').getIntentHandlerLoader();

  var ApplicationProperties = require('nsx/metadata/nsx-application-properties');

  function buildPage() {
    var template = "enterprise/dashboard/dashboard-page-layout";
    var viewModel = {};
    var title = "Dashboard";
    var menuName = "e-crystal";
    var menuAnchor = "navigation";
    var menuTemplate = "nsx/knockout/navigation-bar";


    Menu.defineMenu({
      title: title,
      selector: menuAnchor,
      menuName: menuName,
      view: menuTemplate
    });

    var page = Page.definePageSegment({
      selector: "page",
      view: template,
      viewModel: viewModel
    });

    intentHandlerLoader.startListening();
    ApplicationProperties.defineHelperButton();
  }

  return {
    buildPage: buildPage
  }

});
