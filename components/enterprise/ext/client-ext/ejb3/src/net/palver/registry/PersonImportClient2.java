package net.palver.registry;

// expanded with version 3.2.1.4

import java.util.Vector;
import java.util.Collection;
import java.util.StringTokenizer;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.democritus.sys.DataRef;
import net.democritus.sys.CrudsResult;
import net.democritus.sys.UserContext;
import net.democritus.sys.ParameterContext;

import net.democritus.jndi.ComponentJNDI;


// anchor:imports:start
import java.util.Date;
// anchor:imports:end

/**
 * Remote import client to test and perform CRUD with respect,
 * to the business logic of the Person in the EJB tier
 */

public class PersonImportClient2 {

  private static PersonRemote person = null;


  public static void main(String args[]) {

    Context ctx = null;
    String paramVal = "";
    Integer paramInt = null;
    Long paramLong = null;
    Double paramDouble = null;
    Date paramDate = null;
    Boolean paramBoolean = null;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    DataRef dataRef = null;
    Vector<DataRef> dataRefVec = null;
    StringTokenizer paramValStrTok = null;

    // Check if there is one argument
    if (args.length != 1) {
      System.err.println("Syntax is : java -cp <classpath> PersonImportClient file:/'absolutePath'/Person.csv");
      System.err.println("  using separator token ';' between fields, and '|' inside field enumerations");
      System.err.println("  fields are [name;firstName;lastName;dateOfBirth;privateContact;personalia]");
      System.err.println("  and SimpleDateFormat 'dd-MM-yy HH:mm:ss'");
      System.exit(2);
    }

    try {
      ctx = new InitialContext();
    }
    catch (Exception e) {
      System.err.println("Failed to create initial context: ");
      e.printStackTrace();
    }

    try {
      ComponentJNDI componentJNDI = ComponentJNDI.getComponentJNDI("enterprise");
      String personName = componentJNDI.getDataRemoteName("Person");
      person = (PersonRemote) componentJNDI.lookupRemote(personName);
    }
    catch (Exception e) {
      System.err.println("Failed to narrow PersonRemote: ");
      e.printStackTrace();
    }

    int nrFields = 0;
    // anchor:number-of-fields:start
    nrFields = nrFields + 6;
    // anchor:number-of-fields:end

    try {
      int lineNr = 0;

      // open File with URI args[0]
      File aFile = new File(URI.create(args[0]));
      FileReader aFr = new FileReader(aFile);
      BufferedReader aBr = new BufferedReader(aFr);
      String aLine = "";
      String [] lineParts = null;
      while (aLine != null) {
        aLine = aBr.readLine();
        if (aLine != null) {
          lineParts = aLine.split(";", -1);
          if (lineParts.length == nrFields) {
            System.out.println("import CSV row with index " + lineNr);
            PersonDetails personDetails = new PersonDetails();
            // anchor:detail-assignments:start
            paramVal = lineParts[0];
            personDetails.setName(paramVal);
            paramVal = lineParts[1];
            personDetails.setFirstName(paramVal);
            paramVal = lineParts[2];
            personDetails.setLastName(paramVal);
            paramVal = lineParts[3];
            try {
              paramDate = sdf.parse(paramVal);
              personDetails.setDateOfBirth(paramDate);
            } catch (Exception e) {
              System.err.println("error assigning Date: " + e + ", new Date(0) will be used");
              //e.printStackTrace();
              paramDate = new Date(0);
              personDetails.setDateOfBirth(paramDate);
            }
            paramVal = lineParts[4];
            dataRef = DataRef.withName(paramVal);
            personDetails.setPrivateContact(dataRef);
            paramVal = lineParts[5];
            dataRef = DataRef.withName(paramVal);
            personDetails.setPersonalia(dataRef);
            // anchor:detail-assignments:end
            String name = personDetails.getName();
            Long id = null;
            try {
              CrudsResult<DataRef> result = person.getId(createParameter(name));
              if (result.isSuccess()) {
                id = result.getValue().getId();
                personDetails.setId(id);
                modifyDetails(personDetails);
              } else {
                createDetails(personDetails);
              }
            }
            catch (Exception e) {
              createDetails(personDetails);
            }
          }
          else {
            System.err.println("incorrect number of arguments at line with index = " + lineNr);
          }
          lineNr++;
        }
      }
    }
    catch (Exception e) {
      System.err.println("Failed to create Person entry: ");
      e.printStackTrace();
    }
  }

  private static void createDetails(PersonDetails details) {
     ParameterContext<PersonDetails> detailsParameter = new ParameterContext<PersonDetails>(getUserContext(), details);
     person.create(detailsParameter);
  }

  private static void modifyDetails(PersonDetails details) {
     ParameterContext<PersonDetails> detailsParameter = new ParameterContext<PersonDetails>(getUserContext(), details);
     person.modify(detailsParameter);
  }

  private static UserContext getUserContext() {
    return null;
  }

  private static <T> ParameterContext<T> createParameter(T value) {
    return new ParameterContext(getUserContext(), value);
  }

}

