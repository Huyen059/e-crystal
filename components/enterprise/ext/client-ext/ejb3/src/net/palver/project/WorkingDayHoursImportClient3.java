package net.palver.project;

// expanded with version 3.2.1

import java.util.Vector;
import java.util.Collection;
import java.util.StringTokenizer;
import java.util.List;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.HashMap;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.democritus.sys.DataRef;
import net.democritus.sys.CrudsResult;
import net.democritus.sys.UserContext;
import net.democritus.sys.BasicUserContext;
import net.democritus.sys.ParameterContext;
import net.democritus.usr.ExtendedUserContext;
import net.democritus.usr.UserDetails;
import java.util.HashSet;

import net.democritus.jndi.ComponentJNDI;

import net.democritus.sys.SearchResult;
import net.democritus.sys.search.SearchDetails;
import net.palver.resource.EmployeeAgent;

// anchor:imports:start
import java.util.Date;
// anchor:imports:end

/**
 * Remote import client to test and perform CRUD with respect,
 * to the business logic of the WorkingDayHours in the EJB tier
 */

public class WorkingDayHoursImportClient3 {

  private static WorkingDayHoursRemote workingDayHours = null;
  private static WorkingDayHoursAgent workingDayHoursAgent = WorkingDayHoursAgent.getWorkingDayHoursAgent(getUserContext());
  private static WorkingMonthHoursAgent workingMonthHoursAgent = WorkingMonthHoursAgent.getWorkingMonthHoursAgent(getUserContext());
  private static ProjectResourceAgent projectResourceAgent = ProjectResourceAgent.getProjectResourceAgent(getUserContext());
  private static ProjectAgent projectAgent = ProjectAgent.getProjectAgent(getUserContext());
  private static EmployeeAgent employeeAgent = EmployeeAgent.getEmployeeAgent(getUserContext());

  public static void main(String args[]) {

    Context ctx = null;
    String paramVal = "";
    Integer paramInt = null;
    Long paramLong = null;
    Double paramDouble = null;
    Date paramDate = null;
    Boolean paramBoolean = null;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
    DataRef dataRef = null;
    Vector<DataRef> dataRefVec = null;
    StringTokenizer paramValStrTok = null;
    Hashtable projectIds = new Hashtable();
    ArrayList<String> projectCodes = new ArrayList<String>();
    ArrayList<String> projectNames = new ArrayList<String>();
    ArrayList<String> projectTypes = new ArrayList<String>();
    ArrayList<String> projectInfos = new ArrayList<String>();

    ArrayList<Long> janResources = new ArrayList<Long>(); ArrayList<Double> janHours = new ArrayList<Double>();
    ArrayList<Long> febResources = new ArrayList<Long>(); ArrayList<Double> febHours = new ArrayList<Double>();
    ArrayList<Long> marResources = new ArrayList<Long>(); ArrayList<Double> marHours = new ArrayList<Double>();
    ArrayList<Long> aprResources = new ArrayList<Long>(); ArrayList<Double> aprHours = new ArrayList<Double>();
    //ArrayList<Long> monthResources = new ArrayList<Long>(); ArrayList<Double> monthHours = new ArrayList<Double>();

    String actualMonth = System.getenv("ACTUAL_MONTH");
    String theYear = actualMonth.substring(0,4);
    String theMonth = actualMonth.substring(4, 6);
    String theMonthName = actualMonth.substring(6);
    ArrayList<String> monthCodeList = new ArrayList<String>(0);
    ArrayList<String> monthNameList = new ArrayList<String>(0);
    ArrayList<ArrayList<Long>> monthResources = new ArrayList<ArrayList<Long>>(); 
    ArrayList<ArrayList<Double>> monthHours = new ArrayList<ArrayList<Double>>();
    for (int i=0; i < 1; i++) {
      monthCodeList.add("1/"+theMonth+"/"+theYear);
      monthNameList.add(theMonthName+":"+theYear);
      monthResources.add(new ArrayList<Long>());
      monthHours.add(new ArrayList<Double>());
    }
    String testString = ""; int theLine = 0;

    // Check if there is one argument
    if (args.length != 1) {
      System.err.println("Syntax is : java -cp <classpath> WorkingDayHoursImportClient file:/'absolutePath'/WorkingDayHours.csv");
      System.err.println("  using separator token ';' between fields, and '|' inside field enumerations");
      System.err.println("  fields are [projectResource;workingDay;hoursPerformed;description]");
      System.err.println("  and SimpleDateFormat 'dd-MM-yy HH:mm:ss'");
      System.exit(2);
    }

    try {
      ctx = new InitialContext();
    }
    catch (Exception e) {
      System.err.println("Failed to create initial context: ");
      e.printStackTrace();
    }

    try {
      ComponentJNDI componentJNDI = ComponentJNDI.getComponentJNDI("enterprise");
      String workingDayHoursName = componentJNDI.getDataRemoteName("WorkingDayHours");
      workingDayHours = (WorkingDayHoursRemote) componentJNDI.lookupRemote(workingDayHoursName);
    }
    catch (Exception e) {
      System.err.println("Failed to narrow WorkingDayHoursRemote: ");
      e.printStackTrace();
    }

    try {
      int lineNr = 0;
      File aFile = new File(URI.create(System.getenv("CSV_DATA_URI")+"/actuals/ProjectMapping.csv"));
      FileReader aFr = new FileReader(aFile);
      BufferedReader aBr = new BufferedReader(aFr);
      String aLine = "";
      String [] lineParts = null;
      while (aLine != null) {
        aLine = aBr.readLine();
        if (aLine != null) {
          lineParts = aLine.split(";", -1);
          if (lineParts.length == 5) {
            System.out.println("Putting ProjectId " + lineParts[0] + " at position " + lineNr);
            projectIds.put(new String(lineParts[0]), new Integer(lineNr));
            projectCodes.add(lineParts[1]);
            projectNames.add(lineParts[2]);
            projectTypes.add(lineParts[3]);
            projectInfos.add(lineParts[4]);
          }
          else {
            System.err.println("incorrect number of arguments at line with index = " + lineNr);
          }
          lineNr++;
        }
      }
    }
    catch (Exception e) {
      System.err.println("Failed to read project mappings: ");
      e.printStackTrace();
    }

    int nrFields = 0;
    // anchor:number-of-fields:start
    nrFields = nrFields + 4;
    // anchor:number-of-fields:end
    nrFields = 8;
    SimpleDateFormat ssdf = new SimpleDateFormat("dd/MM/yyyy");

    try {
      int lineNr = 0;

      // open File with URI args[0]
      String theURI = System.getenv("CSV_DATA_URI") + "/actuals/WorkingDayActuals" + theYear + theMonth + ".csv";
      System.out.println("THE ACTUALS URI: [" + theURI + "]");
      //File aFile = new File(URI.create("file:/C:/NSF-3.0/workspace/microERP/components/enterprise/data/csv/WorkingDayActuals.csv"));
      File aFile = new File(URI.create(theURI));
      FileReader aFr = new FileReader(aFile);
      BufferedReader aBr = new BufferedReader(aFr);
      String aLine = "";
      String [] lineParts = null;
      while (aLine != null) {
        aLine = aBr.readLine();
        if (aLine != null) {
          lineParts = aLine.split(";", -1);
          if (lineParts.length == nrFields) {
            if (lineParts[3].equals("2018_NSX_OTher")) lineParts[3] = "2018_NSX_Other";
            if (projectIds.containsKey(lineParts[3])) {
              int projectIndex = ((Integer) projectIds.get(lineParts[3])).intValue();
              String employeeName = lineParts[1];
              if (employeeName.equals("Jimmy raets")) employeeName = "Jimmy Raets";
              String dateString = lineParts[2]; 
              if (dateString.substring(1,2).equals("/")) dateString = "0" + dateString;
              if (dateString.substring(4,5).equals("/")) dateString = dateString.substring(0,3) + "0" + dateString.substring(3);
              Date workingDay = ssdf.parse(dateString);
              String projectName = projectNames.get(projectIndex);
             if (!(projectName.equals("ABSENCE")) && !(projectName.equals("HOLIDAY")) && !(projectName.equals(""))) {
              ProjectResourceFindByEmployeeEq_ProjectEqDetails resourceFinder = new ProjectResourceFindByEmployeeEq_ProjectEqDetails();
              DataRef employeeRef = DataRef.withName(employeeName);
              Long refId = employeeAgent.getId(employeeName);
              if (refId == null) System.out.println("NOT ABLE TO GET ID FOR EMPLOYEE [" + employeeName + "]");
              employeeRef.setId(refId);
              resourceFinder.setEmployee(employeeRef);
              DataRef projectRef = DataRef.withName(projectName);
              refId = projectAgent.getId(projectName);
              if (refId == null) System.out.println("NOT ABLE TO GET ID FOR PROJECT [" + projectName + "]");
              projectRef.setId(refId);
              resourceFinder.setProject(projectRef);
              SearchDetails<ProjectResourceFindByEmployeeEq_ProjectEqDetails> searchDetails = 
                new SearchDetails<ProjectResourceFindByEmployeeEq_ProjectEqDetails>(resourceFinder);
              searchDetails.setProjection("details");
              searchDetails.getPaging().setFetchAll(true);
              SearchResult<ProjectResourceDetails> searchResult = projectResourceAgent.find(searchDetails);
              String workType = projectTypes.get(projectIndex);
              String workInfo = projectInfos.get(projectIndex);
              if (searchResult.getTotalNumberOfItems() > 0) {
                List<ProjectResourceDetails> resourceList = searchResult.getResults();
                boolean resourceFound = false;
                ProjectResourceDetails actualResource = new ProjectResourceDetails();
                for (ProjectResourceDetails projectResourceDetails : resourceList) {
                  if (employeeName.equals("Kris Clottemans")) { actualResource = projectResourceDetails; resourceFound = true; }
                  if (employeeName.equals("An-Katrien Vandekerckhove")) { actualResource = projectResourceDetails; resourceFound = true; }
                  if (projectName.equals("DCS Fuel UK")) { actualResource = projectResourceDetails; resourceFound = true; }
                  if ((workType.equals("sup")) && (projectResourceDetails.getName().contains("Support"))) 
                    { actualResource = projectResourceDetails; resourceFound = true; }
                  if (!(workType.equals("sup")) && !(projectResourceDetails.getName().contains("Support"))) 
                    { actualResource = projectResourceDetails; resourceFound = true; }
                }
                if (!(resourceFound)) System.out.println("No proper match for [" + employeeName + "] and [" + projectName + "] for work type [" + workType + "]");
                //System.out.println("Found for [" + employeeName + "] and [" + projectName + "]");
                else {
                  WorkingDayHoursDetails dayHoursDetails = new WorkingDayHoursDetails();
                  Double nrHours = new Double(lineParts[5]);
                  dayHoursDetails.setProjectResource(DataRef.withIdAndName(actualResource.getId(), actualResource.getName()));
                  dayHoursDetails.setWorkingDay(workingDay);
                  dayHoursDetails.setHoursPerformed(nrHours);
                  dayHoursDetails.setDescription(workInfo);
                  workingDayHoursAgent.create(dayHoursDetails);

                  testString = lineParts[0];
                  int monthIndex = monthCodeList.indexOf(lineParts[0]);
                  if (monthResources.get(monthIndex).contains(actualResource.getId())) {
                    int listIndex = monthResources.get(monthIndex).indexOf(actualResource.getId());
                    Double sumHours = new Double(monthHours.get(monthIndex).get(listIndex).doubleValue() + nrHours.doubleValue());
                    monthHours.get(monthIndex).set(listIndex, sumHours);
                  }
                  else {
                    monthResources.get(monthIndex).add(actualResource.getId());
                    monthHours.get(monthIndex).add(nrHours);
//System.out.println("Creating month resource for month [" + monthIndex + "], id [" + actualResource.getId() + "] and name [" + actualResource.getName() + "]");
                  }
                }
              }
              else System.out.println("Found none for [" + employeeName + "] and [" + projectName + "]");
             }
            }
            else System.out.println("Cannot find ProjectId for [" + lineParts[3] + "] at line " + lineNr);
          }
          else {
            System.err.println("incorrect number of arguments at line with index = " + lineNr);
          }
          lineNr++;
          theLine = lineNr;
        }
      }
      for (int m=0; m < monthResources.size(); m++){
        for (int i=0; i < monthResources.get(m).size(); i++) {
          WorkingMonthHoursDetails monthHoursDetails = new WorkingMonthHoursDetails();
          monthHoursDetails.setProjectResource(DataRef.withId(monthResources.get(m).get(i)));
          monthHoursDetails.setHoursPerformed(monthHours.get(m).get(i));
          monthHoursDetails.setWorkingMonth(DataRef.withName(monthNameList.get(m)));
//System.out.println("Project resource id [" + monthResources.get(m).get(i).longValue() + "] for month ["+monthNameList.get(m)+"]");
          workingMonthHoursAgent.create(monthHoursDetails);
        }
      }
    }
    catch (Exception e) {
      System.err.println("Failed to create WorkingDayHours entry: ["+testString+"] on line ["+theLine+"]");
      e.printStackTrace();
    }
  }

  private static void createDetails(WorkingDayHoursDetails details) {
     //ParameterContext<WorkingDayHoursDetails> detailsParameter = new ParameterContext<WorkingDayHoursDetails>(getUserContext(), details);
     //workingDayHours.create(detailsParameter);
     details.getProjectResource().setId(projectResourceAgent.getId(details.getProjectResource().getName()));
     workingDayHoursAgent.create(details);
  }

  private static void modifyDetails(WorkingDayHoursDetails details) {
     ParameterContext<WorkingDayHoursDetails> detailsParameter = new ParameterContext<WorkingDayHoursDetails>(getUserContext(), details);
     workingDayHours.modify(detailsParameter);
  }

  private static UserContext getUserContext() {
    UserDetails userDetails = new UserDetails();
    userDetails.setName("admin");
    ExtendedUserContext extUserContext = new ExtendedUserContext(userDetails, "admin", new HashSet<String>());
    return extUserContext;
  }

  private static <T> ParameterContext<T> createParameter(T value) {
    return new ParameterContext(getUserContext(), value);
  }

}

