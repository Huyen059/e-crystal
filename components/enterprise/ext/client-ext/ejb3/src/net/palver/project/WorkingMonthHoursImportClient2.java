package net.palver.project;

// expanded with version 3.2.1

import java.util.Vector;
import java.util.Collection;
import java.util.StringTokenizer;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.democritus.sys.DataRef;
import net.democritus.sys.CrudsResult;
import net.democritus.sys.UserContext;
import net.democritus.sys.BasicUserContext;
import net.democritus.sys.ParameterContext;
import net.democritus.usr.ExtendedUserContext;
import net.democritus.usr.UserDetails;
import java.util.HashSet;

import net.democritus.jndi.ComponentJNDI;

import net.palver.project.ProjectResourceAgent;

// anchor:imports:start
// anchor:imports:end

/**
 * Remote import client to test and perform CRUD with respect,
 * to the business logic of the WorkingMonthHours in the EJB tier
 */

public class WorkingMonthHoursImportClient2 {

  private static WorkingMonthHoursRemote workingMonthHours = null;
  private static WorkingMonthHoursAgent workingMonthHoursAgent = WorkingMonthHoursAgent.getWorkingMonthHoursAgent(getUserContext());
  private static ProjectResourceAgent projectResourceAgent = ProjectResourceAgent.getProjectResourceAgent(getUserContext());


  public static void main(String args[]) {

    Context ctx = null;
    String paramVal = "";
    Integer paramInt = null;
    Long paramLong = null;
    Double paramDouble = null;
    Date paramDate = null;
    Boolean paramBoolean = null;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
    DataRef dataRef = null;
    Vector<DataRef> dataRefVec = null;
    StringTokenizer paramValStrTok = null;

    // Check if there is one argument
    if (args.length != 1) {
      System.err.println("Syntax is : java -cp <classpath> WorkingMonthHoursImportClient file:/'absolutePath'/WorkingMonthHours.csv");
      System.err.println("  using separator token ';' between fields, and '|' inside field enumerations");
      System.err.println("  fields are [workingMonth;project;projectResource;hoursPerformed;hoursBillable;projectSprint;employee]");
      System.err.println("  and SimpleDateFormat 'dd-MM-yy HH:mm:ss'");
      System.exit(2);
    }

    try {
      ctx = new InitialContext();
    }
    catch (Exception e) {
      System.err.println("Failed to create initial context: ");
      e.printStackTrace();
    }

    try {
      ComponentJNDI componentJNDI = ComponentJNDI.getComponentJNDI("enterprise");
      String workingMonthHoursName = componentJNDI.getDataRemoteName("WorkingMonthHours");
      workingMonthHours = (WorkingMonthHoursRemote) componentJNDI.lookupRemote(workingMonthHoursName);
    }
    catch (Exception e) {
      System.err.println("Failed to narrow WorkingMonthHoursRemote: ");
      e.printStackTrace();
    }

    int nrFields = 0;
    // anchor:number-of-fields:start
    nrFields = nrFields + 7;
    // anchor:number-of-fields:end

    try {
      int lineNr = 0;

      // open File with URI args[0]
      File aFile = new File(URI.create(args[0]));
      FileReader aFr = new FileReader(aFile);
      BufferedReader aBr = new BufferedReader(aFr);
      String aLine = "";
      String [] lineParts = null;
      while (aLine != null) {
        aLine = aBr.readLine();
        if (aLine != null) {
          lineParts = aLine.split(";", -1);
          if (lineParts.length == nrFields) {
            System.out.println("import CSV row with index " + lineNr);
            WorkingMonthHoursDetails workingMonthHoursDetails = new WorkingMonthHoursDetails();
            // anchor:detail-assignments:start
            paramVal = lineParts[0];
            dataRef = DataRef.withName(paramVal);
            workingMonthHoursDetails.setWorkingMonth(dataRef);
            paramVal = lineParts[1];
            dataRef = DataRef.withName(paramVal);
            workingMonthHoursDetails.setProject(dataRef);
            paramVal = lineParts[2];
            dataRef = DataRef.withName(paramVal);
            workingMonthHoursDetails.setProjectResource(dataRef);
            paramVal = lineParts[3];
            try {
              paramDouble = new Double(paramVal);
              workingMonthHoursDetails.setHoursPerformed(paramDouble);
            } catch (Exception e) {
              System.err.println("error assigning Double: " + e);
              e.printStackTrace();
            }
            paramVal = lineParts[4];
            try {
              paramDouble = new Double(paramVal);
              workingMonthHoursDetails.setHoursBillable(paramDouble);
            } catch (Exception e) {
              System.err.println("error assigning Double: " + e);
              e.printStackTrace();
            }
            paramVal = lineParts[5];
            dataRef = DataRef.withName(paramVal);
            workingMonthHoursDetails.setProjectSprint(dataRef);
            paramVal = lineParts[6];
            dataRef = DataRef.withName(paramVal);
            workingMonthHoursDetails.setEmployee(dataRef);
            // anchor:detail-assignments:end
            createDetails(workingMonthHoursDetails);
          }
          else {
            System.err.println("incorrect number of arguments at line with index = " + lineNr);
          }
          lineNr++;
        }
      }
    }
    catch (Exception e) {
      System.err.println("Failed to create WorkingMonthHours entry: ");
      e.printStackTrace();
    }
  }

  private static void createDetails(WorkingMonthHoursDetails details) {
     //ParameterContext<WorkingMonthHoursDetails> detailsParameter = new ParameterContext<WorkingMonthHoursDetails>(getUserContext(), details);
     //workingMonthHours.create(detailsParameter);
     details.getProjectResource().setId(projectResourceAgent.getId(details.getProjectResource().getName()));
     workingMonthHoursAgent.create(details);
  }

  private static void modifyDetails(WorkingMonthHoursDetails details) {
     ParameterContext<WorkingMonthHoursDetails> detailsParameter = new ParameterContext<WorkingMonthHoursDetails>(getUserContext(), details);
     workingMonthHours.modify(detailsParameter);
  }

  private static UserContext getUserContext() {
    UserDetails userDetails = new UserDetails();
    userDetails.setName("admin");
    ExtendedUserContext extUserContext = new ExtendedUserContext(userDetails, "admin", new HashSet<String>());
    return extUserContext;
  }

  private static <T> ParameterContext<T> createParameter(T value) {
    return new ParameterContext(getUserContext(), value);
  }

}

