package net.palver.invoice;

// expanded with version 3.2.1

import java.util.Vector;
import java.util.Collection;
import java.util.StringTokenizer;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.DecimalFormat;

import net.democritus.sys.DataRef;
import net.democritus.sys.CrudsResult;
import net.democritus.sys.UserContext;
import net.democritus.sys.BasicUserContext;
import net.democritus.sys.ParameterContext;
import net.democritus.usr.ExtendedUserContext;
import net.democritus.usr.UserDetails;
import java.util.HashSet;

import net.democritus.jndi.ComponentJNDI;


// anchor:imports:start
import java.util.Date;
// anchor:imports:end

/**
 * Remote import client to test and perform CRUD with respect,
 * to the business logic of the SalesInvoice in the EJB tier
 */

public class SalesInvoiceImportClient2 {

  private static SalesInvoiceRemote salesInvoice = null;
  private static SalesInvoiceAgent salesInvoiceAgent = SalesInvoiceAgent.getSalesInvoiceAgent(getUserContext());

  public static void main(String args[]) {

    Context ctx = null;
    String paramVal = "";
    Integer paramInt = null;
    Long paramLong = null;
    Double paramDouble = null;
    Date paramDate = null;
    Boolean paramBoolean = null;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
    DataRef dataRef = null;
    Vector<DataRef> dataRefVec = null;
    StringTokenizer paramValStrTok = null;
    DecimalFormat df = new DecimalFormat("0.00");
    SimpleDateFormat sdfr = new SimpleDateFormat("EEE, MMM dd, yyyy");

    String [] quarters = { "2017Q1" , "2017Q2" , "2017Q3" , "2017Q4" , "2018Q1" , "2018Q2"  , "2018Q3" , "2018Q4" , "2019Q1" , "2019Q2" , "2019Q3" , "2019Q4" };
    double [] quarterTotalExcl = new double[quarters.length];
    double [] quarterTotalIncl = new double[quarters.length];

    // Check if there is one argument
    if (args.length != 1) {
      System.err.println("Syntax is : java -cp <classpath> SalesInvoiceImportClient file:/'absolutePath'/SalesInvoice.csv");
      System.err.println("  using separator token ';' between fields, and '|' inside field enumerations");
      System.err.println("  fields are [name;client;project;salesOrder;dateOfInvoice;dateOfPayment;isPaid;totalExclVat;totalInclVat;totalVat;description]");
      System.err.println("  and SimpleDateFormat 'dd-MM-yy HH:mm:ss'");
      System.exit(2);
    }

    try {
      ctx = new InitialContext();
    }
    catch (Exception e) {
      System.err.println("Failed to create initial context: ");
      e.printStackTrace();
    }

    try {
      ComponentJNDI componentJNDI = ComponentJNDI.getComponentJNDI("enterprise");
      String salesInvoiceName = componentJNDI.getDataRemoteName("SalesInvoice");
      salesInvoice = (SalesInvoiceRemote) componentJNDI.lookupRemote(salesInvoiceName);
    }
    catch (Exception e) {
      System.err.println("Failed to narrow SalesInvoiceRemote: ");
      e.printStackTrace();
    }

    int nrFields = 0;
    // anchor:number-of-fields:start
    nrFields = nrFields + 11;
    // anchor:number-of-fields:end
    nrFields = 7;
    SimpleDateFormat ssdf = new SimpleDateFormat("dd/MM/yyyy");

    try {
     for (int q=0; q < quarters.length; q++) {
      int lineNr = 0;
      quarterTotalExcl[q] = 0.;
      quarterTotalIncl[q] = 0.;

      // open File with URI args[0]
   // File aFile = new File(URI.create(args[0].substring(6,args[0].length()-4)+"-"+quarters[q])+".csv");
      File aFile = new File(URI.create(System.getenv("CSV_DATA_URI")+"/SalesInvoices-"+quarters[q]+".csv"));
      FileReader aFr = new FileReader(aFile);
      BufferedReader aBr = new BufferedReader(aFr);
      String aLine = "";
      String [] lineParts = null;
      while (aLine != null) {
        aLine = aBr.readLine();
        if (aLine != null) {
          lineParts = aLine.split(";", -1);
          if (lineParts.length == nrFields) {
            System.out.println("import CSV row with index " + lineNr);
            SalesInvoiceDetails salesInvoiceDetails = new SalesInvoiceDetails();
            // anchor:detail-assignments:start
            paramVal = lineParts[0];
            if (paramVal.length() == 1) paramVal = quarters[q].substring(0,4) + "_00" + paramVal;
            else if (paramVal.length() == 2) paramVal = quarters[q].substring(0,4) + "_0" + paramVal;
            else if (paramVal.length() == 3) paramVal = quarters[q].substring(0,4) + "_" + paramVal;
            else if (paramVal.length() == 5) paramVal = quarters[q].substring(0,4) + "_" + paramVal;
            salesInvoiceDetails.setName(paramVal);
            paramVal = lineParts[1];
            dataRef = DataRef.withName(paramVal);
            salesInvoiceDetails.setClient(dataRef);
            paramVal = lineParts[2];
            dataRef = DataRef.withName(paramVal);
            salesInvoiceDetails.setProject(dataRef);
            System.out.println("Invoice nr [" + salesInvoiceDetails.getName() 
                                              + "] to client [" + salesInvoiceDetails.getClient().getName() 
                                              + "] for project [" + salesInvoiceDetails.getClient().getName() + "]");
            paramVal = lineParts[3];
            salesInvoiceDetails.setDescription(paramVal);
            paramVal = lineParts[4];
            try {
              if (paramVal.substring(1,2).equals("/")) paramVal = "0" + paramVal;
              if (paramVal.length() == 4) paramVal = paramVal.substring(0,3) + "0" + paramVal.substring(3);
              paramVal = paramVal + "/" + quarters[q].substring(0,4);
System.out.println("Parsing date [" + paramVal + "]");
              paramDate = ssdf.parse(paramVal);
              salesInvoiceDetails.setDateOfInvoice(paramDate);
            } catch (Exception e) {
              System.err.println("error assigning Date: " + e + ", new Date() will be used");
              e.printStackTrace();
              paramDate = new Date();
            }
            paramVal = lineParts[5];
            try {
              if (paramVal.contains("/")) {
                String paramVal1 = paramVal.substring(0,paramVal.indexOf("/"));
                String paramVal2 = paramVal.substring(paramVal.indexOf("/")+1);
                if (paramVal1.contains(".")) paramVal1 = paramVal1.substring(0,paramVal1.indexOf(".")) + paramVal1.substring(paramVal1.indexOf(".")+1);
                if (paramVal1.contains(",")) paramVal1 = paramVal1.substring(0,paramVal1.indexOf(",")) + "." + paramVal1.substring(paramVal1.indexOf(",")+1);
System.out.println("Parsing first amount [" + paramVal1 + "]");
                Double paramDouble1 = new Double(paramVal1);
                if (paramVal2.contains(".")) paramVal2 = paramVal2.substring(0,paramVal2.indexOf(".")) + paramVal2.substring(paramVal2.indexOf(".")+1);
                if (paramVal2.contains(",")) paramVal2 = paramVal2.substring(0,paramVal2.indexOf(",")) + "." + paramVal2.substring(paramVal2.indexOf(",")+1);
System.out.println("Parsing second amount [" + paramVal2 + "]");
                Double paramDouble2 = new Double(paramVal2);
                salesInvoiceDetails.setTotalInclVat(paramDouble1);
                salesInvoiceDetails.setTotalExclVat(paramDouble2);
                salesInvoiceDetails.setTotalVat(new Double(paramDouble1.doubleValue() - paramDouble2.doubleValue()));
              }
              else {
                if (paramVal.contains(".")) paramVal = paramVal.substring(0,paramVal.indexOf(".")) + paramVal.substring(paramVal.indexOf(".")+1);
                if (paramVal.contains(",")) paramVal = paramVal.substring(0,paramVal.indexOf(",")) + "." + paramVal.substring(paramVal.indexOf(",")+1);
System.out.println("Parsing single amount [" + paramVal + "]");
                paramDouble = new Double(paramVal);
                salesInvoiceDetails.setTotalExclVat(paramDouble);
                salesInvoiceDetails.setTotalInclVat(paramDouble);
                salesInvoiceDetails.setTotalVat(new Double("0."));
              }
              quarterTotalExcl[q] = quarterTotalExcl[q] + salesInvoiceDetails.getTotalExclVat().doubleValue();
              quarterTotalIncl[q] = quarterTotalIncl[q] + salesInvoiceDetails.getTotalInclVat().doubleValue();
            } catch (Exception e) {
              System.err.println("error assigning Double: " + e);
              e.printStackTrace();
            }
            paramVal = lineParts[6];
            salesInvoiceDetails.setPayment(paramVal);
            // anchor:detail-assignments:end
            String name = salesInvoiceDetails.getName();
            Long id = null;
            try {
              CrudsResult<DataRef> result = salesInvoice.getId(createParameter(name));
              if (result.isSuccess()) {
                id = result.getValue().getId();
                salesInvoiceDetails.setId(id);
                modifyDetails(salesInvoiceDetails);
              } else {
                createDetails(salesInvoiceDetails);
              }
            }
            catch (Exception e) {
              createDetails(salesInvoiceDetails);
            }
          }
          else {
            System.err.println("incorrect number of arguments at line with index = " + lineNr);
          }
          lineNr++;
        }
      }
     }
     System.out.println("QUARTER REVENUES AT " + sdfr.format(new Date()));
     for (int q=0; q < quarters.length; q++) {
       System.out.println("For " + quarters[q] + " : Total excl. VAT = " + df.format(quarterTotalExcl[q]) + " ; Total incl. VAT = " + df.format(quarterTotalIncl[q]));
     }
     System.out.println("YEAR REVENUES AT " + sdfr.format(new Date()));
     System.out.println("For 2017 : Total excl. VAT = " + df.format(quarterTotalExcl[0] + quarterTotalExcl[1] + quarterTotalExcl[2] + quarterTotalExcl[3]) + 
                                " ; Total incl. VAT = " + df.format(quarterTotalIncl[0] + quarterTotalIncl[1] + quarterTotalIncl[2] + quarterTotalIncl[3]));
     System.out.println("For 2018 : Total excl. VAT = " + df.format(quarterTotalExcl[4] + quarterTotalExcl[5] + quarterTotalExcl[6] + quarterTotalExcl[7]) + 
                                " ; Total incl. VAT = " + df.format(quarterTotalIncl[4] + quarterTotalIncl[5] + quarterTotalIncl[6] + quarterTotalIncl[7]));
     System.out.println("For 2019 : Total excl. VAT = " + df.format(quarterTotalExcl[8] + quarterTotalExcl[9] + quarterTotalExcl[10] + quarterTotalExcl[11]) +
                                " ; Total incl. VAT = " + df.format(quarterTotalIncl[8] + quarterTotalIncl[9] + quarterTotalIncl[10] + quarterTotalIncl[11]));
    }
    catch (Exception e) {
      System.err.println("Failed to create SalesInvoice entry: ");
      e.printStackTrace();
    }
  }

  private static void createDetails(SalesInvoiceDetails details) {
     //ParameterContext<SalesInvoiceDetails> detailsParameter = new ParameterContext<SalesInvoiceDetails>(getUserContext(), details);
     //salesInvoice.create(detailsParameter);
     salesInvoiceAgent.create(details);
  }

  private static void modifyDetails(SalesInvoiceDetails details) {
     ParameterContext<SalesInvoiceDetails> detailsParameter = new ParameterContext<SalesInvoiceDetails>(getUserContext(), details);
     salesInvoice.modify(detailsParameter);
  }

  private static UserContext getUserContext() {
    UserDetails userDetails = new UserDetails();
    userDetails.setName("admin");
    ExtendedUserContext extUserContext = new ExtendedUserContext(userDetails, "admin", new HashSet<String>());
    return extUserContext;
  }

  private static <T> ParameterContext<T> createParameter(T value) {
    return new ParameterContext(getUserContext(), value);
  }

}

