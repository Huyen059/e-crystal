package net.palver.invoice;

// expanded with version 3.2.1

import java.util.Vector;
import java.util.Collection;
import java.util.StringTokenizer;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.democritus.sys.DataRef;
import net.democritus.sys.CrudsResult;
import net.democritus.sys.UserContext;
import net.democritus.sys.ParameterContext;

import net.democritus.jndi.ComponentJNDI;

import net.democritus.sys.SearchResult;
import net.democritus.sys.search.SearchDetails;

import net.palver.resource.EmployeeAgent;
import net.palver.project.ProjectAgent;
import net.palver.project.ProjectResourceAgent;
import net.palver.project.ProjectResourceDetails;
import net.palver.project.ProjectResourceFindByEmployeeEq_ProjectResourceRoleEqDetails;
import net.palver.project.ProjectResourceRoleAgent;
import net.palver.project.ProjectResourceRoleDetails;
import net.palver.project.ProjectResourceRoleFindByProjectEq_ProjectResourceTypeEqDetails;
import net.palver.product.ProjectResourceTypeAgent;
import net.palver.project.WorkingMonthHoursAgent;
import net.palver.project.WorkingMonthHoursDetails;
import net.palver.project.WorkingMonthHoursFindByWorkingMonthEq_ProjectResourceEqDetails;
import net.palver.registry.WorkingMonthAgent;

// anchor:imports:start
// anchor:imports:end

/**
 * Remote import client to test and perform CRUD with respect,
 * to the business logic of the InvoiceLineResourceHours in the EJB tier
 */

public class InvoiceLineResourceHoursImportClient2 {

  private static InvoiceLineResourceHoursRemote invoiceLineResourceHours = null;
  private static WorkingMonthAgent workingMonthAgent = WorkingMonthAgent.getWorkingMonthAgent(getUserContext());
  private static WorkingMonthHoursAgent workingMonthHoursAgent = WorkingMonthHoursAgent.getWorkingMonthHoursAgent(getUserContext());
  private static ProjectResourceAgent projectResourceAgent = ProjectResourceAgent.getProjectResourceAgent(getUserContext());
  private static ProjectResourceRoleAgent projectResourceRoleAgent = ProjectResourceRoleAgent.getProjectResourceRoleAgent(getUserContext());
  private static ProjectResourceTypeAgent projectResourceTypeAgent = ProjectResourceTypeAgent.getProjectResourceTypeAgent(getUserContext());
  private static ProjectAgent projectAgent = ProjectAgent.getProjectAgent(getUserContext());
  private static EmployeeAgent employeeAgent = EmployeeAgent.getEmployeeAgent(getUserContext());


  public static void main(String args[]) {

    Context ctx = null;
    String paramVal = "";
    Integer paramInt = null;
    Long paramLong = null;
    Double paramDouble = null;
    Date paramDate = null;
    Boolean paramBoolean = null;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
    DataRef dataRef = null;
    Vector<DataRef> dataRefVec = null;
    StringTokenizer paramValStrTok = null;

    // Check if there is one argument
    if (args.length != 1) {
      System.err.println("Syntax is : java -cp <classpath> InvoiceLineResourceHoursImportClient file:/'absolutePath'/InvoiceLineResourceHours.csv");
      System.err.println("  using separator token ';' between fields, and '|' inside field enumerations");
      System.err.println("  fields are [project;invoice;invoicingMode;hoursPerformed;projectResourceRole;totalAmount;workingMonthHours]");
      System.err.println("  and SimpleDateFormat 'dd-MM-yy HH:mm:ss'");
      System.exit(2);
    }

    try {
      ctx = new InitialContext();
    }
    catch (Exception e) {
      System.err.println("Failed to create initial context: ");
      e.printStackTrace();
    }

    try {
      ComponentJNDI componentJNDI = ComponentJNDI.getComponentJNDI("enterprise");
      String invoiceLineResourceHoursName = componentJNDI.getDataRemoteName("InvoiceLineResourceHours");
      invoiceLineResourceHours = (InvoiceLineResourceHoursRemote) componentJNDI.lookupRemote(invoiceLineResourceHoursName);
    }
    catch (Exception e) {
      System.err.println("Failed to narrow InvoiceLineResourceHoursRemote: ");
      e.printStackTrace();
    }

    int nrFields = 0;
    // anchor:number-of-fields:start
    nrFields = nrFields + 7;
    // anchor:number-of-fields:end

    try {
      int lineNr = 0;

      // open File with URI args[0]
      File aFile = new File(URI.create(args[0]));
      FileReader aFr = new FileReader(aFile);
      BufferedReader aBr = new BufferedReader(aFr);
      String aLine = "";
      String [] lineParts = null;
      while (aLine != null) {
        aLine = aBr.readLine();
        if (aLine != null) {
          lineParts = aLine.split(";", -1);
          if (lineParts.length == nrFields) {
            System.out.println("import CSV row with index " + lineNr);
            InvoiceLineResourceHoursDetails invoiceLineResourceHoursDetails = new InvoiceLineResourceHoursDetails();
            // anchor:detail-assignments:start
            String theYear = "2018";
            paramVal = lineParts[0];
            if (paramVal.length() == 1) paramVal = theYear + "_00" + paramVal;
            else if (paramVal.length() == 2) paramVal = theYear + "_0" + paramVal;
            else if (paramVal.length() == 3) paramVal = theYear + "_" + paramVal;
            else if (paramVal.length() == 5) paramVal = theYear + "_" + paramVal;
            dataRef = DataRef.withName(paramVal);
            invoiceLineResourceHoursDetails.setInvoice(dataRef);
            paramVal = lineParts[2];
            dataRef = DataRef.withName(paramVal);
            invoiceLineResourceHoursDetails.setProject(dataRef);
            paramVal = lineParts[6];
            invoiceLineResourceHoursDetails.setInvoicingMode(paramVal);
            paramVal = lineParts[5];
            try {
              paramDouble = new Double(paramVal);
              invoiceLineResourceHoursDetails.setHoursPerformed(paramDouble);
            } catch (Exception e) {
              System.err.println("error assigning Double: " + e);
              e.printStackTrace();
            }
            paramVal = lineParts[1];
            DataRef employeeRef = DataRef.withName(paramVal);
            Long employeeId = employeeAgent.getId(paramVal);
            if (employeeId == null) System.out.println("NOT ABLE TO GET ID FOR EMPLOYEE [" + paramVal + "]");
            employeeRef.setId(employeeId);
            invoiceLineResourceHoursDetails.setEmployee(employeeRef);
            paramVal = lineParts[2];
            DataRef projectRef = DataRef.withName(paramVal);
            Long projectId = projectAgent.getId(paramVal);
            if (projectId == null) System.out.println("NOT ABLE TO GET ID FOR PROJECT [" + paramVal + "]");
            projectRef.setId(projectId);
            paramVal = lineParts[3];
            DataRef monthRef = DataRef.withName(paramVal);
            Long monthId = workingMonthAgent.getId(paramVal);
            if (monthId == null) System.out.println("NOT ABLE TO GET ID FOR WORKING MONTH [" + paramVal + "]");
            monthRef.setId(monthId);
            paramVal = lineParts[4];
            DataRef resourceTypeRef = DataRef.withName(paramVal);
            Long resourceTypeId = projectResourceTypeAgent.getId(paramVal);
            if (resourceTypeId == null) System.out.println("NOT ABLE TO GET ID FOR RESOURCE TYPE [" + paramVal + "]");
            resourceTypeRef.setId(resourceTypeId);
System.out.println("Employee [" + employeeRef.getName() + "], project [" + projectRef.getName() + "], working month [" + 
                                  monthRef.getName() + "], resource type [" + resourceTypeRef.getName() + "]");
            // find projectResourceRole
            DataRef resourceRoleRef = new DataRef();
            try {
              ProjectResourceRoleFindByProjectEq_ProjectResourceTypeEqDetails resourceRoleFinder = new ProjectResourceRoleFindByProjectEq_ProjectResourceTypeEqDetails();
              resourceRoleFinder.setProject(projectRef);
              resourceRoleFinder.setProjectResourceType(resourceTypeRef);
              SearchDetails<ProjectResourceRoleFindByProjectEq_ProjectResourceTypeEqDetails> searchDetails = 
                new SearchDetails<ProjectResourceRoleFindByProjectEq_ProjectResourceTypeEqDetails>(resourceRoleFinder);
              searchDetails.setProjection("details");
              searchDetails.getPaging().setFetchAll(true);
              SearchResult<ProjectResourceRoleDetails> searchResult = projectResourceRoleAgent.find(searchDetails);
              if (searchResult.getTotalNumberOfItems() > 0) {
                resourceRoleRef = searchResult.getResults().get(0).getDataRef();
              }
            } catch (Exception e) { }
            // find projectResource
            DataRef resourceRef = new DataRef();
            try {
              ProjectResourceFindByEmployeeEq_ProjectResourceRoleEqDetails resourceFinder = new ProjectResourceFindByEmployeeEq_ProjectResourceRoleEqDetails();
              resourceFinder.setEmployee(employeeRef);
              resourceFinder.setProjectResourceRole(resourceRoleRef);
              SearchDetails<ProjectResourceFindByEmployeeEq_ProjectResourceRoleEqDetails> searchDetails = 
                new SearchDetails<ProjectResourceFindByEmployeeEq_ProjectResourceRoleEqDetails>(resourceFinder);
              searchDetails.setProjection("details");
              searchDetails.getPaging().setFetchAll(true);
              SearchResult<ProjectResourceDetails> searchResult = projectResourceAgent.find(searchDetails);
              if (searchResult.getTotalNumberOfItems() > 0) {
                resourceRef = searchResult.getResults().get(0).getDataRef();
              }
            } catch (Exception e) { }
            // find workingMonthHours
            DataRef monthHoursRef = new DataRef();
            WorkingMonthHoursDetails monthHoursDetails = new WorkingMonthHoursDetails();
            try {
              WorkingMonthHoursFindByWorkingMonthEq_ProjectResourceEqDetails monthHoursFinder = new WorkingMonthHoursFindByWorkingMonthEq_ProjectResourceEqDetails();
              monthHoursFinder.setWorkingMonth(monthRef);
              monthHoursFinder.setProjectResource(resourceRef);
              SearchDetails<WorkingMonthHoursFindByWorkingMonthEq_ProjectResourceEqDetails> searchDetails = 
                new SearchDetails<WorkingMonthHoursFindByWorkingMonthEq_ProjectResourceEqDetails>(monthHoursFinder);
              searchDetails.setProjection("details");
              searchDetails.getPaging().setFetchAll(true);
              SearchResult<WorkingMonthHoursDetails> searchResult = workingMonthHoursAgent.find(searchDetails);
              if (searchResult.getTotalNumberOfItems() > 0) {
                monthHoursDetails = searchResult.getResults().get(0);
                monthHoursRef = monthHoursDetails.getDataRef();
              }
              else System.out.println("NO working month hours found !!!");
            } catch (Exception e) { System.out.println("EXCEPTION in working month hours !!!"); }
            // create invoiceLineResourceHours
            invoiceLineResourceHoursDetails.setProjectResourceRole(resourceRoleRef);
            invoiceLineResourceHoursDetails.setWorkingMonthHours(monthHoursRef);
            // anchor:detail-assignments:end
            createDetails(invoiceLineResourceHoursDetails);
            // modify workingMonthHours
            double hoursBilled = 0;
            try {
              hoursBilled = monthHoursDetails.getHoursBilled().doubleValue();
            } catch (Exception e) { }
            hoursBilled = hoursBilled + invoiceLineResourceHoursDetails.getHoursPerformed().doubleValue();
            monthHoursDetails.setHoursBilled(hoursBilled);
            workingMonthHoursAgent.modify(monthHoursDetails);
          }
          else {
            System.err.println("incorrect number of arguments at line with index = " + lineNr);
          }
          lineNr++;
        }
      }
    }
    catch (Exception e) {
      System.err.println("Failed to create InvoiceLineResourceHours entry: ");
      e.printStackTrace();
    }
  }

  private static void createDetails(InvoiceLineResourceHoursDetails details) {
     ParameterContext<InvoiceLineResourceHoursDetails> detailsParameter = new ParameterContext<InvoiceLineResourceHoursDetails>(getUserContext(), details);
     invoiceLineResourceHours.create(detailsParameter);
  }

  private static void modifyDetails(InvoiceLineResourceHoursDetails details) {
     ParameterContext<InvoiceLineResourceHoursDetails> detailsParameter = new ParameterContext<InvoiceLineResourceHoursDetails>(getUserContext(), details);
     invoiceLineResourceHours.modify(detailsParameter);
  }

  private static UserContext getUserContext() {
    return null;
  }

  private static <T> ParameterContext<T> createParameter(T value) {
    return new ParameterContext(getUserContext(), value);
  }

}

