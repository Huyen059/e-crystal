-- Provisioning script for E-CRYSTAL PRD
-- Database: POSTGRESQL
-- expanded with nsx-expanders:4.4.4, expansionResource net.democritus:sql-expanders:1.2.1

-- ------------------- --
-- Create Sequences    --
-- ------------------- --

-- anchor:sequences:start
CREATE SEQUENCE IF NOT EXISTS hibernate_sequence INCREMENT BY 1;
-- anchor:sequences:end

-- ------------------ --
-- Create Schemas     --
-- ------------------ --

-- anchor:schemas:start
CREATE SCHEMA UTILS;
CREATE SCHEMA VALIDATION;
CREATE SCHEMA ACCOUNT;
CREATE SCHEMA WORKFLOW;
CREATE SCHEMA ASSETS;
CREATE SCHEMA ENTERPRISE;
CREATE SCHEMA ERPTIMESHEETS;
CREATE SCHEMA CRMMODEL;
-- anchor:schemas:end

-- ------------------ --
-- Create Tables      --
-- ------------------ --

-- anchor:tables:start
-- > ParamTargetValue
CREATE TABLE UTILS.ParamTargetValue (
    id BIGINT NOT NULL,
    param VARCHAR(255),
    target VARCHAR(255),
    value VARCHAR(255),
    -- @anchor:utils-paramTargetValue-table:start
    -- @anchor:utils-paramTargetValue-table:end
    -- anchor:custom-utils-paramTargetValue-table:start
    -- anchor:custom-utils-paramTargetValue-table:end
    PRIMARY KEY (id)
);
-- < ParamTargetValue

-- > Execution
CREATE TABLE UTILS.Execution (
    id BIGINT NOT NULL,
    component VARCHAR(255),
    element VARCHAR(255),
    name VARCHAR(255),
    packageName VARCHAR(255),
    -- @anchor:utils-execution-table:start
    -- @anchor:utils-execution-table:end
    -- anchor:custom-utils-execution-table:start
    -- anchor:custom-utils-execution-table:end
    PRIMARY KEY (id)
);
-- < Execution

-- > Thumbnail
CREATE TABLE UTILS.Thumbnail (
    id BIGINT NOT NULL,
    border INTEGER,
    clickAction VARCHAR(255),
    depth INTEGER,
    fullName VARCHAR(255),
    height INTEGER,
    hooverAction VARCHAR(255),
    leftX INTEGER,
    name VARCHAR(255),
    targetId BIGINT,
    targetName VARCHAR(255),
    targetType VARCHAR(255),
    thumbName VARCHAR(255),
    thumbType VARCHAR(255),
    topY INTEGER,
    uri VARCHAR(255),
    width INTEGER,
    -- @anchor:utils-thumbnail-table:start
    -- @anchor:utils-thumbnail-table:end
    -- anchor:custom-utils-thumbnail-table:start
    -- anchor:custom-utils-thumbnail-table:end
    PRIMARY KEY (id)
);
-- < Thumbnail

-- > TagValuePair
CREATE TABLE UTILS.TagValuePair (
    id BIGINT NOT NULL,
    tag VARCHAR(255),
    value VARCHAR(255),
    -- @anchor:utils-tagValuePair-table:start
    -- @anchor:utils-tagValuePair-table:end
    -- anchor:custom-utils-tagValuePair-table:start
    -- anchor:custom-utils-tagValuePair-table:end
    PRIMARY KEY (id)
);
-- < TagValuePair

-- > IdCounter
CREATE TABLE UTILS.IdCounter (
    id BIGINT NOT NULL,
    counter BIGINT,
    name VARCHAR(255),
    -- @anchor:utils-idCounter-table:start
    -- @anchor:utils-idCounter-table:end
    -- anchor:custom-utils-idCounter-table:start
    -- anchor:custom-utils-idCounter-table:end
    PRIMARY KEY (id)
);
-- < IdCounter

-- > Validation
CREATE TABLE VALIDATION.Validation (
    id BIGINT NOT NULL,
    name VARCHAR(255),
    -- @anchor:validation-validation-table:start
    -- @anchor:validation-validation-table:end
    -- anchor:custom-validation-validation-table:start
    -- anchor:custom-validation-validation-table:end
    PRIMARY KEY (id)
);
-- < Validation

-- > DataAccess
CREATE TABLE ACCOUNT.DataAccess (
    id BIGINT NOT NULL,
    authorized VARCHAR(255),
    disabled VARCHAR(255),
    element VARCHAR(255),
    enteredAt TIMESTAMP,
    forProfile_id BIGINT,
    forUser_id BIGINT,
    forUserGroup_id BIGINT,
    functionality VARCHAR(255),
    lastModifiedAt TIMESTAMP,
    name VARCHAR(255),
    target VARCHAR(255),
    -- @anchor:account-dataAccess-table:start
    -- @anchor:account-dataAccess-table:end
    -- anchor:custom-account-dataAccess-table:start
    -- anchor:custom-account-dataAccess-table:end
    PRIMARY KEY (id)
);
-- < DataAccess

-- > HelpInfo
CREATE TABLE ACCOUNT.HelpInfo (
    id BIGINT NOT NULL,
    description VARCHAR(255),
    name VARCHAR(255),
    -- @anchor:account-helpInfo-table:start
    -- @anchor:account-helpInfo-table:end
    -- anchor:custom-account-helpInfo-table:start
    -- anchor:custom-account-helpInfo-table:end
    PRIMARY KEY (id)
);
-- < HelpInfo

-- > Account
CREATE TABLE ACCOUNT.Account (
    id BIGINT NOT NULL,
    address VARCHAR(255),
    city VARCHAR(255),
    country VARCHAR(255),
    email VARCHAR(255),
    fullName VARCHAR(255),
    name VARCHAR(255),
    phone VARCHAR(255),
    refId VARCHAR(255),
    status VARCHAR(255),
    style VARCHAR(255),
    zipCode VARCHAR(255),
    -- @anchor:account-account-table:start
    -- @anchor:account-account-table:end
    -- anchor:custom-account-account-table:start
    -- anchor:custom-account-account-table:end
    PRIMARY KEY (id)
);
-- < Account

-- > User
CREATE TABLE ACCOUNT.User (
    id BIGINT NOT NULL,
    account_id BIGINT,
    disabled VARCHAR(255),
    email VARCHAR(255),
    encryptedPassword VARCHAR(255),
    enteredAt TIMESTAMP,
    firstName VARCHAR(255),
    fullName VARCHAR(255),
    language VARCHAR(255),
    lastModifiedAt TIMESTAMP,
    lastName VARCHAR(255),
    mobile VARCHAR(255),
    name VARCHAR(255),
    password VARCHAR(255),
    persNr VARCHAR(255),
    profile_id BIGINT,
    timeout INTEGER,
    -- @anchor:account-user-table:start
    -- @anchor:account-user-table:end
    -- anchor:custom-account-user-table:start
    -- anchor:custom-account-user-table:end
    PRIMARY KEY (id)
);
-- < User

-- > Portal
CREATE TABLE ACCOUNT.Portal (
    id BIGINT NOT NULL,
    description VARCHAR(255),
    name VARCHAR(255),
    version VARCHAR(255),
    -- @anchor:account-portal-table:start
    -- @anchor:account-portal-table:end
    -- anchor:custom-account-portal-table:start
    -- anchor:custom-account-portal-table:end
    PRIMARY KEY (id)
);
-- < Portal

-- > Menu
CREATE TABLE ACCOUNT.Menu (
    id BIGINT NOT NULL,
    name VARCHAR(255),
    portal_id BIGINT,
    profile_id BIGINT,
    -- @anchor:account-menu-table:start
    -- @anchor:account-menu-table:end
    -- anchor:custom-account-menu-table:start
    -- anchor:custom-account-menu-table:end
    PRIMARY KEY (id)
);
-- < Menu

-- > Component
CREATE TABLE ACCOUNT.Component (
    id BIGINT NOT NULL,
    name VARCHAR(255),
    -- @anchor:account-component-table:start
    -- @anchor:account-component-table:end
    -- anchor:custom-account-component-table:start
    -- anchor:custom-account-component-table:end
    PRIMARY KEY (id)
);
-- < Component

-- > Screen
CREATE TABLE ACCOUNT.Screen (
    id BIGINT NOT NULL,
    component_id BIGINT,
    link VARCHAR(255),
    name VARCHAR(255),
    sortOrder INTEGER,
    -- @anchor:account-screen-table:start
    -- @anchor:account-screen-table:end
    -- anchor:custom-account-screen-table:start
    -- anchor:custom-account-screen-table:end
    PRIMARY KEY (id)
);
-- < Screen

-- > MenuItem
CREATE TABLE ACCOUNT.MenuItem (
    id BIGINT NOT NULL,
    menu_id BIGINT,
    menuItem_id BIGINT,
    name VARCHAR(255),
    screen_id BIGINT,
    sortOrder INTEGER,
    -- @anchor:account-menuItem-table:start
    -- @anchor:account-menuItem-table:end
    -- anchor:custom-account-menuItem-table:start
    -- anchor:custom-account-menuItem-table:end
    PRIMARY KEY (id)
);
-- < MenuItem

-- > Profile
CREATE TABLE ACCOUNT.Profile (
    id BIGINT NOT NULL,
    name VARCHAR(255),
    userGroup_id BIGINT,
    weight INTEGER,
    -- @anchor:account-profile-table:start
    -- @anchor:account-profile-table:end
    -- anchor:custom-account-profile-table:start
    -- anchor:custom-account-profile-table:end
    PRIMARY KEY (id)
);
-- < Profile

-- > ScreenProfile
CREATE TABLE ACCOUNT.ScreenProfile (
    id BIGINT NOT NULL,
    profile_id BIGINT,
    screen_id BIGINT,
    -- @anchor:account-screenProfile-table:start
    -- @anchor:account-screenProfile-table:end
    -- anchor:custom-account-screenProfile-table:start
    -- anchor:custom-account-screenProfile-table:end
    PRIMARY KEY (id)
);
-- < ScreenProfile

-- > UserGroup
CREATE TABLE ACCOUNT.UserGroup (
    id BIGINT NOT NULL,
    disabled VARCHAR(255),
    enteredAt TIMESTAMP,
    lastModifiedAt TIMESTAMP,
    name VARCHAR(255),
    type VARCHAR(255),
    -- @anchor:account-userGroup-table:start
    -- @anchor:account-userGroup-table:end
    -- anchor:custom-account-userGroup-table:start
    -- anchor:custom-account-userGroup-table:end
    PRIMARY KEY (id)
);
-- < UserGroup

-- > TimeTask
CREATE TABLE WORKFLOW.TimeTask (
    id BIGINT NOT NULL,
    implementation VARCHAR(255),
    intervalPeriod BIGINT,
    name VARCHAR(255),
    params VARCHAR(255),
    processor VARCHAR(255),
    requiredAction VARCHAR(255),
    triggerState VARCHAR(255),
    workflow_id BIGINT,
    -- @anchor:workflow-timeTask-table:start
    -- @anchor:workflow-timeTask-table:end
    -- anchor:custom-workflow-timeTask-table:start
    -- anchor:custom-workflow-timeTask-table:end
    PRIMARY KEY (id)
);
-- < TimeTask

-- > TimeWindow
CREATE TABLE WORKFLOW.TimeWindow (
    id BIGINT NOT NULL,
    name VARCHAR(255),
    startTime VARCHAR(255),
    stopTime VARCHAR(255),
    -- @anchor:workflow-timeWindow-table:start
    -- @anchor:workflow-timeWindow-table:end
    -- anchor:custom-workflow-timeWindow-table:start
    -- anchor:custom-workflow-timeWindow-table:end
    PRIMARY KEY (id)
);
-- < TimeWindow

-- > StateTask
CREATE TABLE WORKFLOW.StateTask (
    id BIGINT NOT NULL,
    beginState VARCHAR(255),
    endState VARCHAR(255),
    failedState VARCHAR(255),
    implementation VARCHAR(255),
    interimState VARCHAR(255),
    maxConcurrentTasks INTEGER,
    name VARCHAR(255),
    params VARCHAR(255),
    processor VARCHAR(255),
    timeout BIGINT,
    workflow_id BIGINT,
    -- @anchor:workflow-stateTask-table:start
    -- @anchor:workflow-stateTask-table:end
    -- anchor:custom-workflow-stateTask-table:start
    -- anchor:custom-workflow-stateTask-table:end
    PRIMARY KEY (id)
);
-- < StateTask

-- > EngineNode
CREATE TABLE WORKFLOW.EngineNode (
    id BIGINT NOT NULL,
    activeSince TIMESTAMP,
    hostname VARCHAR(255),
    lastActive TIMESTAMP,
    master BOOL,
    name VARCHAR(255),
    status VARCHAR(255),
    -- @anchor:workflow-engineNode-table:start
    -- @anchor:workflow-engineNode-table:end
    -- anchor:custom-workflow-engineNode-table:start
    -- anchor:custom-workflow-engineNode-table:end
    PRIMARY KEY (id)
);
-- < EngineNode

-- > EngineService
CREATE TABLE WORKFLOW.EngineService (
    id BIGINT NOT NULL,
    batchSize INTEGER,
    busy VARCHAR(255),
    changed VARCHAR(255),
    collector BIGINT,
    lastRunAt TIMESTAMP,
    maximumNumberOfNodes INTEGER,
    name VARCHAR(255),
    status VARCHAR(255),
    timeWindowGroup_id BIGINT,
    waitTime INTEGER,
    workflow_id BIGINT,
    -- @anchor:workflow-engineService-table:start
    -- @anchor:workflow-engineService-table:end
    -- anchor:custom-workflow-engineService-table:start
    -- anchor:custom-workflow-engineService-table:end
    PRIMARY KEY (id)
);
-- < EngineService

-- > EngineNodeService
CREATE TABLE WORKFLOW.EngineNodeService (
    id BIGINT NOT NULL,
    engineNode_id BIGINT,
    engineService_id BIGINT,
    lastRunAt TIMESTAMP,
    name VARCHAR(255),
    nextRun TIMESTAMP,
    status VARCHAR(255),
    -- @anchor:workflow-engineNodeService-table:start
    -- @anchor:workflow-engineNodeService-table:end
    -- anchor:custom-workflow-engineNodeService-table:start
    -- anchor:custom-workflow-engineNodeService-table:end
    PRIMARY KEY (id)
);
-- < EngineNodeService

-- > TimeWindowGroup
CREATE TABLE WORKFLOW.TimeWindowGroup (
    id BIGINT NOT NULL,
    name VARCHAR(255),
    visible VARCHAR(255),
    -- @anchor:workflow-timeWindowGroup-table:start
    -- @anchor:workflow-timeWindowGroup-table:end
    -- anchor:custom-workflow-timeWindowGroup-table:start
    -- anchor:custom-workflow-timeWindowGroup-table:end
    PRIMARY KEY (id)
);
-- < TimeWindowGroup

-- > Workflow
CREATE TABLE WORKFLOW.Workflow (
    id BIGINT NOT NULL,
    className VARCHAR(255),
    componentName VARCHAR(255),
    name VARCHAR(255),
    responsible_id BIGINT,
    sequencingStrategy VARCHAR(255),
    target VARCHAR(255),
    -- @anchor:workflow-workflow-table:start
    -- @anchor:workflow-workflow-table:end
    -- anchor:custom-workflow-workflow-table:start
    -- anchor:custom-workflow-workflow-table:end
    PRIMARY KEY (id)
);
-- < Workflow

-- > StateTimer
CREATE TABLE WORKFLOW.StateTimer (
    id BIGINT NOT NULL,
    allowedPeriod BIGINT,
    alteredState VARCHAR(255),
    beginState VARCHAR(255),
    implementation VARCHAR(255),
    name VARCHAR(255),
    params VARCHAR(255),
    processor VARCHAR(255),
    requiredAction VARCHAR(255),
    targetState VARCHAR(255),
    workflow_id BIGINT,
    -- @anchor:workflow-stateTimer-table:start
    -- @anchor:workflow-stateTimer-table:end
    -- anchor:custom-workflow-stateTimer-table:start
    -- anchor:custom-workflow-stateTimer-table:end
    PRIMARY KEY (id)
);
-- < StateTimer

-- > RemoteAsset
CREATE TABLE ASSETS.RemoteAsset (
    id BIGINT NOT NULL,
    name VARCHAR(255),
    url VARCHAR(255),
    -- @anchor:assets-remoteAsset-table:start
    -- @anchor:assets-remoteAsset-table:end
    -- anchor:custom-assets-remoteAsset-table:start
    -- anchor:custom-assets-remoteAsset-table:end
    PRIMARY KEY (id)
);
-- < RemoteAsset

-- > ExternalAsset
CREATE TABLE ASSETS.ExternalAsset (
    id BIGINT NOT NULL,
    byteSize BIGINT,
    contentType VARCHAR(255),
    name VARCHAR(255),
    uri VARCHAR(255),
    -- @anchor:assets-externalAsset-table:start
    -- @anchor:assets-externalAsset-table:end
    -- anchor:custom-assets-externalAsset-table:start
    -- anchor:custom-assets-externalAsset-table:end
    PRIMARY KEY (id)
);
-- < ExternalAsset

-- > FileAsset
CREATE TABLE ASSETS.FileAsset (
    id BIGINT NOT NULL,
    name VARCHAR(255),
    uploadUri VARCHAR(255),
    -- @anchor:assets-fileAsset-table:start
    -- @anchor:assets-fileAsset-table:end
    -- anchor:custom-assets-fileAsset-table:start
    -- anchor:custom-assets-fileAsset-table:end
    PRIMARY KEY (id)
);
-- < FileAsset

-- > InternalAsset
CREATE TABLE ASSETS.InternalAsset (
    id BIGINT NOT NULL,
    name VARCHAR(255),
    -- @anchor:assets-internalAsset-table:start
    -- @anchor:assets-internalAsset-table:end
    -- anchor:custom-assets-internalAsset-table:start
    -- anchor:custom-assets-internalAsset-table:end
    PRIMARY KEY (id)
);
-- < InternalAsset

-- > InternalAssetChunk
CREATE TABLE ASSETS.InternalAssetChunk (
    id BIGINT NOT NULL,
    byteSize INTEGER,
    content BYTEA,
    internalAsset_id BIGINT,
    isLast BOOL,
    name VARCHAR(255),
    position INTEGER,
    -- @anchor:assets-internalAssetChunk-table:start
    -- @anchor:assets-internalAssetChunk-table:end
    -- anchor:custom-assets-internalAssetChunk-table:start
    -- anchor:custom-assets-internalAssetChunk-table:end
    PRIMARY KEY (id)
);
-- < InternalAssetChunk

-- > Asset
CREATE TABLE ASSETS.Asset (
    id BIGINT NOT NULL,
    byteSize BIGINT,
    complete BOOL,
    contentType VARCHAR(255),
    externalAsset_id BIGINT,
    fileAsset_id BIGINT,
    fileId VARCHAR(255),
    internalAsset_id BIGINT,
    name VARCHAR(255),
    type VARCHAR(255),
    -- @anchor:assets-asset-table:start
    -- @anchor:assets-asset-table:end
    -- anchor:custom-assets-asset-table:start
    -- anchor:custom-assets-asset-table:end
    PRIMARY KEY (id)
);
-- < Asset

-- > SalesOrder
CREATE TABLE ENTERPRISE.SalesOrder (
    id BIGINT NOT NULL,
    asset_id BIGINT,
    client_id BIGINT,
    clientReference VARCHAR(255),
    name VARCHAR(255),
    -- @anchor:enterprise-salesOrder-table:start
    -- @anchor:enterprise-salesOrder-table:end
    -- anchor:custom-enterprise-salesOrder-table:start
    -- anchor:custom-enterprise-salesOrder-table:end
    PRIMARY KEY (id)
);
-- < SalesOrder

-- > Subproject
CREATE TABLE ENTERPRISE.Subproject (
    id BIGINT NOT NULL,
    name VARCHAR(255),
    project_id BIGINT,
    -- @anchor:enterprise-subproject-table:start
    -- @anchor:enterprise-subproject-table:end
    -- anchor:custom-enterprise-subproject-table:start
    -- anchor:custom-enterprise-subproject-table:end
    PRIMARY KEY (id)
);
-- < Subproject

-- > WorkingMonthHours
CREATE TABLE ENTERPRISE.WorkingMonthHours (
    id BIGINT NOT NULL,
    accountManager_id BIGINT,
    chiefEngineer_id BIGINT,
    employee_id BIGINT,
    hoursBillable DOUBLE PRECISION,
    hoursBilled DOUBLE PRECISION,
    hoursPerformed DOUBLE PRECISION,
    project_id BIGINT,
    projectResource_id BIGINT,
    projectSprint_id BIGINT,
    workingMonth_id BIGINT,
    -- @anchor:enterprise-workingMonthHours-table:start
    -- @anchor:enterprise-workingMonthHours-table:end
    -- anchor:custom-enterprise-workingMonthHours-table:start
    -- anchor:custom-enterprise-workingMonthHours-table:end
    PRIMARY KEY (id)
);
-- < WorkingMonthHours

-- > ApplicationServiceType
CREATE TABLE ENTERPRISE.ApplicationServiceType (
    id BIGINT NOT NULL,
    catalogPrice INTEGER,
    name VARCHAR(255),
    productLine_id BIGINT,
    -- @anchor:enterprise-applicationServiceType-table:start
    -- @anchor:enterprise-applicationServiceType-table:end
    -- anchor:custom-enterprise-applicationServiceType-table:start
    -- anchor:custom-enterprise-applicationServiceType-table:end
    PRIMARY KEY (id)
);
-- < ApplicationServiceType

-- > Personalia
CREATE TABLE ENTERPRISE.Personalia (
    id BIGINT NOT NULL,
    bankAccount VARCHAR(255),
    birthPlace VARCHAR(255),
    education VARCHAR(255),
    gender VARCHAR(255),
    idNumber VARCHAR(255),
    maritalStatus VARCHAR(255),
    name VARCHAR(255),
    nationality VARCHAR(255),
    nationalRegister VARCHAR(255),
    partner VARCHAR(255),
    -- @anchor:enterprise-personalia-table:start
    -- @anchor:enterprise-personalia-table:end
    -- anchor:custom-enterprise-personalia-table:start
    -- anchor:custom-enterprise-personalia-table:end
    PRIMARY KEY (id)
);
-- < Personalia

-- > EmployeeAbsence
CREATE TABLE ENTERPRISE.EmployeeAbsence (
    id BIGINT NOT NULL,
    employee_id BIGINT,
    fromDate TIMESTAMP,
    reason VARCHAR(16384),
    toDate TIMESTAMP,
    -- @anchor:enterprise-employeeAbsence-table:start
    -- @anchor:enterprise-employeeAbsence-table:end
    -- anchor:custom-enterprise-employeeAbsence-table:start
    -- anchor:custom-enterprise-employeeAbsence-table:end
    PRIMARY KEY (id)
);
-- < EmployeeAbsence

-- > SoftwareLicenseType
CREATE TABLE ENTERPRISE.SoftwareLicenseType (
    id BIGINT NOT NULL,
    catalogPrice INTEGER,
    name VARCHAR(255),
    productLine_id BIGINT,
    -- @anchor:enterprise-softwareLicenseType-table:start
    -- @anchor:enterprise-softwareLicenseType-table:end
    -- anchor:custom-enterprise-softwareLicenseType-table:start
    -- anchor:custom-enterprise-softwareLicenseType-table:end
    PRIMARY KEY (id)
);
-- < SoftwareLicenseType

-- > SourcerContract
CREATE TABLE ENTERPRISE.SourcerContract (
    id BIGINT NOT NULL,
    asset_id BIGINT,
    name VARCHAR(255),
    sourcer_id BIGINT,
    -- @anchor:enterprise-sourcerContract-table:start
    -- @anchor:enterprise-sourcerContract-table:end
    -- anchor:custom-enterprise-sourcerContract-table:start
    -- anchor:custom-enterprise-sourcerContract-table:end
    PRIMARY KEY (id)
);
-- < SourcerContract

-- > WorkingDayHours
CREATE TABLE ENTERPRISE.WorkingDayHours (
    id BIGINT NOT NULL,
    description VARCHAR(16384),
    hoursPerformed DOUBLE PRECISION,
    projectResource_id BIGINT,
    workingDay TIMESTAMP,
    -- @anchor:enterprise-workingDayHours-table:start
    -- @anchor:enterprise-workingDayHours-table:end
    -- anchor:custom-enterprise-workingDayHours-table:start
    -- anchor:custom-enterprise-workingDayHours-table:end
    PRIMARY KEY (id)
);
-- < WorkingDayHours

-- > Sourcer
CREATE TABLE ENTERPRISE.Sourcer (
    id BIGINT NOT NULL,
    corporation_id BIGINT,
    name VARCHAR(255),
    sourcerType_id BIGINT,
    -- @anchor:enterprise-sourcer-table:start
    -- @anchor:enterprise-sourcer-table:end
    -- anchor:custom-enterprise-sourcer-table:start
    -- anchor:custom-enterprise-sourcer-table:end
    PRIMARY KEY (id)
);
-- < Sourcer

-- > ProjectResourceType
CREATE TABLE ENTERPRISE.ProjectResourceType (
    id BIGINT NOT NULL,
    catalogPrice INTEGER,
    name VARCHAR(255),
    productLine_id BIGINT,
    -- @anchor:enterprise-projectResourceType-table:start
    -- @anchor:enterprise-projectResourceType-table:end
    -- anchor:custom-enterprise-projectResourceType-table:start
    -- anchor:custom-enterprise-projectResourceType-table:end
    PRIMARY KEY (id)
);
-- < ProjectResourceType

-- > ProjectGrossMargin
CREATE TABLE ENTERPRISE.ProjectGrossMargin (
    id BIGINT NOT NULL,
    grossMargin DOUBLE PRECISION,
    period VARCHAR(255),
    periodType VARCHAR(255),
    project_id BIGINT,
    -- @anchor:enterprise-projectGrossMargin-table:start
    -- @anchor:enterprise-projectGrossMargin-table:end
    -- anchor:custom-enterprise-projectGrossMargin-table:start
    -- anchor:custom-enterprise-projectGrossMargin-table:end
    PRIMARY KEY (id)
);
-- < ProjectGrossMargin

-- > WorkingYear
CREATE TABLE ENTERPRISE.WorkingYear (
    id BIGINT NOT NULL,
    name VARCHAR(255),
    -- @anchor:enterprise-workingYear-table:start
    -- @anchor:enterprise-workingYear-table:end
    -- anchor:custom-enterprise-workingYear-table:start
    -- anchor:custom-enterprise-workingYear-table:end
    PRIMARY KEY (id)
);
-- < WorkingYear

-- > EmployeeGrossMargin
CREATE TABLE ENTERPRISE.EmployeeGrossMargin (
    id BIGINT NOT NULL,
    employee_id BIGINT,
    grossMargin DOUBLE PRECISION,
    period VARCHAR(255),
    periodType VARCHAR(255),
    -- @anchor:enterprise-employeeGrossMargin-table:start
    -- @anchor:enterprise-employeeGrossMargin-table:end
    -- anchor:custom-enterprise-employeeGrossMargin-table:start
    -- anchor:custom-enterprise-employeeGrossMargin-table:end
    PRIMARY KEY (id)
);
-- < EmployeeGrossMargin

-- > InvoiceLineService
CREATE TABLE ENTERPRISE.InvoiceLineService (
    id BIGINT NOT NULL,
    applicationService_id BIGINT,
    invoice_id BIGINT,
    -- @anchor:enterprise-invoiceLineService-table:start
    -- @anchor:enterprise-invoiceLineService-table:end
    -- anchor:custom-enterprise-invoiceLineService-table:start
    -- anchor:custom-enterprise-invoiceLineService-table:end
    PRIMARY KEY (id)
);
-- < InvoiceLineService

-- > Country
CREATE TABLE ENTERPRISE.Country (
    id BIGINT NOT NULL,
    name VARCHAR(255),
    -- @anchor:enterprise-country-table:start
    -- @anchor:enterprise-country-table:end
    -- anchor:custom-enterprise-country-table:start
    -- anchor:custom-enterprise-country-table:end
    PRIMARY KEY (id)
);
-- < Country

-- > ClientContract
CREATE TABLE ENTERPRISE.ClientContract (
    id BIGINT NOT NULL,
    asset_id BIGINT,
    client_id BIGINT,
    clientReference VARCHAR(255),
    fromDate TIMESTAMP,
    name VARCHAR(255),
    toDate TIMESTAMP,
    -- @anchor:enterprise-clientContract-table:start
    -- @anchor:enterprise-clientContract-table:end
    -- anchor:custom-enterprise-clientContract-table:start
    -- anchor:custom-enterprise-clientContract-table:end
    PRIMARY KEY (id)
);
-- < ClientContract

-- > InvoiceLineResourceHours
CREATE TABLE ENTERPRISE.InvoiceLineResourceHours (
    id BIGINT NOT NULL,
    employee_id BIGINT,
    hoursPerformed DOUBLE PRECISION,
    invoice_id BIGINT,
    invoicingMode VARCHAR(255),
    project_id BIGINT,
    projectResourceRole_id BIGINT,
    workingMonthHours_id BIGINT,
    -- @anchor:enterprise-invoiceLineResourceHours-table:start
    -- @anchor:enterprise-invoiceLineResourceHours-table:end
    -- anchor:custom-enterprise-invoiceLineResourceHours-table:start
    -- anchor:custom-enterprise-invoiceLineResourceHours-table:end
    PRIMARY KEY (id)
);
-- < InvoiceLineResourceHours

-- > InvoiceLineExpenses
CREATE TABLE ENTERPRISE.InvoiceLineExpenses (
    id BIGINT NOT NULL,
    invoice_id BIGINT,
    invoicingMode VARCHAR(255),
    -- @anchor:enterprise-invoiceLineExpenses-table:start
    -- @anchor:enterprise-invoiceLineExpenses-table:end
    -- anchor:custom-enterprise-invoiceLineExpenses-table:start
    -- anchor:custom-enterprise-invoiceLineExpenses-table:end
    PRIMARY KEY (id)
);
-- < InvoiceLineExpenses

-- > EmployeeHoliday
CREATE TABLE ENTERPRISE.EmployeeHoliday (
    id BIGINT NOT NULL,
    employee_id BIGINT,
    fromDate TIMESTAMP,
    status VARCHAR(255),
    toDate TIMESTAMP,
    -- @anchor:enterprise-employeeHoliday-table:start
    -- @anchor:enterprise-employeeHoliday-table:end
    -- anchor:custom-enterprise-employeeHoliday-table:start
    -- anchor:custom-enterprise-employeeHoliday-table:end
    PRIMARY KEY (id)
);
-- < EmployeeHoliday

-- > EmployeeType
CREATE TABLE ENTERPRISE.EmployeeType (
    id BIGINT NOT NULL,
    name VARCHAR(255),
    -- @anchor:enterprise-employeeType-table:start
    -- @anchor:enterprise-employeeType-table:end
    -- anchor:custom-enterprise-employeeType-table:start
    -- anchor:custom-enterprise-employeeType-table:end
    PRIMARY KEY (id)
);
-- < EmployeeType

-- > Person
CREATE TABLE ENTERPRISE.Person (
    id BIGINT NOT NULL,
    dateOfBirth TIMESTAMP,
    firstName VARCHAR(255),
    lastName VARCHAR(255),
    name VARCHAR(255),
    personalia_id BIGINT,
    privateContact_id BIGINT,
    -- @anchor:enterprise-person-table:start
    -- @anchor:enterprise-person-table:end
    -- anchor:custom-enterprise-person-table:start
    -- anchor:custom-enterprise-person-table:end
    PRIMARY KEY (id)
);
-- < Person

-- > ProjectResourceRole
CREATE TABLE ENTERPRISE.ProjectResourceRole (
    id BIGINT NOT NULL,
    dailyRate DOUBLE PRECISION,
    name VARCHAR(255),
    project_id BIGINT,
    projectResourceType_id BIGINT,
    -- @anchor:enterprise-projectResourceRole-table:start
    -- @anchor:enterprise-projectResourceRole-table:end
    -- anchor:custom-enterprise-projectResourceRole-table:start
    -- anchor:custom-enterprise-projectResourceRole-table:end
    PRIMARY KEY (id)
);
-- < ProjectResourceRole

-- > PurchaseOrder
CREATE TABLE ENTERPRISE.PurchaseOrder (
    id BIGINT NOT NULL,
    name VARCHAR(255),
    supplier_id BIGINT,
    -- @anchor:enterprise-purchaseOrder-table:start
    -- @anchor:enterprise-purchaseOrder-table:end
    -- anchor:custom-enterprise-purchaseOrder-table:start
    -- anchor:custom-enterprise-purchaseOrder-table:end
    PRIMARY KEY (id)
);
-- < PurchaseOrder

-- > Contact
CREATE TABLE ENTERPRISE.Contact (
    id BIGINT NOT NULL,
    address VARCHAR(255),
    city VARCHAR(255),
    country_id BIGINT,
    email VARCHAR(255),
    name VARCHAR(255),
    phone VARCHAR(255),
    zipCode VARCHAR(255),
    -- @anchor:enterprise-contact-table:start
    -- @anchor:enterprise-contact-table:end
    -- anchor:custom-enterprise-contact-table:start
    -- anchor:custom-enterprise-contact-table:end
    PRIMARY KEY (id)
);
-- < Contact

-- > ProductLine
CREATE TABLE ENTERPRISE.ProductLine (
    id BIGINT NOT NULL,
    name VARCHAR(255),
    -- @anchor:enterprise-productLine-table:start
    -- @anchor:enterprise-productLine-table:end
    -- anchor:custom-enterprise-productLine-table:start
    -- anchor:custom-enterprise-productLine-table:end
    PRIMARY KEY (id)
);
-- < ProductLine

-- > TravelHours
CREATE TABLE ENTERPRISE.TravelHours (
    id BIGINT NOT NULL,
    atDay TIMESTAMP,
    hoursSpent DOUBLE PRECISION,
    name VARCHAR(255),
    projectResource_id BIGINT,
    type VARCHAR(255),
    -- @anchor:enterprise-travelHours-table:start
    -- @anchor:enterprise-travelHours-table:end
    -- anchor:custom-enterprise-travelHours-table:start
    -- anchor:custom-enterprise-travelHours-table:end
    PRIMARY KEY (id)
);
-- < TravelHours

-- > Sector
CREATE TABLE ENTERPRISE.Sector (
    id BIGINT NOT NULL,
    name VARCHAR(255),
    -- @anchor:enterprise-sector-table:start
    -- @anchor:enterprise-sector-table:end
    -- anchor:custom-enterprise-sector-table:start
    -- anchor:custom-enterprise-sector-table:end
    PRIMARY KEY (id)
);
-- < Sector

-- > PurchaseLineResourceHours
CREATE TABLE ENTERPRISE.PurchaseLineResourceHours (
    id BIGINT NOT NULL,
    employee_id BIGINT,
    hoursPerformed DOUBLE PRECISION,
    projectResourceType_id BIGINT,
    purchaseInvoice_id BIGINT,
    totalAmount DOUBLE PRECISION,
    workingMonthHours_id BIGINT,
    -- @anchor:enterprise-purchaseLineResourceHours-table:start
    -- @anchor:enterprise-purchaseLineResourceHours-table:end
    -- anchor:custom-enterprise-purchaseLineResourceHours-table:start
    -- anchor:custom-enterprise-purchaseLineResourceHours-table:end
    PRIMARY KEY (id)
);
-- < PurchaseLineResourceHours

-- > Project
CREATE TABLE ENTERPRISE.Project (
    id BIGINT NOT NULL,
    client_id BIGINT,
    clientReference VARCHAR(255),
    clientResponsible_id BIGINT,
    developmentBudget DOUBLE PRECISION,
    domainAnalyst_id BIGINT,
    internalCode VARCHAR(255),
    invoicingMode VARCHAR(255),
    maintenanceBudget DOUBLE PRECISION,
    name VARCHAR(255),
    order_id BIGINT,
    projectManager_id BIGINT,
    technicalLead_id BIGINT,
    -- @anchor:enterprise-project-table:start
    -- @anchor:enterprise-project-table:end
    -- anchor:custom-enterprise-project-table:start
    -- anchor:custom-enterprise-project-table:end
    PRIMARY KEY (id)
);
-- < Project

-- > Corporation
CREATE TABLE ENTERPRISE.Corporation (
    id BIGINT NOT NULL,
    bankAccount VARCHAR(255),
    country_id BIGINT,
    legalForm VARCHAR(255),
    mainContact_id BIGINT,
    name VARCHAR(255),
    vatNumber VARCHAR(255),
    -- @anchor:enterprise-corporation-table:start
    -- @anchor:enterprise-corporation-table:end
    -- anchor:custom-enterprise-corporation-table:start
    -- anchor:custom-enterprise-corporation-table:end
    PRIMARY KEY (id)
);
-- < Corporation

-- > Employee
CREATE TABLE ENTERPRISE.Employee (
    id BIGINT NOT NULL,
    active VARCHAR(255),
    contractType VARCHAR(255),
    email VARCHAR(255),
    employeeType_id BIGINT,
    mobile VARCHAR(255),
    name VARCHAR(255),
    person_id BIGINT,
    sourcer_id BIGINT,
    -- @anchor:enterprise-employee-table:start
    -- @anchor:enterprise-employee-table:end
    -- anchor:custom-enterprise-employee-table:start
    -- anchor:custom-enterprise-employee-table:end
    PRIMARY KEY (id)
);
-- < Employee

-- > InvoiceLineLicense
CREATE TABLE ENTERPRISE.InvoiceLineLicense (
    id BIGINT NOT NULL,
    invoice_id BIGINT,
    softwareLicense_id BIGINT,
    -- @anchor:enterprise-invoiceLineLicense-table:start
    -- @anchor:enterprise-invoiceLineLicense-table:end
    -- anchor:custom-enterprise-invoiceLineLicense-table:start
    -- anchor:custom-enterprise-invoiceLineLicense-table:end
    PRIMARY KEY (id)
);
-- < InvoiceLineLicense

-- > EmployeeSalaryRate
CREATE TABLE ENTERPRISE.EmployeeSalaryRate (
    id BIGINT NOT NULL,
    companyCarValue INTEGER,
    employee_id BIGINT,
    fromDate TIMESTAMP,
    hasCompanyCar BOOL,
    monthlySalary DOUBLE PRECISION,
    toDate TIMESTAMP,
    -- @anchor:enterprise-employeeSalaryRate-table:start
    -- @anchor:enterprise-employeeSalaryRate-table:end
    -- anchor:custom-enterprise-employeeSalaryRate-table:start
    -- anchor:custom-enterprise-employeeSalaryRate-table:end
    PRIMARY KEY (id)
);
-- < EmployeeSalaryRate

-- > Supplier
CREATE TABLE ENTERPRISE.Supplier (
    id BIGINT NOT NULL,
    legalForm VARCHAR(255),
    mainContact_id BIGINT,
    name VARCHAR(255),
    sourcer_id BIGINT,
    supplierType VARCHAR(255),
    -- @anchor:enterprise-supplier-table:start
    -- @anchor:enterprise-supplier-table:end
    -- anchor:custom-enterprise-supplier-table:start
    -- anchor:custom-enterprise-supplier-table:end
    PRIMARY KEY (id)
);
-- < Supplier

-- > SourcerType
CREATE TABLE ENTERPRISE.SourcerType (
    id BIGINT NOT NULL,
    name VARCHAR(255),
    -- @anchor:enterprise-sourcerType-table:start
    -- @anchor:enterprise-sourcerType-table:end
    -- anchor:custom-enterprise-sourcerType-table:start
    -- anchor:custom-enterprise-sourcerType-table:end
    PRIMARY KEY (id)
);
-- < SourcerType

-- > EmployeeContract
CREATE TABLE ENTERPRISE.EmployeeContract (
    id BIGINT NOT NULL,
    employee_id BIGINT,
    name VARCHAR(255),
    -- @anchor:enterprise-employeeContract-table:start
    -- @anchor:enterprise-employeeContract-table:end
    -- anchor:custom-enterprise-employeeContract-table:start
    -- anchor:custom-enterprise-employeeContract-table:end
    PRIMARY KEY (id)
);
-- < EmployeeContract

-- > SourcerPersonalContact
CREATE TABLE ENTERPRISE.SourcerPersonalContact (
    id BIGINT NOT NULL,
    contact_id BIGINT,
    name VARCHAR(255),
    person_id BIGINT,
    position VARCHAR(255),
    sourcer_id BIGINT,
    -- @anchor:enterprise-sourcerPersonalContact-table:start
    -- @anchor:enterprise-sourcerPersonalContact-table:end
    -- anchor:custom-enterprise-sourcerPersonalContact-table:start
    -- anchor:custom-enterprise-sourcerPersonalContact-table:end
    PRIMARY KEY (id)
);
-- < SourcerPersonalContact

-- > ClientCorporateContact
CREATE TABLE ENTERPRISE.ClientCorporateContact (
    id BIGINT NOT NULL,
    affiliation VARCHAR(255),
    client_id BIGINT,
    contact_id BIGINT,
    name VARCHAR(255),
    -- @anchor:enterprise-clientCorporateContact-table:start
    -- @anchor:enterprise-clientCorporateContact-table:end
    -- anchor:custom-enterprise-clientCorporateContact-table:start
    -- anchor:custom-enterprise-clientCorporateContact-table:end
    PRIMARY KEY (id)
);
-- < ClientCorporateContact

-- > WorkingMonth
CREATE TABLE ENTERPRISE.WorkingMonth (
    id BIGINT NOT NULL,
    month VARCHAR(255),
    name VARCHAR(255),
    year_id BIGINT,
    -- @anchor:enterprise-workingMonth-table:start
    -- @anchor:enterprise-workingMonth-table:end
    -- anchor:custom-enterprise-workingMonth-table:start
    -- anchor:custom-enterprise-workingMonth-table:end
    PRIMARY KEY (id)
);
-- < WorkingMonth

-- > ProjectResource
CREATE TABLE ENTERPRISE.ProjectResource (
    id BIGINT NOT NULL,
    employee_id BIGINT,
    name VARCHAR(255),
    project_id BIGINT,
    projectResourceRole_id BIGINT,
    -- @anchor:enterprise-projectResource-table:start
    -- @anchor:enterprise-projectResource-table:end
    -- anchor:custom-enterprise-projectResource-table:start
    -- anchor:custom-enterprise-projectResource-table:end
    PRIMARY KEY (id)
);
-- < ProjectResource

-- > EmployeeHourlyRate
CREATE TABLE ENTERPRISE.EmployeeHourlyRate (
    id BIGINT NOT NULL,
    employee_id BIGINT,
    fromDate TIMESTAMP,
    hourlyRate DOUBLE PRECISION,
    toDate TIMESTAMP,
    -- @anchor:enterprise-employeeHourlyRate-table:start
    -- @anchor:enterprise-employeeHourlyRate-table:end
    -- anchor:custom-enterprise-employeeHourlyRate-table:start
    -- anchor:custom-enterprise-employeeHourlyRate-table:end
    PRIMARY KEY (id)
);
-- < EmployeeHourlyRate

-- > SourcerCorporateContact
CREATE TABLE ENTERPRISE.SourcerCorporateContact (
    id BIGINT NOT NULL,
    affiliation VARCHAR(255),
    contact_id BIGINT,
    name VARCHAR(255),
    sourcer_id BIGINT,
    -- @anchor:enterprise-sourcerCorporateContact-table:start
    -- @anchor:enterprise-sourcerCorporateContact-table:end
    -- anchor:custom-enterprise-sourcerCorporateContact-table:start
    -- anchor:custom-enterprise-sourcerCorporateContact-table:end
    PRIMARY KEY (id)
);
-- < SourcerCorporateContact

-- > SalesInvoice
CREATE TABLE ENTERPRISE.SalesInvoice (
    id BIGINT NOT NULL,
    accountManager_id BIGINT,
    chiefEngineer_id BIGINT,
    client_id BIGINT,
    dateOfInvoice TIMESTAMP,
    dateOfPayment TIMESTAMP,
    description VARCHAR(255),
    name VARCHAR(255),
    payment VARCHAR(255),
    project_id BIGINT,
    salesOrder_id BIGINT,
    totalExclVat DOUBLE PRECISION,
    totalInclVat DOUBLE PRECISION,
    totalVat DOUBLE PRECISION,
    -- @anchor:enterprise-salesInvoice-table:start
    -- @anchor:enterprise-salesInvoice-table:end
    -- anchor:custom-enterprise-salesInvoice-table:start
    -- anchor:custom-enterprise-salesInvoice-table:end
    PRIMARY KEY (id)
);
-- < SalesInvoice

-- > WorkingExpenses
CREATE TABLE ENTERPRISE.WorkingExpenses (
    id BIGINT NOT NULL,
    atDate TIMESTAMP,
    description VARCHAR(16384),
    projectResource_id BIGINT,
    -- @anchor:enterprise-workingExpenses-table:start
    -- @anchor:enterprise-workingExpenses-table:end
    -- anchor:custom-enterprise-workingExpenses-table:start
    -- anchor:custom-enterprise-workingExpenses-table:end
    PRIMARY KEY (id)
);
-- < WorkingExpenses

-- > PurchaseInvoice
CREATE TABLE ENTERPRISE.PurchaseInvoice (
    id BIGINT NOT NULL,
    dateOfInvoice TIMESTAMP,
    description VARCHAR(255),
    name VARCHAR(255),
    project_id BIGINT,
    purchaseOrder_id BIGINT,
    supplier_id BIGINT,
    totalExclVat DOUBLE PRECISION,
    totalInclVat DOUBLE PRECISION,
    totalVat DOUBLE PRECISION,
    -- @anchor:enterprise-purchaseInvoice-table:start
    -- @anchor:enterprise-purchaseInvoice-table:end
    -- anchor:custom-enterprise-purchaseInvoice-table:start
    -- anchor:custom-enterprise-purchaseInvoice-table:end
    PRIMARY KEY (id)
);
-- < PurchaseInvoice

-- > ClientPersonalContact
CREATE TABLE ENTERPRISE.ClientPersonalContact (
    id BIGINT NOT NULL,
    client_id BIGINT,
    contact_id BIGINT,
    name VARCHAR(255),
    person_id BIGINT,
    position VARCHAR(255),
    -- @anchor:enterprise-clientPersonalContact-table:start
    -- @anchor:enterprise-clientPersonalContact-table:end
    -- anchor:custom-enterprise-clientPersonalContact-table:start
    -- anchor:custom-enterprise-clientPersonalContact-table:end
    PRIMARY KEY (id)
);
-- < ClientPersonalContact

-- > SoftwareLicense
CREATE TABLE ENTERPRISE.SoftwareLicense (
    id BIGINT NOT NULL,
    amount INTEGER,
    client_id BIGINT,
    licensePrice INTEGER,
    softwareLicenseType_id BIGINT,
    workingYear_id BIGINT,
    -- @anchor:enterprise-softwareLicense-table:start
    -- @anchor:enterprise-softwareLicense-table:end
    -- anchor:custom-enterprise-softwareLicense-table:start
    -- anchor:custom-enterprise-softwareLicense-table:end
    PRIMARY KEY (id)
);
-- < SoftwareLicense

-- > Client
CREATE TABLE ENTERPRISE.Client (
    id BIGINT NOT NULL,
    accountManager_id BIGINT,
    corporation_id BIGINT,
    country_id BIGINT,
    name VARCHAR(255),
    sector_id BIGINT,
    status VARCHAR(255),
    -- @anchor:enterprise-client-table:start
    -- @anchor:enterprise-client-table:end
    -- anchor:custom-enterprise-client-table:start
    -- anchor:custom-enterprise-client-table:end
    PRIMARY KEY (id)
);
-- < Client

-- > ApplicationService
CREATE TABLE ENTERPRISE.ApplicationService (
    id BIGINT NOT NULL,
    applicationServiceType_id BIGINT,
    client_id BIGINT,
    name VARCHAR(255),
    servicePrice INTEGER,
    workingYear_id BIGINT,
    -- @anchor:enterprise-applicationService-table:start
    -- @anchor:enterprise-applicationService-table:end
    -- anchor:custom-enterprise-applicationService-table:start
    -- anchor:custom-enterprise-applicationService-table:end
    PRIMARY KEY (id)
);
-- < ApplicationService

-- > ProjectSprint
CREATE TABLE ENTERPRISE.ProjectSprint (
    id BIGINT NOT NULL,
    completedAt TIMESTAMP,
    description VARCHAR(16384),
    fromDate TIMESTAMP,
    name VARCHAR(255),
    project_id BIGINT,
    status VARCHAR(255),
    toDate TIMESTAMP,
    -- @anchor:enterprise-projectSprint-table:start
    -- @anchor:enterprise-projectSprint-table:end
    -- anchor:custom-enterprise-projectSprint-table:start
    -- anchor:custom-enterprise-projectSprint-table:end
    PRIMARY KEY (id)
);
-- < ProjectSprint

-- > TimesheetEntryTaskStatus
CREATE TABLE ERPTIMESHEETS.TimesheetEntryTaskStatus (
    id BIGINT NOT NULL,
    finishedAt TIMESTAMP,
    name VARCHAR(255),
    startedAt TIMESTAMP,
    stateTask_id BIGINT,
    status VARCHAR(255),
    timesheetEntry_id BIGINT,
    -- @anchor:erpTimesheets-timesheetEntryTaskStatus-table:start
    -- @anchor:erpTimesheets-timesheetEntryTaskStatus-table:end
    -- anchor:custom-erpTimesheets-timesheetEntryTaskStatus-table:start
    -- anchor:custom-erpTimesheets-timesheetEntryTaskStatus-table:end
    PRIMARY KEY (id)
);
-- < TimesheetEntryTaskStatus

-- > TimesheetRemark
CREATE TABLE ERPTIMESHEETS.TimesheetRemark (
    id BIGINT NOT NULL,
    createdBy_id BIGINT,
    name VARCHAR(255),
    remark VARCHAR(16384),
    timesheet_id BIGINT,
    -- @anchor:erpTimesheets-timesheetRemark-table:start
    -- @anchor:erpTimesheets-timesheetRemark-table:end
    -- anchor:custom-erpTimesheets-timesheetRemark-table:start
    -- anchor:custom-erpTimesheets-timesheetRemark-table:end
    PRIMARY KEY (id)
);
-- < TimesheetRemark

-- > ExpenseDocumentTaskStatus
CREATE TABLE ERPTIMESHEETS.ExpenseDocumentTaskStatus (
    id BIGINT NOT NULL,
    expenseDocument_id BIGINT,
    finishedAt TIMESTAMP,
    name VARCHAR(255),
    startedAt TIMESTAMP,
    stateTask_id BIGINT,
    status VARCHAR(255),
    -- @anchor:erpTimesheets-expenseDocumentTaskStatus-table:start
    -- @anchor:erpTimesheets-expenseDocumentTaskStatus-table:end
    -- anchor:custom-erpTimesheets-expenseDocumentTaskStatus-table:start
    -- anchor:custom-erpTimesheets-expenseDocumentTaskStatus-table:end
    PRIMARY KEY (id)
);
-- < ExpenseDocumentTaskStatus

-- > Expense
CREATE TABLE ERPTIMESHEETS.Expense (
    id BIGINT NOT NULL,
    cost DOUBLE PRECISION,
    currency_id BIGINT,
    description VARCHAR(255),
    name VARCHAR(255),
    status VARCHAR(255),
    timesheetEntry_id BIGINT,
    -- @anchor:erpTimesheets-expense-table:start
    -- @anchor:erpTimesheets-expense-table:end
    -- anchor:custom-erpTimesheets-expense-table:start
    -- anchor:custom-erpTimesheets-expense-table:end
    PRIMARY KEY (id)
);
-- < Expense

-- > ExpenseTaskStatus
CREATE TABLE ERPTIMESHEETS.ExpenseTaskStatus (
    id BIGINT NOT NULL,
    expense_id BIGINT,
    finishedAt TIMESTAMP,
    name VARCHAR(255),
    startedAt TIMESTAMP,
    stateTask_id BIGINT,
    status VARCHAR(255),
    -- @anchor:erpTimesheets-expenseTaskStatus-table:start
    -- @anchor:erpTimesheets-expenseTaskStatus-table:end
    -- anchor:custom-erpTimesheets-expenseTaskStatus-table:start
    -- anchor:custom-erpTimesheets-expenseTaskStatus-table:end
    PRIMARY KEY (id)
);
-- < ExpenseTaskStatus

-- > Currency
CREATE TABLE ERPTIMESHEETS.Currency (
    id BIGINT NOT NULL,
    name VARCHAR(255),
    shortName VARCHAR(255),
    -- @anchor:erpTimesheets-currency-table:start
    -- @anchor:erpTimesheets-currency-table:end
    -- anchor:custom-erpTimesheets-currency-table:start
    -- anchor:custom-erpTimesheets-currency-table:end
    PRIMARY KEY (id)
);
-- < Currency

-- > TimesheetTaskStatus
CREATE TABLE ERPTIMESHEETS.TimesheetTaskStatus (
    id BIGINT NOT NULL,
    finishedAt TIMESTAMP,
    name VARCHAR(255),
    startedAt TIMESTAMP,
    stateTask_id BIGINT,
    status VARCHAR(255),
    timesheet_id BIGINT,
    -- @anchor:erpTimesheets-timesheetTaskStatus-table:start
    -- @anchor:erpTimesheets-timesheetTaskStatus-table:end
    -- anchor:custom-erpTimesheets-timesheetTaskStatus-table:start
    -- anchor:custom-erpTimesheets-timesheetTaskStatus-table:end
    PRIMARY KEY (id)
);
-- < TimesheetTaskStatus

-- > ExpenseDocument
CREATE TABLE ERPTIMESHEETS.ExpenseDocument (
    id BIGINT NOT NULL,
    description VARCHAR(255),
    document_id BIGINT,
    expense_id BIGINT,
    name VARCHAR(255),
    status VARCHAR(255),
    -- @anchor:erpTimesheets-expenseDocument-table:start
    -- @anchor:erpTimesheets-expenseDocument-table:end
    -- anchor:custom-erpTimesheets-expenseDocument-table:start
    -- anchor:custom-erpTimesheets-expenseDocument-table:end
    PRIMARY KEY (id)
);
-- < ExpenseDocument

-- > TimesheetEntry
CREATE TABLE ERPTIMESHEETS.TimesheetEntry (
    id BIGINT NOT NULL,
    date TIMESTAMP,
    description VARCHAR(16384),
    employee_id BIGINT,
    hours DOUBLE PRECISION,
    name VARCHAR(255),
    project_id BIGINT,
    status VARCHAR(255),
    subproject_id BIGINT,
    timesheet_id BIGINT,
    user_id BIGINT,
    -- @anchor:erpTimesheets-timesheetEntry-table:start
    -- @anchor:erpTimesheets-timesheetEntry-table:end
    -- anchor:custom-erpTimesheets-timesheetEntry-table:start
    -- anchor:custom-erpTimesheets-timesheetEntry-table:end
    PRIMARY KEY (id)
);
-- < TimesheetEntry

-- > TimesheetProvisioning
CREATE TABLE ERPTIMESHEETS.TimesheetProvisioning (
    id BIGINT NOT NULL,
    name VARCHAR(255),
    status VARCHAR(255),
    -- @anchor:erpTimesheets-timesheetProvisioning-table:start
    -- @anchor:erpTimesheets-timesheetProvisioning-table:end
    -- anchor:custom-erpTimesheets-timesheetProvisioning-table:start
    -- anchor:custom-erpTimesheets-timesheetProvisioning-table:end
    PRIMARY KEY (id)
);
-- < TimesheetProvisioning

-- > TimesheetProvisioningTaskStatus
CREATE TABLE ERPTIMESHEETS.TimesheetProvisioningTaskStatus (
    id BIGINT NOT NULL,
    finishedAt TIMESTAMP,
    name VARCHAR(255),
    startedAt TIMESTAMP,
    stateTask_id BIGINT,
    status VARCHAR(255),
    timesheetProvisioning_id BIGINT,
    -- @anchor:erpTimesheets-timesheetProvisioningTaskStatus-table:start
    -- @anchor:erpTimesheets-timesheetProvisioningTaskStatus-table:end
    -- anchor:custom-erpTimesheets-timesheetProvisioningTaskStatus-table:start
    -- anchor:custom-erpTimesheets-timesheetProvisioningTaskStatus-table:end
    PRIMARY KEY (id)
);
-- < TimesheetProvisioningTaskStatus

-- > Timesheet
CREATE TABLE ERPTIMESHEETS.Timesheet (
    id BIGINT NOT NULL,
    employee_id BIGINT,
    month INTEGER,
    name VARCHAR(255),
    status VARCHAR(255),
    user_id BIGINT,
    year INTEGER,
    -- @anchor:erpTimesheets-timesheet-table:start
    -- @anchor:erpTimesheets-timesheet-table:end
    -- anchor:custom-erpTimesheets-timesheet-table:start
    -- anchor:custom-erpTimesheets-timesheet-table:end
    PRIMARY KEY (id)
);
-- < Timesheet

-- > Note
CREATE TABLE CRMMODEL.Note (
    id BIGINT NOT NULL,
    adventure_id BIGINT,
    content VARCHAR(16384),
    created TIMESTAMP,
    file_id BIGINT,
    name VARCHAR(255),
    owner_id BIGINT,
    -- @anchor:crmModel-note-table:start
    -- @anchor:crmModel-note-table:end
    -- anchor:custom-crmModel-note-table:start
    -- anchor:custom-crmModel-note-table:end
    PRIMARY KEY (id)
);
-- < Note

-- > Adventure
CREATE TABLE CRMMODEL.Adventure (
    id BIGINT NOT NULL,
    active BOOL,
    corporation_id BIGINT,
    coverPhoto VARCHAR(255),
    name VARCHAR(255),
    -- @anchor:crmModel-adventure-table:start
    -- @anchor:crmModel-adventure-table:end
    -- anchor:custom-crmModel-adventure-table:start
    -- anchor:custom-crmModel-adventure-table:end
    PRIMARY KEY (id)
);
-- < Adventure

-- > Activity
CREATE TABLE CRMMODEL.Activity (
    id BIGINT NOT NULL,
    adventure_id BIGINT,
    controller_id BIGINT,
    description VARCHAR(255),
    done BOOL,
    name VARCHAR(255),
    timestamp TIMESTAMP,
    -- @anchor:crmModel-activity-table:start
    -- @anchor:crmModel-activity-table:end
    -- anchor:custom-crmModel-activity-table:start
    -- anchor:custom-crmModel-activity-table:end
    PRIMARY KEY (id)
);
-- < Activity
-- anchor:tables:end

-- ------------------ --
-- Create Join Tables --
-- ------------------ --

-- anchor:join-tables:start
-- > ParamTargetValue
-- < ParamTargetValue

-- > Execution
-- < Execution

-- > Thumbnail
-- < Thumbnail

-- > TagValuePair
-- < TagValuePair

-- > IdCounter
-- < IdCounter

-- > Validation
-- < Validation

-- > DataAccess
CREATE TABLE ACCOUNT.dataAccess_userGroups (
    dataAccess_id BIGINT,
    userGroup_id BIGINT
    -- @anchor:account-dataAccess-userGroups-jointable:start
    -- @anchor:account-dataAccess-userGroups-jointable:end
    -- anchor:custom-account-dataAccess-userGroups-jointable:start
    -- anchor:custom-account-dataAccess-userGroups-jointable:end
);

ALTER TABLE ACCOUNT.dataAccess_userGroups
ADD FOREIGN KEY (dataAccess_id)
REFERENCES ACCOUNT.DataAccess (id) DEFERRABLE;

ALTER TABLE ACCOUNT.dataAccess_userGroups
ADD FOREIGN KEY (userGroup_id)
REFERENCES ACCOUNT.UserGroup (id) DEFERRABLE;
-- < DataAccess

-- > HelpInfo
-- < HelpInfo

-- > Account
-- < Account

-- > User
CREATE TABLE ACCOUNT.user_userGroups (
    user_id BIGINT,
    userGroup_id BIGINT
    -- @anchor:account-user-userGroups-jointable:start
    -- @anchor:account-user-userGroups-jointable:end
    -- anchor:custom-account-user-userGroups-jointable:start
    -- anchor:custom-account-user-userGroups-jointable:end
);

ALTER TABLE ACCOUNT.user_userGroups
ADD FOREIGN KEY (user_id)
REFERENCES ACCOUNT.User (id) DEFERRABLE;

ALTER TABLE ACCOUNT.user_userGroups
ADD FOREIGN KEY (userGroup_id)
REFERENCES ACCOUNT.UserGroup (id) DEFERRABLE;
-- < User

-- > Portal
-- < Portal

-- > Menu
-- < Menu

-- > Component
-- < Component

-- > Screen
-- < Screen

-- > MenuItem
-- < MenuItem

-- > Profile
CREATE TABLE ACCOUNT.profile_screens (
    profile_id BIGINT,
    screen_id BIGINT
    -- @anchor:account-profile-screens-jointable:start
    -- @anchor:account-profile-screens-jointable:end
    -- anchor:custom-account-profile-screens-jointable:start
    -- anchor:custom-account-profile-screens-jointable:end
);

ALTER TABLE ACCOUNT.profile_screens
ADD FOREIGN KEY (profile_id)
REFERENCES ACCOUNT.Profile (id) DEFERRABLE;

ALTER TABLE ACCOUNT.profile_screens
ADD FOREIGN KEY (screen_id)
REFERENCES ACCOUNT.Screen (id) DEFERRABLE;
-- < Profile

-- > ScreenProfile
CREATE TABLE ACCOUNT.screenProfile_screens (
    screenProfile_id BIGINT,
    screen_id BIGINT
    -- @anchor:account-screenProfile-screens-jointable:start
    -- @anchor:account-screenProfile-screens-jointable:end
    -- anchor:custom-account-screenProfile-screens-jointable:start
    -- anchor:custom-account-screenProfile-screens-jointable:end
);

ALTER TABLE ACCOUNT.screenProfile_screens
ADD FOREIGN KEY (screenProfile_id)
REFERENCES ACCOUNT.ScreenProfile (id) DEFERRABLE;

ALTER TABLE ACCOUNT.screenProfile_screens
ADD FOREIGN KEY (screen_id)
REFERENCES ACCOUNT.Screen (id) DEFERRABLE;
-- < ScreenProfile

-- > UserGroup
-- < UserGroup

-- > TimeTask
-- < TimeTask

-- > TimeWindow
-- < TimeWindow

-- > StateTask
-- < StateTask

-- > EngineNode
-- < EngineNode

-- > EngineService
-- < EngineService

-- > EngineNodeService
-- < EngineNodeService

-- > TimeWindowGroup
CREATE TABLE WORKFLOW.timeWindowGroup_timeWindows (
    timeWindowGroup_id BIGINT,
    timeWindow_id BIGINT
    -- @anchor:workflow-timeWindowGroup-timeWindows-jointable:start
    -- @anchor:workflow-timeWindowGroup-timeWindows-jointable:end
    -- anchor:custom-workflow-timeWindowGroup-timeWindows-jointable:start
    -- anchor:custom-workflow-timeWindowGroup-timeWindows-jointable:end
);

ALTER TABLE WORKFLOW.timeWindowGroup_timeWindows
ADD FOREIGN KEY (timeWindowGroup_id)
REFERENCES WORKFLOW.TimeWindowGroup (id) DEFERRABLE;

ALTER TABLE WORKFLOW.timeWindowGroup_timeWindows
ADD FOREIGN KEY (timeWindow_id)
REFERENCES WORKFLOW.TimeWindow (id) DEFERRABLE;
-- < TimeWindowGroup

-- > Workflow
-- < Workflow

-- > StateTimer
-- < StateTimer

-- > RemoteAsset
-- < RemoteAsset

-- > ExternalAsset
-- < ExternalAsset

-- > FileAsset
-- < FileAsset

-- > InternalAsset
-- < InternalAsset

-- > InternalAssetChunk
-- < InternalAssetChunk

-- > Asset
-- < Asset

-- > SalesOrder
-- < SalesOrder

-- > Subproject
-- < Subproject

-- > WorkingMonthHours
-- < WorkingMonthHours

-- > ApplicationServiceType
-- < ApplicationServiceType

-- > Personalia
-- < Personalia

-- > EmployeeAbsence
-- < EmployeeAbsence

-- > SoftwareLicenseType
-- < SoftwareLicenseType

-- > SourcerContract
-- < SourcerContract

-- > WorkingDayHours
-- < WorkingDayHours

-- > Sourcer
-- < Sourcer

-- > ProjectResourceType
-- < ProjectResourceType

-- > ProjectGrossMargin
-- < ProjectGrossMargin

-- > WorkingYear
-- < WorkingYear

-- > EmployeeGrossMargin
-- < EmployeeGrossMargin

-- > InvoiceLineService
-- < InvoiceLineService

-- > Country
-- < Country

-- > ClientContract
-- < ClientContract

-- > InvoiceLineResourceHours
-- < InvoiceLineResourceHours

-- > InvoiceLineExpenses
-- < InvoiceLineExpenses

-- > EmployeeHoliday
-- < EmployeeHoliday

-- > EmployeeType
-- < EmployeeType

-- > Person
-- < Person

-- > ProjectResourceRole
-- < ProjectResourceRole

-- > PurchaseOrder
-- < PurchaseOrder

-- > Contact
-- < Contact

-- > ProductLine
-- < ProductLine

-- > TravelHours
-- < TravelHours

-- > Sector
-- < Sector

-- > PurchaseLineResourceHours
-- < PurchaseLineResourceHours

-- > Project
-- < Project

-- > Corporation
-- < Corporation

-- > Employee
-- < Employee

-- > InvoiceLineLicense
-- < InvoiceLineLicense

-- > EmployeeSalaryRate
-- < EmployeeSalaryRate

-- > Supplier
-- < Supplier

-- > SourcerType
-- < SourcerType

-- > EmployeeContract
-- < EmployeeContract

-- > SourcerPersonalContact
-- < SourcerPersonalContact

-- > ClientCorporateContact
-- < ClientCorporateContact

-- > WorkingMonth
-- < WorkingMonth

-- > ProjectResource
-- < ProjectResource

-- > EmployeeHourlyRate
-- < EmployeeHourlyRate

-- > SourcerCorporateContact
-- < SourcerCorporateContact

-- > SalesInvoice
-- < SalesInvoice

-- > WorkingExpenses
-- < WorkingExpenses

-- > PurchaseInvoice
-- < PurchaseInvoice

-- > ClientPersonalContact
-- < ClientPersonalContact

-- > SoftwareLicense
-- < SoftwareLicense

-- > Client
-- < Client

-- > ApplicationService
-- < ApplicationService

-- > ProjectSprint
-- < ProjectSprint

-- > TimesheetEntryTaskStatus
-- < TimesheetEntryTaskStatus

-- > TimesheetRemark
-- < TimesheetRemark

-- > ExpenseDocumentTaskStatus
-- < ExpenseDocumentTaskStatus

-- > Expense
-- < Expense

-- > ExpenseTaskStatus
-- < ExpenseTaskStatus

-- > Currency
-- < Currency

-- > TimesheetTaskStatus
-- < TimesheetTaskStatus

-- > ExpenseDocument
-- < ExpenseDocument

-- > TimesheetEntry
-- < TimesheetEntry

-- > TimesheetProvisioning
-- < TimesheetProvisioning

-- > TimesheetProvisioningTaskStatus
-- < TimesheetProvisioningTaskStatus

-- > Timesheet
-- < Timesheet

-- > Note
-- < Note

-- > Adventure
-- < Adventure

-- > Activity
-- < Activity
-- anchor:join-tables:end

-- ------------------ --
-- Add Foreign Keys   --
-- ------------------ --

-- anchor:foreign-keys:start
-- > ParamTargetValue
-- < ParamTargetValue

-- > Execution
-- < Execution

-- > Thumbnail
-- < Thumbnail

-- > TagValuePair
-- < TagValuePair

-- > IdCounter
-- < IdCounter

-- > Validation
-- < Validation

-- > DataAccess
-- < DataAccess

-- > HelpInfo
-- < HelpInfo

-- > Account
-- < Account

-- > User
-- < User

-- > Portal
-- < Portal

-- > Menu
-- < Menu

-- > Component
-- < Component

-- > Screen
-- < Screen

-- > MenuItem
-- < MenuItem

-- > Profile
-- < Profile

-- > ScreenProfile
-- < ScreenProfile

-- > UserGroup
-- < UserGroup

-- > TimeTask
-- < TimeTask

-- > TimeWindow
-- < TimeWindow

-- > StateTask
-- < StateTask

-- > EngineNode
-- < EngineNode

-- > EngineService
-- < EngineService

-- > EngineNodeService
ALTER TABLE WORKFLOW.EngineNodeService
ADD FOREIGN KEY (engineNode_id)
REFERENCES WORKFLOW.EngineNode (id) DEFERRABLE;

ALTER TABLE WORKFLOW.EngineNodeService
ADD FOREIGN KEY (engineService_id)
REFERENCES WORKFLOW.EngineService (id) DEFERRABLE;
-- < EngineNodeService

-- > TimeWindowGroup
-- < TimeWindowGroup

-- > Workflow
-- < Workflow

-- > StateTimer
-- < StateTimer

-- > RemoteAsset
-- < RemoteAsset

-- > ExternalAsset
-- < ExternalAsset

-- > FileAsset
-- < FileAsset

-- > InternalAsset
-- < InternalAsset

-- > InternalAssetChunk
ALTER TABLE ASSETS.InternalAssetChunk
ADD FOREIGN KEY (internalAsset_id)
REFERENCES ASSETS.InternalAsset (id) DEFERRABLE;
-- < InternalAssetChunk

-- > Asset
-- < Asset

-- > SalesOrder
-- < SalesOrder

-- > Subproject
-- < Subproject

-- > WorkingMonthHours
-- < WorkingMonthHours

-- > ApplicationServiceType
-- < ApplicationServiceType

-- > Personalia
-- < Personalia

-- > EmployeeAbsence
-- < EmployeeAbsence

-- > SoftwareLicenseType
-- < SoftwareLicenseType

-- > SourcerContract
-- < SourcerContract

-- > WorkingDayHours
-- < WorkingDayHours

-- > Sourcer
-- < Sourcer

-- > ProjectResourceType
-- < ProjectResourceType

-- > ProjectGrossMargin
-- < ProjectGrossMargin

-- > WorkingYear
-- < WorkingYear

-- > EmployeeGrossMargin
-- < EmployeeGrossMargin

-- > InvoiceLineService
-- < InvoiceLineService

-- > Country
-- < Country

-- > ClientContract
-- < ClientContract

-- > InvoiceLineResourceHours
-- < InvoiceLineResourceHours

-- > InvoiceLineExpenses
-- < InvoiceLineExpenses

-- > EmployeeHoliday
-- < EmployeeHoliday

-- > EmployeeType
-- < EmployeeType

-- > Person
-- < Person

-- > ProjectResourceRole
-- < ProjectResourceRole

-- > PurchaseOrder
-- < PurchaseOrder

-- > Contact
-- < Contact

-- > ProductLine
-- < ProductLine

-- > TravelHours
-- < TravelHours

-- > Sector
-- < Sector

-- > PurchaseLineResourceHours
-- < PurchaseLineResourceHours

-- > Project
-- < Project

-- > Corporation
-- < Corporation

-- > Employee
-- < Employee

-- > InvoiceLineLicense
-- < InvoiceLineLicense

-- > EmployeeSalaryRate
-- < EmployeeSalaryRate

-- > Supplier
-- < Supplier

-- > SourcerType
-- < SourcerType

-- > EmployeeContract
-- < EmployeeContract

-- > SourcerPersonalContact
-- < SourcerPersonalContact

-- > ClientCorporateContact
-- < ClientCorporateContact

-- > WorkingMonth
-- < WorkingMonth

-- > ProjectResource
-- < ProjectResource

-- > EmployeeHourlyRate
-- < EmployeeHourlyRate

-- > SourcerCorporateContact
-- < SourcerCorporateContact

-- > SalesInvoice
-- < SalesInvoice

-- > WorkingExpenses
-- < WorkingExpenses

-- > PurchaseInvoice
-- < PurchaseInvoice

-- > ClientPersonalContact
-- < ClientPersonalContact

-- > SoftwareLicense
-- < SoftwareLicense

-- > Client
-- < Client

-- > ApplicationService
-- < ApplicationService

-- > ProjectSprint
-- < ProjectSprint

-- > TimesheetEntryTaskStatus
-- < TimesheetEntryTaskStatus

-- > TimesheetRemark
ALTER TABLE ERPTIMESHEETS.TimesheetRemark
ADD FOREIGN KEY (timesheet_id)
REFERENCES ERPTIMESHEETS.Timesheet (id) DEFERRABLE;
-- < TimesheetRemark

-- > ExpenseDocumentTaskStatus
-- < ExpenseDocumentTaskStatus

-- > Expense
ALTER TABLE ERPTIMESHEETS.Expense
ADD FOREIGN KEY (currency_id)
REFERENCES ERPTIMESHEETS.Currency (id) DEFERRABLE;

ALTER TABLE ERPTIMESHEETS.Expense
ADD FOREIGN KEY (timesheetEntry_id)
REFERENCES ERPTIMESHEETS.TimesheetEntry (id) DEFERRABLE;
-- < Expense

-- > ExpenseTaskStatus
-- < ExpenseTaskStatus

-- > Currency
-- < Currency

-- > TimesheetTaskStatus
-- < TimesheetTaskStatus

-- > ExpenseDocument
ALTER TABLE ERPTIMESHEETS.ExpenseDocument
ADD FOREIGN KEY (expense_id)
REFERENCES ERPTIMESHEETS.Expense (id) DEFERRABLE;
-- < ExpenseDocument

-- > TimesheetEntry
ALTER TABLE ERPTIMESHEETS.TimesheetEntry
ADD FOREIGN KEY (timesheet_id)
REFERENCES ERPTIMESHEETS.Timesheet (id) DEFERRABLE;
-- < TimesheetEntry

-- > TimesheetProvisioning
-- < TimesheetProvisioning

-- > TimesheetProvisioningTaskStatus
-- < TimesheetProvisioningTaskStatus

-- > Timesheet
-- < Timesheet

-- > Note
-- < Note

-- > Adventure
-- < Adventure

-- > Activity
-- < Activity
-- anchor:foreign-keys:end

-- ------------------ --
-- Custom Statements  --
-- ------------------ --

-- @anchor:statements:start
-- @anchor:statements:end
-- anchor:custom-statements:start
-- anchor:custom-statements:end
