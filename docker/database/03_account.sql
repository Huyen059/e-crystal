
-- Account
INSERT INTO account.account (id, address, city, country, email, fullname, name, phone, refid, status, style, zipcode)
VALUES (nextval('public.hibernate_sequence'), 'Galileilaan 15', 'Niel', 'BE', 'info@nsx.normalizedsystems.org', 'Normalized Systems Expanders', 'NSX', '03/826.93.75', '1', 'Activated', 'nsxbootstrap', '2650');

-- Profiles
INSERT INTO account.profile (id, name, usergroup_id, weight) VALUES (nextval('public.hibernate_sequence'), 'master', NULL, 1) ;
INSERT INTO account.profile (id, name, usergroup_id, weight) VALUES (nextval('public.hibernate_sequence'), 'admin', NULL, 1) ;
INSERT INTO account.profile (id, name, usergroup_id, weight) VALUES (nextval('public.hibernate_sequence'), 'user', NULL, 1) ;

-- Users
INSERT INTO account.user (id, account_id, disabled, email, encryptedpassword, enteredat, firstname, fullname, language, lastmodifiedat, lastname, mobile, name, password, persnr, profile_id, timeout)
VALUES (nextval('public.hibernate_sequence'), (SELECT ID FROM account.account WHERE name = 'NSX'), 'no', 'superuser@nsx.normalizedsystems.org', null, '2020-01-01 00:00:00', 'Super',  'Super User', 'dutch', '2020-01-01 00:00:00', 'User', null, 'super', 'testSuper', null, (SELECT ID FROM account.profile WHERE name = 'master'), '1220');
INSERT INTO account.user (id, account_id, disabled, email, encryptedpassword, enteredat, firstname, fullname, language, lastmodifiedat, lastname, mobile, name, password, persnr, profile_id, timeout)
VALUES (nextval('public.hibernate_sequence'), (SELECT ID FROM account.account WHERE name = 'NSX'), 'no', 'beheerder@nsx.normalizedsystems.org', null, '2020-01-01 00:00:00', 'Beheerder', 'Beheerder', 'dutch', '2020-01-01 00:00:00', 'Beheerder', null, 'admin', 'testAdmin', null, (SELECT ID FROM account.profile WHERE name = 'admin'), '1220');
INSERT INTO account.user (id, account_id, disabled, email, encryptedpassword, enteredat, firstname, fullname, language, lastmodifiedat, lastname, mobile, name, password, persnr, profile_id, timeout)
VALUES (nextval('public.hibernate_sequence'), (SELECT ID FROM account.account WHERE name = 'NSX'), 'no', 'gebruiker@nsx.normalizedsystems.org', null, '2020-01-01 00:00:00', 'Gebruiker', 'Gebruiker', 'dutch', '2020-01-01 00:00:00', 'Gebruiker', null, 'user', 'testUser', null, (SELECT ID FROM account.profile WHERE name = 'user'), '1220');
