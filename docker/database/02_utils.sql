-- TagValuePairs
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'notifier_priority', 'High');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'notifier_priority', 'Normal');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'notifier_priority', 'Low');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'notifier_status', 'Initial');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'notifier_status', 'ToBeSend');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'notifier_status', 'Sent');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'notifier_status', 'NotSent');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'account_style', 'nsxbootstrap');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'account_style', 'nsx');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'account_style', 'basic');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'account_style', 'provAnt');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'account_style', 'naked');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'user_language', 'dutch');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'user_language', 'english');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'user_timeout', '3');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'user_timeout', '5');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'user_timeout', '15');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'user_timeout', '30');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'user_timeout', '60');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'user_timeout', '360');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'user_timeout', '720');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'user_timeout', '1220');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'user_timeout', '1440');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'engineService_status', 'stop');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'engineService_status', 'start');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'engineService_changed', 'no');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'engineService_changed', 'yes');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'engineService_busy', 'no');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'engineService_busy', 'yes');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'stateTimer_requiredAction', 'DoTask');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'stateTimer_requiredAction', 'AlterState');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'stateTimer_requiredAction', 'DoAndAlter');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'timeTask_requiredAction', 'DoTask');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'timeTask_requiredAction', 'AlterState');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'timeTask_requiredAction', 'DoAndAlter');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'execution_name', 'importCsv');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'execution_name', 'provisionFlow');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'execution_name', 'startEngine');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'asset_type', 'FILE');
INSERT INTO utils.tagvaluepair (id, tag, value) VALUES (nextval('public.hibernate_sequence'), 'asset_type', 'INTERNAL');

-- ParamTargetValues
INSERT INTO utils.paramtargetvalue (id, param, target, value)
VALUES (nextval('public.hibernate_sequence'), 'longDataFormat', 'default', 'dd-MM-yyyy,HH:mm:ss');
INSERT INTO utils.paramtargetvalue (id, param, target, value)
VALUES (nextval('public.hibernate_sequence'), 'screenRefreshRate', 'default', '30');
INSERT INTO utils.paramtargetvalue (id, param, target, value)
VALUES (nextval('public.hibernate_sequence'), 'pageBrowsingWidth', 'default', '15');
INSERT INTO utils.paramtargetvalue (id, param, target, value)
VALUES (nextval('public.hibernate_sequence'), 'forwardingLastAckSeen', 'default', 'yes');
INSERT INTO utils.paramtargetvalue (id, param, target, value)
VALUES (nextval('public.hibernate_sequence'), 'ScreenInfoRetrieverActionImpl', 'default', 'net.democritus.web.action.ScreenInfoRetrieverAction');
INSERT INTO utils.paramtargetvalue (id, param, target, value)
VALUES (nextval('public.hibernate_sequence'), 'defaultStyle', 'default', '');
INSERT INTO utils.paramtargetvalue (id, param, target, value)
VALUES (nextval('public.hibernate_sequence'), 'fileDownloadDirUri', 'default', 'file:////home/palver/NSX-download/NSF-3.0.5/infrastructure/apache-tomee-7.0.4-plume/BASE_SIMCARDMANAGER/documents');
INSERT INTO utils.paramtargetvalue (id, param, target, value)
VALUES (nextval('public.hibernate_sequence'), 'fileDownloadDirUri', 'upload', 'file:////home/palver/NSX-download/NSF-3.0.5/infrastructure/apache-tomee-7.0.4-plume/BASE_SIMCARDMANAGER/documents');
INSERT INTO utils.paramtargetvalue (id, param, target, value)
VALUES (nextval('public.hibernate_sequence'), 'fileUploadDirUri', 'default', 'file:////home/palver/NSX-download/NSF-3.0.5/infrastructure/apache-tomee-7.0.4-plume/BASE_SIMCARDMANAGER/documents');
INSERT INTO utils.paramtargetvalue (id, param, target, value)
VALUES (nextval('public.hibernate_sequence'), 'useEncryptedPassword', 'default', 'no');
INSERT INTO utils.paramtargetvalue (id, param, target, value)
VALUES (nextval('public.hibernate_sequence'), 'anchorParam', 'default', 'value');
INSERT INTO utils.paramtargetvalue (id, param, target, value)
VALUES (nextval('public.hibernate_sequence'), 'assetChunkSize', 'default', '524288');
