#!/bin/sh
set -e

#Inject environment variables into datasource
sed -i -e 's/${DATABASE_HOST}/'${DATABASE_HOST}'/g' \
       -e 's/${DATABASE_PORT}/'${DATABASE_PORT}'/g' \
       -e 's/${DATABASE_NAME}/'${DATABASE_NAME}'/g' \
       -e 's/${DATABASE_USER}/'${DATABASE_USER}'/g' \
       -e 's/${DATABASE_PWD}/'${DATABASE_PWD}'/g' \
       -e 's/${DATABASE_MAX_ACTIVE}/'${DATABASE_MAX_ACTIVE}'/g' \
       ${CATALINA_BASE}/conf/tomee.xml

echo 'Injected DATABASE_HOST:' ${DATABASE_HOST}
echo 'Injected DATABASE_PORT:' ${DATABASE_PORT}
echo 'Injected DATABASE_NAME:' ${DATABASE_NAME}
echo 'Injected DATABASE_USER:' ${DATABASE_USER}
echo 'Injected DATABASE_PWD: ********'
